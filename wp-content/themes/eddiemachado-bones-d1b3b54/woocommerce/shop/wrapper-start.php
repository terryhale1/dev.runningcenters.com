<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$template = get_option('template');

switch( $template ) {
	case 'twentyeleven' :
		echo '<section id="primary"><section id="content" role="main">';
		break;
	case 'twentytwelve' :
		echo '<section id="primary" class="site-content"><section id="content" role="main">';
		break;
	case 'twentythirteen' :
		echo '<section id="primary" class="site-content"><section id="content" role="main" class="entry-content twentythirteen">';
		break;
	default :
		echo '<div class="wrapper"><section class="main-full"><section class="main left">';
		break;
}