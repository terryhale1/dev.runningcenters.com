<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if (empty($woocommerce_loop['loop']))
    $woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if (empty($woocommerce_loop['columns']))
    $woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 4);

// Ensure visibility
if (!$product->is_visible())
    return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if (0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'])
    $classes[] = 'first';
if (0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'])
    $classes[] = 'last';
?>

<?php                
$link = add_query_arg(
        apply_filters( 'woocommerce_loop_quick_view_link_args', array(
            'wc-api'  => 'WC_Quick_View',
            'product' => $product->id,
            'width'   => '60%',
            'height'  => '60%',
            'ajax'    => 'true'
        ) ),
        home_url( '/' )
    );
?>

<li <?php post_class($classes); ?>> 
  
    <a href="<?php the_permalink(); ?>"> 
        <div id="prod-img-<?php the_ID(); ?>" style="height:254px" class="prod-imge" >
            <?php
            /**
             * woocommerce_before_shop_loop_item_title hook
             *
             * @hooked woocommerce_show_product_loop_sale_flash - 10
             * @hooked woocommerce_template_loop_product_thumbnail - 10
             */
            do_action('woocommerce_before_shop_loop_item_title');
            ?>
            <?php $terms = wp_get_post_terms( $product->id, 'product_brand', array("fields" => "names") ); ?>
            <div class="brand-name"><?php echo $terms[0] ?></div>
            <?php $gender = wp_get_post_terms($product->id, 'pa_gender', array("fields" => "names") ); ?>
            <h3><?php the_title(); ?> - <?php echo $gender[0] ?></h3> </a>  

    <?php
    /**
     * woocommerce_after_shop_loop_item_title hook
     *
     * @hooked woocommerce_template_loop_price - 10
     */
    do_action('woocommerce_after_shop_loop_item_title');
    ?>
</div>
<div class="product-detail">
    <?php echo apply_filters( 'woocommerce_loop_quick_view_button', sprintf( '<a href="%s" title="' . get_the_title() . '" class="qview button "><span></span>%s</a>',  $link, __( 'Quick View', 'wc_quick_view' ) ) );?>
</div> 

<script>
    jQuery(document).ready(function($) {
       $('#prod-img-<?php the_ID(); ?>').contenthover({data_selector: '.product-detail', overlay_height: 100, effect: 'slide', slide_direction: 'bottom'});
    });
</script>
<?php do_action('woocommerce_after_shop_loop_item'); ?>
</li>