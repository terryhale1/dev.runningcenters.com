<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.3
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $woocommerce, $product;

?>


<div class="images">
    <div style="text-align: center"><h1><?php the_title(); ?></h1>
<?php ( $rating_html = $product->get_rating_html() )?>
<?php if($rating_html) { ?>

	<div class="starrating toprating" style="width:auto"> 
		<?php $count = $product->get_rating_count(); ?>
		<span class="rating"><?php echo $rating_html; ?></span>
		<span class="review"> (<?php echo $count; ?> Reviews) </span>
	</div>   

<?php    
} else { ?>
	<div class="starrating toprating noreview" style="width:auto"> 
		<p class="noreviews">There are no reviews yet, would you like to <a href="#review_form" class="inline show_review_form">submit yours</a>?</p>
<!-- 		<span class="rating"><div class="star-rating" <span style="width:100%"><strong class="rating"></strong></span></div></span>
		<span class="review"> (0 Reviews) </span> -->
	</div>
<?php } ?>
    </div>
	<?php
		if ( has_post_thumbnail() ) {

			$image       		= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) );
			$image_title 		= esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_link  		= wp_get_attachment_url( get_post_thumbnail_id() );
			$attachment_count   = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s"  rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $image ), $post->ID );

		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="Placeholder" />', woocommerce_placeholder_img_src() ), $post->ID );

		}
	?>
        
</div>

<?php do_action( 'woocommerce_product_thumbnails' ); ?>
<div class="clear"></div>

