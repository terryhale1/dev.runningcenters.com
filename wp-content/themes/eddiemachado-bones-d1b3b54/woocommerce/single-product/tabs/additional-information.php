<?php
/**
 * Additional Information tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce, $post, $product;

$heading = apply_filters( 'woocommerce_product_additional_information_heading', __( 'Additional Information', 'woocommerce' ) );
?>
<div class="bottom-padding-fix" style="padding:30px;margin-top:0px;">

<h1 class="description"><?php echo $heading; ?></h1>

<?php $product->list_attributes(); ?>
<hr class="up">
</div>