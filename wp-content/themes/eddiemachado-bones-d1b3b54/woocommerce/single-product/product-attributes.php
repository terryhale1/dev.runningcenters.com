<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.8
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $woocommerce;

$alt = 1;
$attributes = $product->get_attributes();

if (empty($attributes) && (!$product->enable_dimensions_display() || (!$product->has_dimensions() && !$product->has_weight() ) ))
    return;
?>
<div class="shop_attributes">

    <?php if ($product->enable_dimensions_display()) : ?>

    <?php if ($product->has_weight()) : ?>

            <div class="<?php if (( $alt = $alt * -1 ) == 1) echo 'alt'; ?>">
                <div><?php _e('Weight', 'woocommerce') ?></div>
                <div class="product_weight"><?php echo $product->get_weight() . ' ' . esc_attr(get_option('woocommerce_weight_unit')); ?></div>
            </div>

        <?php endif; ?>

    <?php if ($product->has_dimensions() && false) : ?>

            <div class="<?php if (( $alt = $alt * -1 ) == 1) echo 'alt'; ?>">
                <div><?php _e('Dimensions', 'woocommerce') ?></div>
                <div class="product_dimensions"><?php echo $product->get_dimensions(); ?></div>
            </div>

        <?php endif; ?>

    <?php endif; ?>

    <?php
    $loop = 0;
    foreach ($attributes as $attribute) :

        if (empty($attribute['is_visible']) || ( $attribute['is_taxonomy'] && !taxonomy_exists($attribute['name']) ))
            continue;
        ?>
        <?php
        $loop++;
        if ($loop == 1): $class = 'first';
        endif;
        if ($loop == 2): $class = 'middle';
        endif;
        if ($loop == 3): $class = 'last';
            $loop = 0;
        endif;
        ?>
        <div class="<?php echo $class; ?>">
            <div class="attribute"><div class="mod-wrapper <?php echo $attribute['name'];?>">
            <h3><?php echo $woocommerce->attribute_label($attribute['name']); ?></h3>
                <div class="mod-subname"><?php
                if ($attribute['is_taxonomy']) {
                    $values = woocommerce_get_product_terms($product->id, $attribute['name'], 'names');
                } else {
                    // Convert pipes to commas and display values
                    $values = array_map('trim', explode('|', $attribute['value']));
                }
                ?>
                <?php
                if ($attribute['is_taxonomy']) {
                    $values = woocommerce_get_product_terms($product->id, $attribute['name'], 'names');
                    echo apply_filters('woocommerce_attribute', wptexturize(implode(', ', $values)), $attribute, $values);
                } else {
                    // Convert pipes to commas and display values
                    $values = array_map('trim', explode('|', $attribute['value']));
                    echo apply_filters('woocommerce_attribute', wptexturize(implode(', ', $values)), $attribute, $values);
                }
                ?></div>
            <div class="attribute-<?php echo $attribute['name'] . " " . strtolower(str_replace(" ", "-", $values[0])); ?>                <?php
                if ($attribute['is_taxonomy']) {
                    $values = woocommerce_get_product_terms($product->id, $attribute['name'], 'names');
                    echo apply_filters('woocommerce_attribute', wptexturize(strtolower(implode(', ', $values))), $attribute, $values);
                } else {
                    // Convert pipes to commas and display values
                    $values = array_map('trim', explode('|', $attribute['value']));
                    echo apply_filters('woocommerce_attribute', wptexturize(strtolower(implode(', ', $values))), $attribute, $values);
                }
                ?>-value"></div>
            <?php $all_terms = get_terms($attribute['name'], array('menu_order' => 'ASC', 'hide_empty' => false, 'fields' => 'names')); ?>
           <?php foreach($all_terms as $term): ?>
            <div class="<?php echo strtolower(str_replace(" ","-",$term)); ?>"> <?php echo $term; ?></div> 
            <?php endforeach; ?>
            </div></div>
        </div>

<?php endforeach; ?>
</div>
