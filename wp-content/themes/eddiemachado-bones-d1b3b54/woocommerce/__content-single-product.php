<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $post, $woocommerce, $product;
?>

<?php
/**
 * woocommerce_before_single_product hook
 *
 * @hooked woocommerce_show_messages - 10
 */
do_action('woocommerce_before_single_product');
?>

<div class="porduct-left">
    <h1><?php the_title(); ?></h1>
    <div class="porduct-star-rating">
        <span class="rating">
            <span class="star-blue"></span> 
            <span class="star-blue"></span> 
            <span class="star-blue"></span> 
            <span class="star-blue"></span> 
            <span class="star-grey"></span> 
        </span> 

        <span class="review">(4 Reviews)</span> 
    </div>
    <?php
    if (has_post_thumbnail()) {

        $image = get_the_post_thumbnail($post->ID, apply_filters('single_product_large_thumbnail_size', 'shop_single'));
        $image_title = esc_attr(get_the_title(get_post_thumbnail_id()));
        $image_link = wp_get_attachment_url(get_post_thumbnail_id());
        $attachment_count = count($product->get_gallery_attachment_ids());

        if ($attachment_count > 0) {
            $gallery = '[product-gallery]';
        } else {
            $gallery = '';
        }

        echo apply_filters('woocommerce_single_product_image_html', sprintf('<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s"  rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $image), $post->ID);
    } else {

        echo apply_filters('woocommerce_single_product_image_html', sprintf('<img src="%s" alt="Placeholder" />', woocommerce_placeholder_img_src()), $post->ID);
    }
    ?>

    <?php do_action('woocommerce_product_thumbnails'); ?>
</div>

<div class="porduct-right">

    <span class="product-price-detail">
        <?php echo $product->get_price_html(); ?>
        <link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
    </span>
    
    <span class="product-thumb">
        <img src="images/product/shoe-02-thumb-01.png">
    </span>

    <span class="product-thumb">
        <img src="images/product/shoe-02-thumb-02.png">
    </span>

    <span class="product-thumb">
        <img src="images/product/shoe-02-thumb-03.png">
    </span>

</div>

</section>
<aside class="option-pod right">
    <h2>CHOOSE YOUR OPTIONS</h2>

    <div class="pod-grey">
        <span class="pod-head">COLOR</span>
        <span class="pod-head-bar"><a href="#"> - </a></span>

        <div class="clearfix"></div>
        <span class="color-palette selected"><img src="images/color-palette-01.png" alt="" title=""></span>
        <span class="color-palette"><img src="images/color-palette-02.png" alt="" title=""></span>
        <span class="color-palette"><img src="images/color-palette-03.png" alt="" title=""></span>
        <span class="color-palette"><img src="images/color-palette-04.png" alt="" title=""></span>
        <div class="clearfix"></div>

    </div>


    <div class="pod-grey">
        <span class="pod-head">SIZE</span>
        <span class="pod-head-bar"><a href="#"> - </a></span>

        <div class="clearfix"></div>
        <span class="size">8.5</span>
        <span class="size">9</span>
        <span class="size">9.5</span>
        <span class="size selected">11.5</span>
        <span class="size">12</span>
        <span class="size">12.5</span>
        <span class="size">10</span>
        <span class="size">10.5</span>
        <span class="size">11</span>

        <div class="clearfix"></div>

    </div>


    <div class="pod-grey">
        <span class="pod-head">WIDTH</span>
        <span class="pod-head-bar"><a href="#"> - </a></span>

        <div class="clearfix"></div>
        <span class="width">d</span>
        <span class="width">2e</span>
        <span class="width">4e</span>
        <div class="clearfix"></div>

    </div>


    <div class="pod-grey">
        <span class="pod-head">QTY</span>
        <span class="qty-select">
            <select>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>                    
            </select>
        </span>



        <div class="clearfix"></div>
    </div>


    <button class="product_add_to_cart_button button alt" type="submit">Add to cart</button>


</aside>

<?php do_action('woocommerce_after_single_product'); ?>