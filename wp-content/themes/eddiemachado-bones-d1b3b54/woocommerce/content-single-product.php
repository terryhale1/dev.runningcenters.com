<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
?>
<?php global $woocommerce, $product; ?>
<?php
/**
 * woocommerce_before_single_product hook
 *
 * @hooked woocommerce_show_messages - 10
 */
do_action('woocommerce_before_single_product');
?>



<div class="hopeful">
<div itemscope itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php $msrp_price = get_post_meta( $product->id, '_msrp_price', true ); ?>
 
<div class="h-font2"><?php echo get_the_title(); ?> </div>
        <div itemprop="offers"  class ="prod-right" itemscope itemtype="http://schema.org/Offer">
            <p itemprop="price" class="price"><?php echo str_replace("From:", "Our Price", $product->get_price_html()); ?></p>
            
    <meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
    <link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
<p itemprop="price" class="msrp"><?php echo "MSRP:" ?><?php echo $msrp_price ?></p>
</div>

<div class="clear"></div>
    <?php
    /**
     * woocommerce_show_product_images hook
     *
     * @hooked woocommerce_show_product_sale_flash - 10
     * @hooked woocommerce_show_product_images - 20
     */
    //add_filter('woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5);
    //add_filter('woocommerce_before_single_product_summary', 'woocommerce_template_single_price',10);
    remove_filter('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
    do_action('woocommerce_before_single_product_summary');
    ?>


</div>
<div class="clear"></div>
    <div class="summary entry-summary">

        <?php
        /**
         * woocommerce_single_product_summary hook
         *
         * @hooked woocommerce_template_single_title - 5
         * @hooked woocommerce_template_single_price - 10
         * @hooked woocommerce_template_single_excerpt - 20
         * @hooked woocommerce_template_single_add_to_cart - 30
         * @hooked woocommerce_template_single_meta - 40
         * @hooked woocommerce_template_single_sharing - 50
         */
        remove_filter('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
        remove_filter('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        remove_filter('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
        do_action('woocommerce_single_product_summary');
        ?>

    </div><!-- .summary -->

    
</div>
<aside class="option-pod right">
<h2>CHOOSE YOUR OPTIONS</h2>
<?php do_action('woocommerce_single_product_sidebar'); ?>
</aside>
<?php
/**
 * woocommerce_after_single_product_summary hook
 *
 * @hooked woocommerce_output_product_data_tabs - 10
 * @hooked woocommerce_output_related_products - 20
 */
remove_filter ( 'woocommerce_after_single_product_summary' , 'woocommerce_output_related_products', 20);
do_action( 'woocommerce_after_single_product_summary' );
?>

<!-- #product-<?php the_ID(); ?> -->

                
<?php do_action('woocommerce_after_single_product'); ?>


</section>

<?php// echo get_attachment_id_by_url( 'http://dev.runningcenters.com/wp-content/uploads/2013/07/T3CON.8101.04.jpg' ) 
//  woocommerce_get_template('single-product/tabs/description.php');
//  woocommerce_get_template('single-product/tabs/additional-information.php');
//    woocommerce_get_template('single-product-reviews.php');
//    woocommerce_get_template('single-product/review.php');
?>  

