<?php
/**
 * Product Loop End
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
?>
</ul>
<script type='text/javascript' src='<?php echo bloginfo('template_url'); ?>/js/jquery.contenthover.js'></script>        
<script>
jQuery('.prod-imge').each(function(i){
                jQuery(this).contenthover({data_selector: '.product-detail', overlay_height: 100, effect: 'slide', slide_direction: 'bottom'});
            });
</script>