<?php
/**
 * Loop Rating
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;

if ( get_option( 'woocommerce_enable_review_rating' ) == 'no' )
	return;
?>

<?php ( $rating_html = $product->get_rating_html() )?>
<?php if($rating_html) { ?>

	<div class="starrating"> 
		<?php $count = $product->get_rating_count(); ?>
		<span class="rating"><?php echo $rating_html; ?></span>
		<span class="review"> (<?php echo $count; ?> Reviews) </span>
	</div>   

<?php    
} else { ?>
	<!-- <div class="starrating"> 
		<span class="rating"><div class="star-rating" <span style="width:100%"><strong class="rating"></strong></span></div></span>
		<span class="review"> (0 Reviews) </span>
	</div> -->
 <div class="noreview"></div>
<?php } ?>
