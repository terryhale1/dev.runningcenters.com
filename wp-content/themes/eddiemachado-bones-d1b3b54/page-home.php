<?php
/*
  Template Name: Home page
 */
?>

<?php get_header(); ?>

<!--! start of wrapper -->
<div class="wrapper">

    <div class="">
        <?php get_sidebar('home-slider'); ?>
    </div>

    <hr>
</div>
    <section class="full-pod pod1">
        <h1>FEATURED PRODUCTS</h1>
<ul class= 'inner-full-pod'>
        <?php
        global $product;
        $featured_product_query = new WP_Query(array(
            'post_status' => 'publish',
            'post_type' => 'product',
            'meta_key' => '_featured',
            'orderby'=> 'date',
            'order'=> 'DESC',
            'meta_value' => 'yes',
            'posts_per_page' => 4
                ));

        if ($featured_product_query->have_posts()) : while ($featured_product_query->have_posts()) : $featured_product_query->the_post(); 
        $terms = wp_get_post_terms( $product->id, 'product_brand', array("fields" => "names") ); 
        global $product, $woocommerce_loop;

        // Store loop count we're currently on
        if (empty($woocommerce_loop['loop']))
            $woocommerce_loop['loop'] = 0;

        // Store column count for displaying the grid
        if (empty($woocommerce_loop['columns']))
            $woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 4);

        // Ensure visibility
        if (!$product->is_visible())
            return;

        // Increase loop count
        $woocommerce_loop['loop']++;

        // Extra post classes
        $classes = array();
        if (0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'])
            $classes[] = '';
        if (0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'])
            $classes[] = '';

        $link = add_query_arg(
        apply_filters( 'woocommerce_loop_quick_view_link_args', array(
            'wc-api'  => 'WC_Quick_View',
            'product' => $product->id,
            'width'   => '60%',
            'height'  => '60%',
            'ajax'    => 'true'
        ) ),
        home_url( '/' )
    );  ?>

        <li <?php post_class($classes); ?>>


                <?php if(has_post_thumbnail($featured_product_query->post->ID)): ?>
                   <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?></a>
                <?php endif; ?>
            <div class="homeLables">
            <div class="homeTitles">
            <span class="homeShoe"><?php $terms = wp_get_post_terms( $product->id, 'product_brand', array("fields" => "names") ); ?> 
            <?php echo $terms[0] ?></span><span>/</span>
            <?php echo get_the_title($featured_product_query->post->ID); ?>
            </div> 
            </div>
            <br />
            <div class="findout">
                <a href="<?php the_permalink(); ?>">Find Out More</a>
            </div>    

            <script>
                jQuery(document).ready(function($) {
                   $('#prod-img-<?php the_ID(); ?>').contenthover({data_selector: '.product-detail', overlay_height: 100, effect: 'slide', slide_direction: 'bottom'});
                });
            </script>
            <?php //do_action('woocommerce_after_shop_loop_item'); ?>
        </li>
            <?php endwhile;
            
         endif;
        wp_reset_query();
        ?></ul>
        <div class="clearfix"></div>
    </section>



    <div class="clearfix"></div>

<!--! end of wrapper -->

<section class="full-pod">

    <hr class="up">

    <h1>WHAT’S TRENDING</h1>
    <?php if(get_field('trending')): ?>
     <div id="trending-grid">
    <?php while(has_sub_field('trending')): ?>
        
            <?php $subfield = get_sub_field('trending_post');
                  $trending_image = get_sub_field('image'); ?>
           <a href="<?php echo get_permalink($subfield->ID);?>" title="<?php echo get_the_title($subfield->ID);?>"><img src="<?php echo $trending_image; ?>"></a>

        
 	<?php endwhile; ?>
 
	</div>
    <script type="text/javascript">
    jQuery("#treding-grid").justifiedGallery({
        'usedSuffix':'lt240', 
        'justifyLastRow':true, 
        'rowHeight':110, 
        'fixedHeight':false, 
        'lightbox':false, 
        'captions':true, 
        'margins':1
    });
</script>
 
<?php endif; ?>
<!--<img src="<?php bloginfo('template_url'); ?>/images/slide-img.png" alt="" title="">-->
</section>

<!--! start of wrapper -->
<section class="full-pod pod2">

    <hr class="up">

        <h1>BRANDS</h1>
<?php 
// no default values. using these as examples
$taxonomies = 'product_brand';

$args = array(
    'orderby'       => 'name', 
    'order'         => 'ASC',
    'hide_empty'    => true, 
    'exclude'       => array(), 
    'exclude_tree'  => array(), 
    'include'       => array(),
    'number'        => '', 
    'fields'        => 'all', 
    'slug'          => '', 
    'parent'         => '',
    'hierarchical'  => true, 
    'child_of'      => 0, 
    'get'           => '', 
    'name__like'    => '',
    'pad_counts'    => false, 
    'offset'        => '', 
    'search'        => '', 
    'cache_domain'  => 'core',
    'hide_empty'    => false
); 

$brands = get_terms($taxonomies, $args ) ?>
<!-- <div class="flexslider"> -->

<div class="image_carousel">
    <div id="sliderwrapper">
    <div id="brandslides">
        <?php foreach ( $brands as $brand ) : 
            
            $thumbnail = get_brand_thumbnail_url( $brand->term_id, 'brand-thumb' );
            
            if ( ! $thumbnail )
                $thumbnail = woocommerce_placeholder_img_src();

            
            ?>
            <a href="<?php echo get_term_link( $brand->slug, 'product_brand' ); ?>" title="<?php echo $brand->name; ?>">
                <img src="<?php echo $thumbnail; ?>" alt="<?php echo $brand->name; ?>" />
            </a>            

        <?php endforeach; ?>
        
    </div>
</div>
</div>
<!--  -->


<script type="text/javascript">

jQuery( document ).ready(function( $ ) {

  // Code using $ as usual goes here.

  jQuery("#brandslides").carouFredSel({ 
    width: "99%", 
    height: 160,
    items: {
        width: "variable",
        height: "variable",
        visible: "variable",
        align:"center"
    },
    scroll: {
        pauseOnHover: true,
    },
});

  


})

</script>


    </section>

    <div class="clearfix"></div>
</div>
<!--! end of wrapper -->

<?php get_footer(); ?>


