<?php
/*
Template Name: Page Left
*/
?>

<?php get_header(); ?>

<!--! start of wrapper -->
<div class="wrapper">

    <aside class="option-pod left">
        <?php get_sidebar(); ?>
    </aside>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <section class="main right">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </section>
        <?php endwhile; endif; ?>

    <div class="clearfix"></div>
</div>
<!--! end of wrapper -->

<?php get_footer(); ?>