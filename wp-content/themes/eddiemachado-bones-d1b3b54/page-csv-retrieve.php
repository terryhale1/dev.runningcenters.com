<?php
/*
  Template Name: CSV
 */
?>


<?php get_header(); ?>

<!--! start of wrapper -->
<div class="wrapper">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <section class="main full">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </section>
        <?php endwhile; endif; ?>

    <div class="clearfix"></div>
</div>
<!--! end of wrapper -->


<?php 

$data = $_REQUEST['image_url'];
$feed_url = get_template_directory() . '/image.txt';
$save_path = get_template_directory() . '/temp.csv';
feeding_urls($feed_url, $data, $save_path) ?>

<?php get_footer(); ?>