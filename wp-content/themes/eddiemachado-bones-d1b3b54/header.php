<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php wp_title(''); ?></title>
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/layout.css">
  
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/form.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/typo.css">
  
  <!-- mobile meta (hooray!) -->
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
  <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
  <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
  <!--[if IE]>
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
  <![endif]-->
  <!-- or, set /favicon.ico for IE10 win -->
  <meta name="msapplication-TileColor" content="#f01d4f">
  <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  <!-- wordpress head functions -->
  <?php wp_head(); ?>
  <!-- end of wordpress head -->
  
  <script type="text/javascript" language="javascript" src="<?php bloginfo('template_url'); ?>/js/libs/jquery.carouFredSel-6.2.1.js"></script>
  <script type="text/javascript" language="javascript" src="<?php bloginfo('template_url'); ?>/js/libs/justified-gallery/js/jquery.justifiedgallery.min.js"></script>
  
  <!--<script type="text/javascript" language="javascript" src="<?php bloginfo('template_url'); ?>/js/libs/flexslider/jquery.flexslider-min.js"></script>
 <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/libs/flexslider/flexslider.css" type="text/css"> -->
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/libs/justified-gallery/css/jquery.justifiedgallery.min.css" type="text/css">
  <!-- drop Google Analytics Here -->

  <!-- end analytics -->
  
 <!-- flexslider javascript-->

<!-- Place in the <head>, after the three links -->
<script type="text/javascript" charset="utf-8">

  // jQuery(window).load(function() {
  //   jQuery('.flexslider').flexslider({
  //     animation: "slide",
  //     animationLoop: true,
  //     itemWidth: 210,
  //     itemMargin: 15,
  //     controlNav: false,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
  //     directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
  //     prevText: "&larr; Previous",           //String: Set the text for the "previous" directionNav item
  //     nextText: "Next &rarr;"   
  //   });
  // });
</script>
<?php
    if ( is_user_logged_in() ) {
      
    } else { ?>
    <style type="text/css">
        .login-check {
          display: none !important;
        }
      </style>
      <?php }
?>


</head>
<body <?php body_class(); ?>>
   

<!--! start of #header -->
<header>

    <!--! start of wrapper -->
    <div class="wrapper">
    
        <div class="logo"><a href="<?php echo home_url(); ?>" rel="nofollow"><img src="<?php bloginfo('template_url'); ?>/images/logo-running-cernter.png"/></a></div>
    	<span class="tagline">serving runners since 1977</span>
    
        <div class="media"> 
	        <p class="number">1-877-509-1122</p>
            
            <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/icon-pinterest.png" alt="" title=""></a>
            <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/icon-twitter.png" alt="" title=""></a>
            <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/icon-yelp.png" alt="" title=""></a>
            <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/icon-facebook.png" alt="" title=""></a>
            
        </div>
        
    </div>
    <!--! end of wrapper -->        

</header>
<!--! end of #header -->

<!--! start of wrapper -->
<div class="wrapper">
    
    
    <div class="top-nav">
    
        <!--! start of nav -->
        <nav class="main">
            <?php bones_main_nav(); ?>
		</nav>
        <!--! start of nav -->
            
        <nav class="main-right">
            <?php bones_secondary_nav(); ?>
        </nav>
	    <!--! end of nav -->    
                
		<input type="text" value="search" id="" class="search-home" >

	</div>


</div>
<div class= "top-menu-shadow"></div>
<!--! end of wrapper -->