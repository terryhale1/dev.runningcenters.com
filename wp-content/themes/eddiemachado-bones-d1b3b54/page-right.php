<?php
/*
Template Name: Page Right
*/
?>

<?php get_header(); ?>

<!--! start of wrapper -->
<div class="wrapper">


    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <section class="main left">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </section>
        <?php endwhile; endif; ?>
    <aside class="option-pod right featured">
        <ul class= 'products'>
        <?php
        global $product;
        $featured_product_query = new WP_Query(array(
            'post_status' => 'publish',
            'post_type' => 'product',
            'meta_key' => '_featured',
            // 'meta_key' => '_total_sales',
            'orderby'  => 'rand',
            'order'=> 'DESC',
            'meta_value' => 'yes',
            'posts_per_page' => 1
                ));

        if ($featured_product_query->have_posts()) : while ($featured_product_query->have_posts()) : $featured_product_query->the_post();
?>
<!--          <span class="featured-product-wrap">
            <span class="featured-product">
                <?php if(has_post_thumbnail($featured_product_query->post->ID)): ?>
                   <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
                <?php endif; ?>
            </span>
            <span class="featured-title"><a href="<?php get_permalink(); ?>"><?php echo get_the_title($featured_product_query->post->ID); ?></a></span>
        </span> -->
        <?php woocommerce_get_template_part('content', 'product'); ?>
         <?php
            endwhile;
            
         endif;
        wp_reset_query();
        ?></ul>
        <?php get_sidebar(); ?>
    </aside>
    <div class="clearfix"></div>
</div>
<!--! end of wrapper -->

<?php get_footer(); ?>