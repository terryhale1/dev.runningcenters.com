<?php
/**
 * Quick View Button
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo apply_filters( 'woocommerce_loop_quick_view_button', sprintf( '<a href="%s" title="' . get_the_title() . '" class="quick-view-button button hidden"><span></span>%s</a>', $link, __( 'Quick View', 'wc_quick_view' ) ) );