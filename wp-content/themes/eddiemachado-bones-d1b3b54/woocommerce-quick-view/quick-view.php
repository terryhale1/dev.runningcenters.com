<?php
/**
 * Quick view template
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $post, $woocommerce;
?>
<div class="woocommerce quick-view product">
<div class="popHeader">	

	<div class="popTitle"><h3><?php echo the_title(); ?></h3></div>

		<div class="pop-price"><?php woocommerce_template_single_price(); ?></div>	

</div>

		<div class="quick-view-image images">

		<?php if ( has_post_thumbnail() ) : ?>

			<?php echo get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) ) ?>

		<?php else : ?>

			<img src="<?php echo woocommerce_placeholder_img_src(); ?>" alt="Placeholder" />

		<?php endif; ?>
		<?php woocommerce_template_single_excerpt(); ?>

	</div>

	<div class="quick-view-content">

		
		
		
		<div class="popadd"><?php woocommerce_template_single_add_to_cart(); ?></div>
<a class="button addPop" href="<?php echo get_permalink( $product->id ); ?>" onclick="window.parent.location ='<?php echo get_permalink( $product->id ); ?>'"><?php _e( 'View Full Details', 'wc_quick_view' ); ?></a>
	</div>

</div>