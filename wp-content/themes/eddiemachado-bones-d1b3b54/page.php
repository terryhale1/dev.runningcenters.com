<?php get_header(); ?>

<!--! start of wrapper -->
<div class="wrapper">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <section class="main full">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </section>
        <?php endwhile; endif; ?>

    <div class="clearfix"></div>
</div>
<!--! end of wrapper -->

<?php get_footer(); ?>