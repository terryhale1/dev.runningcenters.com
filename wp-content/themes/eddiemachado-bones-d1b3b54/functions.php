<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once('library/bones.php'); // if you remove this, bones will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once('library/custom-post-type.php'); // you can disable this if you like
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once('library/admin.php'); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once('library/translation/translation.php'); // this comes turned off by default

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'home-slider',
		'name' => __('Homepage Slider', 'bonestheme'),
		'description' => __('The secondary sidebar.', 'bonestheme'),
		'before_widget' => '<div class="slider">',
		'after_widget' => '</div>',
		'before_title' => '<div class="hidden">',
		'after_title' => '</div>',
	));

	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __('Sidebar 1', 'bonestheme'),
		'description' => __('The first (primary) sidebar.', 'bonestheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div><div style="clear:both;"></div>',
		'before_title' => '<h3 class="pod-head accordion">',
		'after_title' => '<span></span></h3>',
	));
    
    register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'bonestheme'),
		'description' => __('The secondary sidebar.', 'bonestheme'),
		'before_widget' => '<div class="pod-dark-grey">',
		'after_widget' => '</div>',
		'before_title' => '<span class="pod-head">',
		'after_title' => '</span>',
	));

    register_sidebar(array(
        'id' => 'footer-sidebar',
        'name' => __('Footer Sidebar', 'bonestheme'),
        'description' => __('The Footer side bar for menus.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));


	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'bonestheme'),
		'description' => __('The second (secondary) sidebar.', 'bonestheme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5($bgauthemail); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<!-- end custom gravatar call -->
				<?php printf(__('<cite class="fn">%s</cite>', 'bonestheme'), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__('F jS, Y', 'bonestheme')); ?> </a></time>
				<?php edit_comment_link(__('(Edit)', 'bonestheme'),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e('Your comment is awaiting moderation.', 'bonestheme') ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->

<?php	define('WOOCOMMERCE_USE_CSS', false); ?>
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __('Search for:', 'bonestheme') . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search the Site...','bonestheme').'" />
	<input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
	</form>';
	return $form;
} // don't remove this bracket!



function accordian_scripts() {
	wp_enqueue_script(
		'jquery-ui-core'		
	);
}
add_action( 'wp_enqueue_scripts', 'accordian_scripts' );


add_filter( 'sod_ajax_layered_nav_containers', 'bones_containers' );
function bones_containers(){
		$html_containers = array(
			'.main-full',
                        '#products',
			'#pagination-wrapper',
			'.woocommerce-pagination',
                        '.widget_layered_nav',
			'.widget_layered_nav_filters',
			'.woocommerce-ordering',
			'.woocommerce-result-count'
		);
        return $html_containers;
}

add_action( 'woocommerce_single_product_sidebar', 'woocommerce_template_single_add_to_cart', 30 );
//add_filter('woocommerce_before_single_product_summary', 'my_print_stars' , 15 );

function my_print_stars(){
    global $wpdb;
    global $post;
    $count = $wpdb->get_var("
    SELECT COUNT(meta_value) FROM $wpdb->commentmeta
    LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
    WHERE meta_key = 'rating'
    AND comment_post_ID = $post->ID
    AND comment_approved = '1'
    AND meta_value > 0
");

$rating = $wpdb->get_var("
    SELECT SUM(meta_value) FROM $wpdb->commentmeta
    LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
    WHERE meta_key = 'rating'
    AND comment_post_ID = $post->ID
    AND comment_approved = '1'
");

if ( $count > 0 ) {

    $average = number_format($rating / $count, 2);
    echo '<div class="clear"></div>';
    echo '<div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating" style="margin-bottom:20px;">';
    //echo '<div class="star-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">';
    
    echo '<div class="star-rating onsale" title="'.sprintf(__('Rated %s out of 5', 'woocommerce'), $average).'">';
            
    echo '<span style="width:'.($average*20).'%">';
    
    echo '<strong itemprop="ratingValue" class="rating">'.$average.'</strong> out of 5';
    
    echo '<span>';
    

    //echo '<span class="rating" title="'.sprintf(__('Rated %s out of 5', 'woocommerce'), $average).'"><span style="width:'.($average*16).'px"><span itemprop="ratingValue" class="rating">'.$average.'</span> </span></span>';

    echo '</div>';
    echo '('.$count. " Reviews)";
    echo '</div>';
    
  
    }
    
    echo '<div class="clear"></div>';

}

//add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
 
//function woo_remove_product_tabs( $tabs ) {
 
    //unset( $tabs['description'] );      	// Remove the description tab
    //unset( $tabs['reviews'] ); 			// Remove the reviews tab
    //unset( $tabs['additional_information'] );  	// Remove the additional information tab
 
  //  return $tabs;
 
//}

?>

<?php
	// included Chosen.js:
function my_scripts_method() {

	wp_enqueue_script (
		'chosen-jquery',
		get_stylesheet_directory_uri() . '/js/chosen.jquery.js',
		array( 'jquery' )
	);

}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );


function child_manage_woocommerce_styles() {
  remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

    wp_dequeue_script( 'wc-chosen' );

}

/**
     * Return an ID of an attachment by searching the database with the file URL.
     *
     * First checks to see if the $url is pointing to a file that exists in
     * the wp-content directory. If so, then we search the database for a
     * partial match consisting of the remaining path AFTER the wp-content
     * directory. Finally, if a match is found the attachment ID will be
     * returned.
     *
     * @return {int} $attachment
     */

    function get_attachment_id_by_url($url) {

        // Split the $url into two parts with the wp-content directory as the separator.
        $parse_url = explode(parse_url(WP_CONTENT_URL, PHP_URL_PATH), $url);

        // Get the host of the current site and the host of the $url, ignoring www.
        $this_host = str_ireplace('www.', '', parse_url(home_url(), PHP_URL_HOST));
        $file_host = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));

        // Return nothing if there aren't any $url parts or if the current host and $url host do not match.
        if (!isset($parse_url[1]) || empty($parse_url[1]) || ( $this_host != $file_host )) {
            return;
        }

        // Now we're going to quickly search the DB for any attachment GUID with a partial path match.
        // Example: /uploads/2013/05/test-image.jpg
        global $wpdb;

        $prefix = $wpdb->prefix;
        $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM " . $prefix . "posts WHERE guid RLIKE %s;", $parse_url[1]));

        // Returns null if no attachment is found.
        return $attachment[0];
    }

    
    /* Function feeding_urls
     * Arguments feed_url(text file path), $data (data from textarea), $save_path (csv file path to save)
     * return void
     */
    function feeding_urls($feed_url, $data, $save_path) {
        $images = array();
        $i = 0;
        if (!empty($feed_url)) { // if data is passed into text file.
            if (file_exists($feed_url)) {
                $file = fopen($feed_url, "r") or exit("Unable to open file!");
                while (!feof($file)) {
                    $image_url = fgets($file);
                    $image_id = get_attachment_id_by_url(trim($image_url));
                    $images[$i]['id'] = $image_id;
                    $images[$i]['url'] = $image_url;
                    $i++;
                }
                fclose($file);
            } else {
                print 'File doesn\'t exist';
            }
        } else { 
            $text = trim($data);
            $urls = explode(PHP_EOL, $text);
            foreach ($urls as $url) {
                $id = get_attachment_id_by_url(trim($url));
                $images[$i]['id'] = $id;
                $images[$i]['url'] = $url;
                $i++;
            }
        }
        
        // saving data in csv file        
        $file = fopen($save_path, "w");
        foreach ($images as $image) {
            fputcsv($file, $image);
        }
        fclose($file);        
    }

if(!is_admin()){

// filter price HTML on loop & single product pages
//add_filter( 'woocommerce_get_price_html', 'get_price_html_custom', 10, 2 );
add_filter( 'woocommerce_variation_price_html','get_price_html_custom', 10, 2 );
add_filter( 'woocommerce_variation_sale_price_html', 'get_price_html_custom' , 10, 2 );

function get_price_html_custom($price_html, $product){

    $regular_price =  $product->regular_price;      //saving the product regular price
    $sale_price = $product->sale_price;      //saving the product sale price

    $msrp_price = get_post_meta($product->id,'_msrp_price',true);      //saving the product msrp price

    /*
     * Checking if the product has minimum advertised price using the plugin class
     * This means the WC_Minimum_Advertised_Price plugin should be installed.
     */

    if ( ! WC_Minimum_Advertised_Price_Frontend::product_has_map( $product ) || ! $product->is_purchasable() )
        $map_price = "";
    else
        $map_price = WC_Minimum_Advertised_Price_Frontend::get_product_map( $product );

    //Flow chart translated into the code logic

    if($sale_price!="" && $sale_price > $map_price ){
        $price_html =  '<del class="woocommerce-minimum-advertised-price">' . woocommerce_price( $regular_price ) . '</del>'. '<ins><span class="amount">' .woocommerce_price( $sale_price ) .'</span></ins>' ; //. $this->get_map_label_html( $product )
    } else  if($sale_price=="" || empty($sale_price)){
        if($regular_price > $map_price && $msrp_price!=""){
            $price_html =  '<del class="woocommerce-minimum-advertised-price">' . woocommerce_price( $msrp_price ) . '</del>'. '<ins><span class="amount">'. woocommerce_price( $regular_price ) .'</span></ins>'; //. $this->get_map_label_html( $product )
        } else {
            $price_html = "<span class='price-in-cart'>See price in cart</span>";
        }
    }else {
        $price_html = "<span class='price-in-cart'>See price in cart</span>";
    }

    return $price_html; //The final price

}



global $woocommerce_msrp_frontend;      //Getting the msrp_frontend class to remove the msrp actions

    /*
     * MSRP actions are being removed so that msrp will not be shown on any of the single product page.
     */
remove_action( 'woocommerce_single_product_summary', array( $woocommerce_msrp_frontend, 'show_msrp' ), 7 );
remove_action( 'woocommerce_available_variation', array( $woocommerce_msrp_frontend, 'add_msrp_to_js' ), 10, 3 );
remove_action( 'woocommerce_grouped_product_list_before_price', array( $woocommerce_msrp_frontend, 'show_grouped_msrp' ), 9);


}


?>

