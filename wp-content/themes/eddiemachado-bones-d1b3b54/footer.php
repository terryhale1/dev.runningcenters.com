<!--! Start of #footer -->
<div class="footer">

    <div class="wrapper">
        
            
        <div class="boot-image"><!-- <img src="/wp-content/themes/eddiemachado-bones-d1b3b54/images/footer-boot.png" alt=""> --> 
        <div class="footer-menu">
            <?php if(is_dynamic_sidebar('footer-sidebar')): ?>

                <?php dynamic_sidebar('footer-sidebar'); ?>

            <?php endif; ?>
        
        </div></div>

         
 



            <div class="emailwrapper">
            <div class="email-list">
                <h2>join our email list</h2>
                <div class="bg-email-list">

                    <span class="email-col">
                        <input type="checkbox" value="" id="" class="checkbox-email" >
                        <label>ONLINE</label>
                    </span>
                    <span class="email-col">
                        <input type="checkbox" value="" id="" class="checkbox-email" >
                        <label>CARLSBAD</label>
                    </span>
                    <span class="email-col">
                        <input type="checkbox" value="" id="" class="checkbox-email" >
                        <label>TEMECULA</label>
                    </span>
                    <span class="email-col">
                        <input type="checkbox" value="" id="" class="checkbox-email" >
                        <label>REDLANDS</label>
                    </span>

                    <input type="text" value="search" id="" class="input-email" >
                    <input type="submit" value="submit" id="" class="submit-email" >

                </div>
            </div> 
        </div>
    </div>

</div>
<div style="clear:both;"></div>
<div class="wrapper">
  <div class="cards-row">
                <span class="card"><img src="<?php bloginfo('template_url'); ?>/images/card-visa.png" alt="" title=""></span>
                <span class="card"><img src="<?php bloginfo('template_url'); ?>/images/card-master.png" alt="" title=""></span>
                <span class="card"><img src="<?php bloginfo('template_url'); ?>/images/card-amex.png" alt="" title=""></span>
                <span class="card"><img src="<?php bloginfo('template_url'); ?>/images/card-d.png" alt="" title=""></span>
                <span class="card"><img src="<?php bloginfo('template_url'); ?>/images/card-paypal.png" alt="" title=""></span>
            </div>

</div>


<div class="wrapper">

<div style="clear:both;"></div>


</div> 
<div class="wrapper">
<div class="copyrighted"><div><p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p></div></div>  
</div>
<!--! End of #footer -->
<?php wp_footer(); ?>
<script type='text/javascript' src='<?php echo bloginfo('template_url'); ?>/js/jquery.contenthover.js'></script>  
<script type='text/javascript' src='<?php echo bloginfo('template_url'); ?>/js/jquery.accordion.js'></script>  
<script type='text/javascript' src='<?php echo bloginfo('template_url'); ?>/js/jquery.cookie.js'></script>  
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
    jQuery(document).ready(function() {
        
        jQuery("a.zoom").prettyPhoto({social_tools:!1,theme:"pp_woocommerce",horizontal_padding:40,opacity:.9});

        jQuery("#ui-accordion-accordion-panel-2").find("ul").prepend("<li class='clearfix'></li>");
        jQuery("#ui-accordion-accordion-panel-2").find("ul").append("<li class='clearfix'></li>");
        jQuery("#ui-accordion-accordion-panel-3").find("ul").prepend("<li class='clearfix'></li>");
        jQuery("#ui-accordion-accordion-panel-3").find("ul").append("<li class='clearfix'></li>");
        jQuery("#price_filter-2").find("h3").attr("id", "openfirst");
        jQuery("#price_filter-2").find("h3").removeClass("accordion").addClass("accordion1");
        jQuery('.accordion1').accordion({defaultOpen: 'openfirst'});
        jQuery('#openfirst').removeClass('accordion-close').addClass("accordion-open");
        jQuery('#openfirst').next().show();

        setInterval(function() {
            jQuery('.prod-imge').each(function(i) {
                jQuery(this).contenthover({data_selector: '.product-detail', overlay_height: 100, effect: 'slide', slide_direction: 'bottom'});
            });
            if (jQuery('.widget_layered_nav').find("h3").attr("class") == "pod-head accordion") {
                jQuery('.accordion').accordion({
                    speed: 'slow',
                    animateOpen: function(elem, opts) { //replace the standard slideUp with custom function
                        elem.next().slideFadeToggle(opts.speed);
                    },
                    animateClose: function(elem, opts) { //replace the standard slideDown with custom function
                        elem.next().slideFadeToggle(opts.speed);
                    }
                });
                //custom animation for open/close
                jQuery.fn.slideFadeToggle = function(speed, easing, callback) {
                    return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
                };

            }
            jQuery('.widget_layered_nav').each(function(i) {

                if (!jQuery(this).hasClass("mens-size") && (jQuery(this).find("h3").text() == "Mens Size" || jQuery(this).find("h3").text() == "Womens Size")) {
                    jQuery(this).addClass("mens-size");
                } else
                if (!jQuery(this).hasClass("other-size") && !jQuery(this).hasClass("mens-size")) {
                    jQuery(this).addClass("other-size");
                }

            });

        }, 800);

    });

</script>

  <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      jQuery(selector).chosen(config[selector]);
    }
  </script>

  <script type="text/javascript">

jQuery( document ).ready(function() {
  // Handler for .ready() called.
  jQuery("#asideFix").show();
 
});

  </script>


</body>
</html>