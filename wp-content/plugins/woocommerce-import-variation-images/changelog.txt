*** Professor Cloud Changelog ***

2013.06.05 - version 2.0.1
 * Added an additional check to stop the function earlier if not on the single product page
 * Removed stray , - IE fix

2013.06.03 - version 2.0.0
 * Removed TimThumb
 * Removed IE6 support
 * Compatible with jQuery 1.9 / WP3.6
 * Image Zoom now added using JavaScript for better theme compatibility
 * Code tidy
 * 'Click For Larger' now filterable to remove completely or modify
 * Updated Mobile Detect script
 * Fix bug in mobile detection

2013.03.18 - version 1.7.9
 * Add Featured image to thumbnails

2013.03.18 - version 1.7.8
 * Add WC2 defaults to settings
 * General code tidy
 * Add featured image to thumbnail array
 * Fix support for Product Gallery Slider
 * Set Scaling default to False
 * Redirect to settings page on activation
 * Localization added
 * Support / Settings links
 * Add version to CSS and JavaScript
 * Change load priority

2013.03.10 - version 1.7.7
 * WC 2.0 Compat - prettyPhoto

2013.01.22 - version 1.7.6
 * WC 2.0 Compat
 * Update timthumb

2012.12.06 - version 1.7.5
 * Path fix after Mike broke it
 * Set $show_original

2012.12.04 - version 1.7.4
 * New updater

2012.10.01 - version 1.7.3
 * Compatibility with zoom plugin and swatches and photos

2012.08.25 - version 1.7.2
 * Fixes for WC 1.6.5
 * Smartened up the zoom CSS

2012.07.17 - version 1.7.1
 * WordPress MU fix

2012.05.30 - version 1.7
 * Incldued CSS can now be replaced by a theme version
 * TimThumb updated

2012.04.09 - version 1.6
 * Only include CSS and Script files on the Single Product page

2012.03.30 - version 1.5.1
 * Moved mobile detect init

2012.03.10 - version 1.5
 * 'Click for larger image' text now customisable in the admin
 * CSS fixes for better theme compatibility
 * Bug fix for Product Slider compatibility
 * Remove included fancybox and revert to WooComm FancyBox - licencing restrictions and unwanted behaviours
 * Put Cloud options in their own attribute - no more rel for me!

2012.03.08 - version 1.4 - not released
 * Tidy up thumbnails - only load the featured image as a thumbnail if required
 * Don't load Mobile_Detect if a theme has beaten us to it
 * Check the attribute image size and adjust things accordingly

2012.03.05 - version 1.3
 * Product Slider
 * Attribute Images
 * Thumbnail images

2011.11.10 - version 1.2
 * TimThumb added to allow scaling of zoom image
 * Woo plugin updater

2011.11.10 - version 1.0
 * First Release

 ToDo
 * Make FancyBox show a gallery rather than a single image
 * Make attribute reset link reset the image as well