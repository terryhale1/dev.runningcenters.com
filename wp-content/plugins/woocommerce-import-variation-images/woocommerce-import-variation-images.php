<?php
/*
Plugin Name: Woocommerce Import Variation Images
Plugin URI:
Description:Import the variation images
Version: 0.0.1
Author: VRAZER
Author URI: http://www.vrazer.com
*/

if (!function_exists('get_post_id_by_meta_key_and_like_value')) {
    /**
     * Get post id from meta key and value
     * @param string $key
     * @param mixed $value
     * @return int|bool
     */
    function get_post_id_by_meta_key_and_like_value($key, $value)
    {
        global $wpdb;
        echo $wpdb->escape($value);
        $meta = $wpdb->get_results("SELECT * FROM `" . $wpdb->postmeta . "` WHERE meta_key='" . $wpdb->escape($key) . "' AND meta_value like '%" . $wpdb->escape($value) . "%'");
        return $meta;
        /* if (is_array($meta) && !empty($meta) && isset($meta[0])) {
             $meta = $meta[0];
         }
         if (is_object($meta)) {
             return $meta->post_id;
         }
         else {
             return 0;
         }*/
    }
}

/*
 * Update all the products with swatches from import file.
 *
 */

function update_all_products_for_variation_images()
{

    $args = array(
        'post_type' => 'product',
        'post_parent' => 0, //only parent products because child products are variations
        'posts_per_page' => 10000,
    );

    $the_query = new WP_Query($args);

    while ($the_query->have_posts()) {
        $the_query->the_post();
        update_variation_images($the_query->post->ID);
    }

    return true;

}


/*
 * Show the form to update the option
 */

function show_woocommerce_import_variation_images_option_form()
{
    echo "<h2> Import Variation Images from Media Library Based on Sku </h2>";
    echo "<form name='update_option' method='post'>";
    echo "<input type='submit' name='submit' id='submit' value='Import Variation Images' >";
    echo "</form>";

    if (isset($_POST['submit'])) {

        if (update_all_products_for_variation_images()) {
            ?>
            <div class="success">
                <p><?php _e('All the images are set for the variations!'); ?></p>
            </div>
        <?php
        } else {
            ?>
        <div class="success">
                <p><?php _e('There is problem Importing variation images!'); ?></p>
        </div>
        <?php
        }

    }

}

/*
 * Function for updating swatch type options for each product.
 *
 */

function update_variation_images($product_id)
{

    $post_meta = get_post_meta($product_id);

    $args = array(
        'post_type' => 'product_variation',
        'post_parent' => $product_id,
        'posts_per_page' => 10000,
    );

    $child_posts = get_posts($args);

    $p_product = 0;

    foreach ($child_posts as $c_post) {

        $child_post_sku = get_post_meta($c_post->ID, '_sku', true);
        $attached_files = get_post_id_by_meta_key_and_like_value('_wp_attached_file', $child_post_sku . "-");
        $total_images = count($attached_files);
        if ($total_images > 0) {

            /*
             * If 04 image is present make it the thumbnail image
             * If it is not present make 01 image the thumbnail image for variation product
             */
            $additional_images = "";

            if ($total_images >= 4) {
                $thumbnail_image = $attached_files[3]->post_id;
                update_post_meta($c_post->ID, '_thumbnail_id', $thumbnail_image);
                if($p_product == 0) {
                    update_post_meta($product_id, '_thumbnail_id', $thumbnail_image);
                }
                $thumb_index = 3;

            } else {
                $thumbnail_image = $attached_files[0]->post_id;
                update_post_meta($c_post->ID, '_thumbnail_id', $thumbnail_image);
                if($p_product == 0) {
                    update_post_meta($product_id, '_thumbnail_id', $thumbnail_image);
                }
                $thumb_index = 0;
            }

            for ($i = 0; $i < $total_images; $i++) {
                if ($i != $thumb_index) {
                    $additional_images .= $attached_files[$i]->post_id . ",";
                    if($p_product==0){
                        $post_arr = array(
                            'ID' => $attached_files[$i]->post_id,
                            'post_parent' => $product_id
                        );
                        wp_update_post($post_arr);
                    }
                }
            }

            update_post_meta($c_post->ID, 'variation_image_gallery', $additional_images);
            if($p_product == 0) {
                update_post_meta($product_id, '_product_image_gallery', $additional_images);
            }

            $p_product++;

        }

    }

}

add_action('admin_menu', 'woocommerce_import_variation_images_menu');

function woocommerce_import_variation_images_menu()
{

    add_options_page('Import Variation Images', 'Import Variation Images', 'manage_options', 'import-variation-images.php', 'show_woocommerce_import_variation_images_option_form');
}