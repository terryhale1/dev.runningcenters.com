*** Shipment Tracking Changelog ***

2013.08.16 - version 1.1.8
 * Add Hermes

2013.07.15 - version 1.1.7
 * Update royal mail url

2013.06.04 - version 1.1.6
 * Fixed tracking link in plain text emails.

2013.04.19 - version 1.1.5
 * woocommerce_shipment_tracking_default_provider filter for setting a default

2013.03.11 - version 1.1.4
 * Added austrian services

2013.03.11 - version 1.1.3
 * Added Deutsche Post DHL

2013.03.08 - version 1.1.2
 * SAPO URL change

2013.01.15 - version 1.1.1
 * Change localisation strings

2013.01.11 - version 1.1.0
 * WC 2.0 Compatibility
 * Custom providers can have custom links and provider names.

2012.12.04 - version 1.0.5
 * New updater

2012.08.15 - version 1.0.4
 * PostNL support

2012.06.25 - version 1.0.3
 * Changed text for 'custom' links
 * Changed save priority

2012.06.25 - version 1.0.2
 * Added Correios
 * Added Posten AB

2012.06.06 - version 1.0.1
 * Added DTDC shipping

2012.05.17 - version 1.0
 * First Release