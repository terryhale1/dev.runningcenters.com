<?php
/*
Plugin Name: Woocommerce Import Swatches
Plugin URI:
Description:Import the swatches for variable products
Version: 0.0.1
Author: VRAZER
Author URI: http://www.vrazer.com
*/

if (!function_exists('get_post_id_by_meta_key_and_value')) {
    /**
     * Get post id from meta key and value
     * @param string $key
     * @param mixed $value
     * @return int|bool
     */
    function get_post_id_by_meta_key_and_value($key, $value) {
        global $wpdb;
        echo $wpdb->escape($value);
        $meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='".$wpdb->escape($key)."' AND meta_value='".$wpdb->escape($value)."'");
        print_r($meta);
        if (is_array($meta) && !empty($meta) && isset($meta[0])) {
            $meta = $meta[0];
        }
        if (is_object($meta)) {
            return $meta->post_id;
        }
        else {
            return 0;
        }
    }
}

/*
 * Update all the products with swatches from import file.
 *
 */

function update_all_products() {

    $import_file = get_option('woocommerce_import_file');

    if($import_file){

        $args = array (
            'post_type' => 'product',
            'post_parent' => 0              //only parent products because child products are variations
        );

        $the_query = new WP_Query( $args );

        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            update_swatch_type_options($the_query->post->ID,$import_file);
        }

    } else {
        function no_file_admin_notice() {
            ?>
            <div class="error">
                <p><?php _e( 'Import csv file id is not set!', 'my-text-domain' ); ?></p>
            </div>
        <?php
        }
        add_action( 'admin_notices', 'no_file_admin_notice' );
    }

}

/*
 * Update CSV file id option in the option table
 */

function update_csvfile_id($value){
    update_option('woocommerce_import_file',$value);
}

/*
 * Show the form to update the option
 */

function show_woocommerce_import_swatches_option_form(){
   echo "<h2> Enter Import Csv file ID From Media Library </h2>";
   echo "<form name='update_option' method='post'>";
   echo "<input type='text' name='file_id' id='file_id'>";

   echo "<input type='submit' name='submit' id='submit'>";
   echo "</form>";

    if($_POST['submit']){

        if(!empty($_POST['file_id'])){
            update_csvfile_id($_POST['file_id']);

            update_all_products();
        } else {
            ?>
                <div class="error">
                    <p><?php _e( 'Import csv file id is not set, Please enter ID of the file!'); ?></p>
                </div>
            <?php
            }

        }
}


/*
 * Function for updating swatch type options for each product.
 *
 */

function update_swatch_type_options($product_id,$file_id){

    $import_file_url = wp_get_attachment_url($file_id);

    $csv = readCSV($_SERVER['DOCUMENT_ROOT'].$import_file_url);

    $csv_final = array();

    for($row=0; $row<count($csv);$row++){
        if($row==0){

        } else {
            if($csv[$row]['0']!='' && $csv[$row]['1']!='') {
                $csv_final[$row-1][] = $csv[$row]['0'];
                $csv_final[$row-1][] = $csv[$row]['1'];
            }
        }
    }

    $swatch_options = get_post_meta($product_id,'_swatch_type_options',true);

    //Make an array for the swatches to add it into the _swatch_type_options meta
    $swatch_options_insert['pa_color']['type'] = 'product_custom';
    $swatch_options_insert['pa_color']['size'] = 'swatches_image_size';

    $terms = wp_get_post_terms($product_id,'pa_color');

   /* echo "<pre>";
    echo count($terms); echo"<br>";
    echo count($csv_final);echo"<br>";

    print_r($terms);echo"<br>";

    print_r($csv_final);echo"<br>";



   if(count($terms) != count($csv_final)){
        echo "Terms count and csv terms are not the same";
        exit;
    }*/

    $upload_path = wp_upload_dir();

    foreach($terms as $term){

        for($row=0;$row<count($csv_final);$row++){
            if($term->name == $csv_final[$row][1]){
                $swatch_options_insert['pa_color']['attributes'][$term->slug]['type'] = 'image';
                $swatch_options_insert['pa_color']['attributes'][$term->slug]['color'] = '#ffffff';

                $image_id = get_post_id_by_meta_key_and_value('_wp_attached_file',substr($upload_path['subdir'],1,strlen($upload_path['subdir']))."/".$csv_final[$row][0]."_swatch.jpg");
                $swatch_options_insert['pa_color']['attributes'][$term->slug]['image'] = $image_id;
            }
        }

    }

    $swatch_options['pa_color'] = $swatch_options_insert['pa_color'];

    update_post_meta($product_id,'_swatch_type','pickers');
    update_post_meta($product_id,'_swatch_type_options',$swatch_options);

}

/*
 * Read CSV file and return the array
 */
function readCSV($csvFile){
    $file_handle = fopen($csvFile, 'r');
    while (!feof($file_handle) ) {
        $line_of_text[] = fgetcsv($file_handle, 1024);
    }
    fclose($file_handle);
    return $line_of_text;
}

add_action('admin_menu','woocommerce_import_swatches_menu');

function woocommerce_import_swatches_menu(){

    add_options_page('Import Swatches for Products', 'Import Swatches', 'manage_options', 'import-swatches.php', 'show_woocommerce_import_swatches_option_form');
}