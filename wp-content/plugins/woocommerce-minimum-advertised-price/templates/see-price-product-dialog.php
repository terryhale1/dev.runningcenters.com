<?php
/**
 * WooCommerce Minimum Advertised Price
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Minimum Advertised Price to newer
 * versions in the future. If you wish to customize WooCommerce Minimum Advertised Price for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-minimum-advertised-price/ for more information.
 *
 * @package     WC-Minimum-Advertised-Price/Templates
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

/**
 * Product "See Price" Dialog content
 *
 * @param WC_Product $product the product
 * @param string $minimum_advertised_price the minimum advertised price
 * @param string $savings the savings string to display, if any
 *
 * @version 1.0
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$product_id = ( ! empty( $product->variation_id ) ) ? $product->variation_id : $product->id;

?>
<div style="display: none;" class="wc-map-dialog" id="wc-map-see-price-<?php echo esc_attr( $product_id ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
	<p class="price"><?php echo __( 'Advertised Price', WC_Minimum_Advertised_Price::TEXT_DOMAIN ) . ': <del>' . $minimum_advertised_price . '</del></p>'; ?>
	<p class="price"><?php echo __( 'Price', WC_Minimum_Advertised_Price::TEXT_DOMAIN ) . ': ' . woocommerce_price( $product->get_price() ) . '</p>'; ?>
	<?php if ( ! empty( $savings ) ) echo '<p class="price">' . $savings . '</p>'; ?>

	<?php if ( is_product() ) : ?>
		<br/>
		<button class="single_add_to_cart_button button alt"><?php _e( 'Add to cart', WC_Minimum_Advertised_Price::TEXT_DOMAIN ); ?></button>
	<?php else : ?>
		<br/>
		<a href="<?php echo esc_url( $product->add_to_cart_url() ); ?>" rel='nofollow' data-product_id="<?php echo esc_attr( $product_id ); ?>" data-product_sku="<?php echo esc_attr( $product->get_sku() ); ?>" class="add_to_cart_button button product_type_simple"><?php _e( 'Add to cart', WC_Minimum_Advertised_Price::TEXT_DOMAIN ); ?></a>
	<?php endif; ?>
</div>
