<?php
/**
 * WooCommerce Minimum Advertised Price
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Minimum Advertised Price to newer
 * versions in the future. If you wish to customize WooCommerce Minimum Advertised Price for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-minimum-advertised-price/ for more information.
 *
 * @package     WC-Minimum-Advertised-Price/Frontend
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * WooCommerce Minimum Advertised Price frontend handler
 *
 * @since 1.0
 */
class WC_Minimum_Advertised_Price_Frontend {


	/** @var string where to display actual price, either in cart only, or in cart/on gesture */
	private $display_location;


	/**
	 * Initializes the frontend
	 *
	 * @since 1.0
	 */
	public function __construct() {

		$this->display_location = get_option( 'wc_minimum_advertised_price_display_location' );

		// filter price HTML on loop & single product pages
		add_filter( 'woocommerce_get_price_html',            array( $this, 'get_price_html' ), 10, 2 );
		add_filter( 'woocommerce_variation_price_html',      array( $this, 'get_price_html' ), 10, 2 );
		add_filter( 'woocommerce_variation_sale_price_html', array( $this, 'get_price_html' ), 10, 2 );

		// for same-price product variations, add the price_html back into the datastructure for the frontend so we have a place to hook in and show our "See Price" action
		add_filter( 'woocommerce_available_variation',       array( $this, 'same_price_variation_price_html' ), 10, 3 );

		// show the 'See Price' action in the product loop
		add_action( 'woocommerce_after_shop_loop_item', array( $this, 'loop_add_see_price_action' ), 9 );

		// filter price HTML in cart
		add_filter( 'woocommerce_cart_item_price_html', array( $this, 'get_cart_item_price_html' ), 10, 2 );

		// load javascript/css to display dialog when 'see price' button is clicked
		if ( 'on_gesture' == $this->display_location ) {
			add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ) );
			// render frontend embedded styles
			add_action( 'wp_print_styles', array( $this, 'render_embedded_styles' ), 1 );
		}
	}


	/**
	 * Load scripts for displaying price in dialog box
	 *
	 * @since 1.0
	 */
	public function load_scripts() {
		global $woocommerce, $wp_scripts;

		// TODO: the drawback to this is that these scripts will not be made available to products loaded on pages via shortcodes.  I guess one solution would be to load scripts/styles into the page footer when *needed*?
		// only load on shop loop/single product pages
		if ( ! is_woocommerce() )
			return;

		// load jQuery UI Dialog
		wp_enqueue_script( 'jquery-ui-dialog' );

		// load jQuery UI CSS
		$jquery_version = isset( $wp_scripts->registered['jquery-ui-core']->ver ) ? $wp_scripts->registered['jquery-ui-core']->ver : '1.9.2';
		wp_enqueue_style( 'jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/' . $jquery_version . '/themes/smoothness/jquery-ui.css' );

		// add dialog handler
		$woocommerce->add_inline_js( "

			// init the dialogs when a variation is shown for variable products, otherwise init immediately
			if ( $( 'form.variations_form' ).length ) {
				$( 'form.variations_form').bind( 'show_variation', function() { wcMapDialogHandler() } );
			} else {
				wcMapDialogHandler();
			}

			// 'See Price' dialog handler
			function wcMapDialogHandler() {

				// setup dialogs
				$( '.wc-map-dialog' ).dialog( {
					autoOpen: false,
					draggable: false,
					resizable: false
				} );

				// add click handler
				$( 'a.wc-map-see-price' ).click( function( e ) {

					e.preventDefault();

					// set position for dialog and open it
					$( '#wc-map-see-price-' + $(this).data( 'product-id' ) ).dialog( 'option', 'position', { my: 'center bottom', at: 'center top', of: this } ).dialog( 'open' );

				} );

				// product page 'see price' -> 'add to cart' handler which submits the product page form
				$( '.wc-map-dialog .single_add_to_cart_button' ).click( function() {
					$( '.cart' ).submit();
				} );
			}
		");
	}


	/**
	 * Renders the styles
	 *
	 * @since 1.0
	 */
	public function render_embedded_styles() {

		// only load on shop loop/single product pages
		if ( ! is_woocommerce() )
			return;

		// add some CSS to correct alignment issues
		echo '<style type="text/css">.woocommerce ul.products li.product a.wc-map-see-price { margin-bottom: 5px; } .wc-map-dialog a.added_to_cart { margin-left: 30px; }</style>';
	}


	/**
	 * Return the minimum advertised price html if a MAP is set for shop loop/single product pages. The MAP will be displayed
	 * with a strike-through, followed by the globally configured MAP label (label is added only on single product pages,
	 * since the "see price in cart" button if gesture's
	 *
	 * @since 1.0
	 * @param string $price_html the price html
	 * @param WC_Product $product the product
	 * @return string the price html
	 */
	public function get_price_html( $price_html, $product ) {

		// return regular price if a MAP isn't set
		if ( ! self::product_has_map( $product ) || ! $product->is_purchasable() )
			return $price_html;

		// get the MAP
		$map = self::get_product_map( $product );

		if ( $product->is_type( 'simple' ) || $product->is_type( 'variation' ) ) {

			$price_html = '<del class="woocommerce-minimum-advertised-price">' . woocommerce_price( $map ) . '</del>' . $this->get_map_label_html( $product );

		} elseif ( $product->is_type( 'variable' ) ) {

			// two different cases: a price range, or all the same price
			if ( $product->min_minimum_advertised_price == $product->max_minimum_advertised_price ) {
				$price_html =  '<del class="woocommerce-minimum-advertised-price">' . woocommerce_price( $product->min_minimum_advertised_price ) . '</del>' . $this->get_map_label_html( $product );
			} else {
				$price_html =  $product->get_price_html_from_text() . '<del class="woocommerce-minimum-advertised-price">' . woocommerce_price( $product->min_minimum_advertised_price ) . '</del>' . $this->get_map_label_html( $product );
			}

		}

		return $price_html;
	}


	/**
	 * Renders the "See Price" button in the product loop for simple MAP products
	 *
	 * @since 1.0
	 */
	public function loop_add_see_price_action() {
		global $product;

		// return if a MAP isn't set or the product is not purchasable, or not simple
		if ( ! self::product_has_map( $product ) || ! $product->is_purchasable() || ! $product->is_type( 'simple' ) )
			return;

		echo $this->get_map_see_price_html( $product );
	}


	/**
	 * If we're dealing with a variable product with all variations of the same
	 * price, by default the individual product variations price_html strings
	 * will not be included onto the page (since they're all the same).  However,
	 * we need them to hook onto and display our "See Price" action.
	 *
	 * @since 1.0
	 * @param array $available_variation the variation page data
	 * @param WC_Product_Variable $variable the variable product object
	 * @param WC_Product_Variation $variation the product variation object
	 * @return array the variation page data, with the price_html injected back in if need be
	 */
	public function same_price_variation_price_html( $available_variation, $variable, $variation ) {

		// if this variable product has a map price, and all the same prices, inject the variation price_html back in so we can hook onto it
		if ( ! $available_variation['price_html'] && 'on_gesture' == $this->display_location && self::product_has_map( $variable ) && $variable->min_variation_price == $variable->max_variation_price ) {
			$available_variation['price_html'] = '<span class="price">' . $variation->get_price_html() . '</span>';
		}

		return $available_variation;
	}


	/**
	 * Render the minimum advertised price html if a MAP is set for products on the cart page. The MAP will be displayed
	 * with a strike-through, followed by an optional "You save X.XX" label if enabled
	 *
	 * @since 1.0
	 * @param string $price_html the price html
	 * @param array $values the cart item values
	 * @return string the price html
	 */
	public function get_cart_item_price_html( $price_html, $values ) {

		global $wc_minimum_advertised_price;

		// return regular price if a MAP isn't set, or no regular price
		if ( ! self::product_has_map( $values['data'] ) )
			return $price_html;

		$map = self::get_product_map( $values['data'] );

		$price_html = sprintf( '<del>%s</del>&nbsp;%s', woocommerce_price( $map ), $price_html );

		if ( $wc_minimum_advertised_price->show_savings() )
			$price_html .= apply_filters( 'wc_minimum_advertised_price_savings_html', '<br/>'. __( 'You save', WC_Minimum_Advertised_Price::TEXT_DOMAIN) . ': ' . woocommerce_price( $map - $values['data']->get_price() ), $map, $values );

		return $price_html;
	}


	/** Helper methods ******************************************************/


	/**
	 * Checks if the given product has a minimum advertised price set. For variable products this will return true if
	 * at least one variation has a MAP set.
	 *
	 * @since 1.0
	 * @param object $product \WC_Product object
	 * @return bool true if the product has MAP set, false otherwise
	 */
	public static function product_has_map( $product ) {

		if ( $product->is_type( 'simple' ) ) {

			return ( isset( $product->minimum_advertised_price ) && is_numeric( $product->minimum_advertised_price ) );

		} elseif ( $product->is_type( 'variable' ) ) {

			return ( isset( $product->min_minimum_advertised_price ) && is_numeric( $product->min_minimum_advertised_price ) );

		} elseif ( $product->is_type( 'variation' ) ) {

			return ( isset( $product->product_custom_fields['_minimum_advertised_price'][0] ) && is_numeric( $product->product_custom_fields['_minimum_advertised_price'][0] ) );
		}

		return false;
	}


	/**
	 * Returns the minimum advertised price set for the given product
	 *
	 * @since 1.0
	 * @param object $product \WC_Product object
	 * @return string the MAP
	 */
	public static function get_product_map( $product ) {

		if ( $product->is_type( 'simple' ) ) {

			return $product->minimum_advertised_price;

		} elseif ( $product->is_type( 'variation' ) ) {

			return $product->product_custom_fields['_minimum_advertised_price'][0];
		}

		return;
	}


	/**
	 * Returns the label displayed next to the MAP:  if the display location is
	 * "in cart" or the product is a variable type, this will return the
	 * "See price in cart" text.  Otherwise, if the display location is
	 * "on gesture" and we're on the product page, the "See Price" link will be returned.
	 *
	 * @since 1.0
	 * @param object $product \WC_Product instance passed to gesture label filter
	 * @return string the configured minimum advertised price label
	 */
	private function get_map_label_html( $product ) {

		$label_html = '';

		if ( 'in_cart' == $this->display_location || $product->is_type( 'variable' ) ) {

			$label_html = '&nbsp;<span class="wc-map-label">' . get_option( 'wc_minimum_advertised_price_label' ) . '</span>';

		} elseif ( 'on_gesture' == $this->display_location && is_product() ) {

			$label_html = $this->get_map_see_price_html( $product );

		}

		return apply_filters( 'wc_minimum_advertised_price_label_html', $label_html, $product, $this->display_location );
	}


	/**
	 * Returns the "See Price" action html
	 *
	 * @since 1.0
	 * @param object $product \WC_Product instance passed to gesture label filter
	 * @return string the see price action html
	 */
	private function get_map_see_price_html( $product ) {

		$action_html = '';

		if ( 'on_gesture' == $this->display_location && $product->is_purchasable() && $product->is_in_stock() ) {

			// add button (on shop loop) or link (on single product page) to display dialog
			$action_html = sprintf( ' <a href="#" rel="nofollow" data-product-id="%s" class="wc-map-see-price%s">%s</a>',
				( ! empty( $product->variation_id ) ) ? $product->variation_id : $product->id,
				( ! is_product() ) ? ' button' : '',
				__( 'See Price', WC_Minimum_Advertised_Price::TEXT_DOMAIN )
			);

			// add dialog HTML
			$action_html .= $this->get_dialog_html( $product );
		}

		return apply_filters( 'wc_minimum_advertised_price_see_price_html', $action_html, $product, $this->display_location );
	}


	/**
	 * Return the HTML for the dialog shown when 'See Price' is clicked
	 *
	 * @since 1.0
	 * @param object $product the \WC_Product object
	 * @return string the dialog HTML
	 */
	private function get_dialog_html( $product ) {

		ob_start();

		woocommerce_minimum_advertised_price_see_price_product_dialog( $product );

		return ob_get_clean();
	}


} // end \WC_Minimum_Advertised_Price_Frontend class
