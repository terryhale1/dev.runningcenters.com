<?php
/**
 * WooCommerce Minimum Advertised Price
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Minimum Advertised Price to newer
 * versions in the future. If you wish to customize WooCommerce Minimum Advertised Price for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-minimum-advertised-price/ for more information.
 *
 * @package     WC-Minimum-Advertised-Price/Admin
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * WooCommerce Minimum Advertised Price admin handler.  Loads all Minimum
 * Advertised Price product data panels and modifications for WooCommerce
 * general settings.
 *
 * @since 1.0
 */
class WC_Minimum_Advertised_Price_Admin {


	/**
	 * Initialize the admin, adding actions to properly display and handle
	 * the minimum advertised price fields
	 * @since 1.0
	 */
	public function __construct() {

		// add general settings to WooCommerce > Settings > Catalog
		add_filter( 'woocommerce_catalog_settings', array( $this, 'add_global_settings' ) );

		/* Simple Product Hooks */

		// display the simple product meta field
		add_action( 'woocommerce_product_options_pricing', array( $this, 'add_map_field_to_simple_product' ) );

		// save the simple product meta field
		add_action( 'woocommerce_process_product_meta',    array( $this, 'save_simple_product_map' ) );

		/* Variable Product Hooks */

		// adds the product variation 'MAP' bulk edit action
		add_action( 'woocommerce_variable_product_bulk_edit_actions', array( $this, 'add_variable_product_bulk_edit_map_action' ) );

		// add MAP field to variable products under the 'Variations' tab after the shipping class dropdown
		add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'add_map_field_to_variable_product' ), 15, 2 );

		// save the MAP field for variable products
		add_action( 'woocommerce_save_product_variation', array( $this, 'save_variable_product_map' ) );

		// save the lowest/highest MAP for variable products
		add_action( 'woocommerce_process_product_meta_variable', array( $this, 'save_min_max_variable_product_map' ) );

		/* Product list bulk edit hooks */

		// add Products list table MAP bulk edit field
		add_action( 'woocommerce_product_bulk_edit_end', array( $this, 'add_map_field_bulk_edit' ) );

		// save Products List table MAP bulk edit field
		add_action( 'woocommerce_product_bulk_edit_save', array( $this, 'save_map_field_bulk_edit' ) );
	}


	/** Global Configuration ******************************************************/


	/**
	 * Inject global settings into the Settings > Catalog page, immediately after the 'Pricing Options' section
	 *
	 * @since 1.0
	 * @param array $settings associative array of WooCommerce settings
	 * @return array associative array of WooCommerce settings
	 */
	public function add_global_settings( $settings ) {

		$updated_settings = array();

		foreach ( $settings as $setting ) {

			$updated_settings[] = $setting;

			if ( isset( $setting['id'] ) && 'pricing_options' === $setting['id']
			  && isset( $setting['type'] ) && 'sectionend' === $setting['type'] ) {
				$updated_settings = array_merge( $updated_settings, $this->get_global_settings() );
			}
		}

		return $updated_settings;
	}


	/**
	 * Returns the global settings array for the plugin
	 *
	 * @since 1.0
	 * @return array the global settings
	 */
	public static function get_global_settings() {

		return apply_filters( 'wc_minimum_advertised_price_global_settings', array(

			// section start
			array(
				'title' => __( 'Minimum Advertised Pricing Options', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				'type'  => 'title',
				'id'    => 'wc_minimum_advertised_price_title',
			),

			// Where to display price
			array(
				'title'    => __( 'Display Actual Price', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				'desc_tip' => __( 'Where to display the actual price: in the cart only, or in the cart and also in a popover on the catalog / product page when the "Show Price" button is clicked', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				'type'     => 'select',
				'id'       => 'wc_minimum_advertised_price_display_location',
				'default'  => 'in_cart',
				'options'  => array(
					'in_cart'     => __( 'In Cart Only', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
					'on_gesture'  => __( 'In Cart / On Gesture', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				),
			),

			// Text to display next to the MAP
			array(
				'title'    => __( 'Minimum Advertised Price Label', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				'desc_tip' => __( 'Change the text displayed next to the minimum advertised price on loop/single product pages', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				'type'     => 'text',
				'id'       => 'wc_minimum_advertised_price_label',
				'default'  => __( 'See Price in Cart', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
			),

			// Show savings
			array(
				'title'   => __( 'Show Savings', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				'id'      => 'wc_minimum_advertised_price_show_savings',
				'desc'    => __( 'Show a message with the savings when displaying the actual price', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				'default' => 'no',
				'type'    => 'checkbox',
			),

			// section end
			array( 'type' => 'sectionend', 'id'   => 'wc_minimum_advertised_price_title', ),

		) );
	}


	/** Product Configuration ******************************************************/


	/**
	 * Display our simple product Minimum Advertised Pricing field
	 *
	 * @since 1.0
	 */
	public function add_map_field_to_simple_product() {

		// add MAP
		woocommerce_wp_text_input( array(
				'id'                => '_minimum_advertised_price',
				'class'             => 'wc_input_price short',
				'label'             => __( 'MAP', WC_Minimum_Advertised_Price::TEXT_DOMAIN ) . ' (' . get_woocommerce_currency_symbol() . ')',
				'desc_tip'          => __( 'The Minimum Advertised Price (or MAP), set this to hide the Regular Price unless the product is added to the cart', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
				'type'              => 'number',
				'custom_attributes' => array(
					'step' => 'any',
					'min'  => '0',
				),
			)
		);
	}


	/**
	 * Save our simple product meta fields
	 *
	 * @since 1.0
	 */
	public function save_simple_product_map( $post_id ) {

		// avoid creating empty map entries on non-map products
		if ( ! empty( $_POST['_minimum_advertised_price'] ) )
			update_post_meta( $post_id, '_minimum_advertised_price', stripslashes( $_POST['_minimum_advertised_price'] ) );
		else
			delete_post_meta( $post_id, '_minimum_advertised_price' );
	}


	/**
	 * Renders the 'MAP' bulk edit action on the product admin Variations tab.
	 * There is core JS code that automatically handles these bulk edits.
	 *
	 * @since 1.0
	 */
	public function add_variable_product_bulk_edit_map_action() {
		echo '<option value="variable_minimum_advertised_price">' . __( 'MAP', WC_Minimum_Advertised_Price::TEXT_DOMAIN ) . '</option>';
	}


	/**
	 * Add MAP field to variable products under the 'Variations' tab after the shipping class dropdown
	 *
	 * @since 1.0
	 */
	public function add_map_field_to_variable_product( $loop, $variation_data ) {

		$map = ( isset( $variation_data['_minimum_advertised_price'][0] ) ) ? $variation_data['_minimum_advertised_price'][0] : '';

		?>
		<tr>
			<td>
				<label><?php printf( __( 'MAP: (%s)', WC_Minimum_Advertised_Price::TEXT_DOMAIN ), esc_html( get_woocommerce_currency_symbol() ) ); ?></label>
				<input type="number" size="5" name="variable_minimum_advertised_price[<?php echo esc_attr( $loop ); ?>]" value="<?php echo esc_attr( $map ); ?>" step="any" min="0" placeholder="<?php _e( 'Variation MAP', WC_Minimum_Advertised_Price::TEXT_DOMAIN ); ?>" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<?php
	}


	/**
	 * Save our variable product meta fields
	 *
	 * @since 1.0
	 */
	public function save_variable_product_map( $variation_id ) {

		// find the index for the given variation ID and save the associated MAP
		$index = array_search( $variation_id, $_POST['variable_post_id'] );

		if ( false !== $index ) {
			if ( ! empty( $_POST['variable_minimum_advertised_price'][ $index ] ) )
				update_post_meta( $variation_id, '_minimum_advertised_price', stripslashes( $_POST['variable_minimum_advertised_price'][ $index ] ) );
			else
				delete_post_meta( $variation_id, '_minimum_advertised_price' );
		}
	}


	/**
	 * Save the minimum & maximum variation MAP prices to the parent variable product
	 *
	 * @since 1.0
	 * @param int $post_id the post ID of the variable product
	 */
	public function save_min_max_variable_product_map( $post_id ) {

		$lowest_minimum_advertised_price = $highest_minimum_advertised_price = '';

		if ( is_array( $_POST['variable_minimum_advertised_price'] ) ) {
			foreach ( $_POST['variable_minimum_advertised_price'] as $map ) {

				if ( ! is_numeric( $lowest_minimum_advertised_price ) || ( is_numeric( $map ) && $map < $lowest_minimum_advertised_price ) )
					$lowest_minimum_advertised_price = $map;

				if ( ! is_numeric( $highest_minimum_advertised_price ) || ( is_numeric( $map ) && $map > $highest_minimum_advertised_price ) )
					$highest_minimum_advertised_price = $map;

			}
		}

		if ( is_numeric( $lowest_minimum_advertised_price ) || is_numeric( $highest_minimum_advertised_price ) ) {
			update_post_meta( $post_id, '_min_minimum_advertised_price', $lowest_minimum_advertised_price );
			update_post_meta( $post_id, '_max_minimum_advertised_price', $highest_minimum_advertised_price );
		} else {
			// don't create the min/max records for non-map variable products
			delete_post_meta( $post_id, '_min_minimum_advertised_price' );
			delete_post_meta( $post_id, '_max_minimum_advertised_price' );
		}
	}


	/**
	 * Add a MAP bulk edit field, this is displayed on the Products list page
	 * when one or more products is selected, and the Edit Bulk Action is applied
	 *
	 * @since 1.0
	 */
	public function add_map_field_bulk_edit() {
		?>
			<div class="inline-edit-group">
				<label class="alignleft">
					<span class="title"><?php _e( 'MAP', WC_Minimum_Advertised_Price::TEXT_DOMAIN ); ?></span>
					<span class="input-text-wrap">
						<select class="change_minimum_advertised_price change_to" name="change_minimum_advertised_price">
						<?php
							$options = array(
								''  => __( '— No Change —', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
								'1' => __( 'Change to:', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
								'2' => __( 'Increase by (fixed amount or %):', WC_Minimum_Advertised_Price::TEXT_DOMAIN ),
								'3' => __( 'Decrease by (fixed amount or %):', WC_Minimum_Advertised_Price::TEXT_DOMAIN )
							);
							foreach ( $options as $key => $value ) {
								echo '<option value="' . esc_attr( $key ) . '">' . esc_html( $value ) . '</option>';
							}
						?>
						</select>
					</span>
				</label>
				<label class="alignright">
					<input type="text" name="_minimum_advertised_price" class="text minimum_advertised_price" placeholder="<?php _e( 'Enter MAP:', WC_Minimum_Advertised_Price::TEXT_DOMAIN ); ?>" value="" />
				</label>
			</div>
		<?php
	}


	/**
	 * Save the MAP bulk edit field
	 *
	 * @since 1.0
	 */
	public function save_map_field_bulk_edit( $product ) {

		if ( ! empty( $_REQUEST['change_minimum_advertised_price'] ) ) {

			$option_selected      = absint( $_REQUEST['change_minimum_advertised_price'] );
			$requested_map_change = stripslashes( $_REQUEST['_minimum_advertised_price'] );
			$current_map_value    = get_post_meta( $product->id, '_minimum_advertised_price', true );

			switch ( $option_selected ) {

				// change MAP to fixed amount
				case 1 :
					$new_map = $requested_map_change;
					break;

				// increase MAP by fixed amount/percentage
				case 2 :
					if ( false !== strpos( $requested_map_change, '%' ) ) {
						$percent = str_replace( '%', '', $requested_map_change ) / 100;
						$new_map = $current_map_value + ( $current_map_value * $percent );
					} else {
						$new_map = $current_map_value + $requested_map_change;
					}
					break;

				// decrease MAP by fixed amount/percentage
				case 3 :
					if ( false !== strpos( $requested_map_change, '%' ) ) {
						$percent = str_replace( '%', '', $requested_map_change ) / 100;
						$new_map = $current_map_value - ( $current_map_value * $percent );
					} else {
						$new_map = $current_map_value - $requested_map_change;
					}
				break;
			}

			// update to new MAP if different than current MAP
			if ( isset( $new_map ) && $new_map != $current_map_value )
				update_post_meta( $product->id, '_minimum_advertised_price', $new_map );
		}
	}


} // end \WC_Minimum_Advertised_Price_Admin class
