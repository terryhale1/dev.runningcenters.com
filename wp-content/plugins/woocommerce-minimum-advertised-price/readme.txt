=== WooCommerce Minimum Advertised Price ===
Author: skyverge
Tags: woocommerce
Requires at least: 3.5
Tested up to: 3.5.1
Requires WooCommerce at least: 2.0
Tested WooCommerce up to: 2.0.10

Easily set a minimum advertised price and hide the regular price until the product is added to the cart

See http://docs.woothemes.com/document/woocommerce-minimum-advertised-price/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-minimum-advertised-price' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
