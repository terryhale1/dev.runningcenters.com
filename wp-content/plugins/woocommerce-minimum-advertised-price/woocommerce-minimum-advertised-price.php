<?php
/**
 * Plugin Name: WooCommerce Minimum Advertised Price
 * Plugin URI: http://www.woothemes.com/products/woocommerce-minimum-advertised-price/
 * Description: Easily set a minimum advertised price and hide the regular price until the product is added to the cart
 * Author: SkyVerge
 * Author URI: http://www.skyverge.com
 * Version: 1.0
 * Text Domain: wc-minimum-advertised-price
 * Domain Path: /languages/
 *
 * Copyright: (c) 2013 SkyVerge (info@skyverge.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Minimum-Advertised-Price
 * @author    SkyVerge
 * @category  Plugin
 * @copyright Copyright (c) 2013, SkyVerge
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '7a296466c278db3913f7e3bed2a160d1', '201953' );

// Check if WooCommerce is active
if ( ! is_woocommerce_active() )
	return;

/**
 * The WC_Minimum_Advertised_Price global object
 * @name $wc_minimum_advertised_price
 * @global WC_Minimum_Advertised_Price $GLOBALS['wc_minimum_advertised_price']
 */
$GLOBALS['wc_minimum_advertised_price'] = new WC_Minimum_Advertised_Price();


/**
 * # WooCommerce Minimum Advertised Price Main Plugin Class
 *
 * ## Plugin Overview
 *
 * This plugin adds a new pricing field named MAP to simple and variation products,
 * which when set with a numeric value, will be shown on the frontend with a
 * strikethrough in place of the true product price, along with either a message to
 * "See price in cart" or a link/button to click to "See Price".  Once the product
 * is in the cart the true price will be shown.  This allows shops to enforce
 * resale price maintenance policies.
 *
 *
 * ## Terminology
 *
 * + `MAP` - Minimum Advertised Price, the lowest price a retailer or distributor
 *   can advertise for a product.
 *
 * ## Admin Considerations
 *
 * A MAP meta field is added to the product data tab for simple products and variable products
 * Settings are added under WooCommerce > Settings > Catalog
 *
 * ## Frontend Considerations
 *
 * On the catalog page the MAP will be shown in place of the product price, with
 * a strikethrough and text indicating that the customer can "See price in cart"
 * or take an action to "See Price", as determined by the Catalog configuration.
 * If the price can be seen 'on gesture' a button will be displayed for
 * non-variable products to show the price in a modeless dialog box, which is
 * rendered by the included template file.
 *
 * On the product page the pricing will be displayed similarly to the catalog
 * page, though the "on gesture" action will be available for variable products
 * once a variation is configured.
 *
 * In the cart the MAP price will be shown along with the actual price, and the
 * savings, as determined by teh Catalog configuration.
 *
 * ## Database
 *
 * ### Global Settings
 *
 * + `wc_minimum_advertised_price_display_location` - where to display the actual price, 'on_gesture' or 'in_cart'
 * + `wc_minimum_advertised_price_label` - the label shown next to the minimum advertised price, ie "See Price in Cart"
 * + `wc_minimum_advertised_price_show_savings` - whether to display a "save $X.XX" label along with the actual price
 *
 * ### Options table
 *
 * + `wc_minimum_advertised_price_version` - the current plugin version, set on install/upgrade
 *
 * ### Product Meta
 *
 * + `_minimum_advertised_price` - the minimum advertised price for the product
 * + `_min_minimum_advertised_price` - the smallest minimum advertised price of a set of product variations
 * + `_max_minimum_advertised_price` - the largest minimum advertised price of a set of product variations
 *
 * @since 1.0
 */
class WC_Minimum_Advertised_Price {


	/** the current plugin version */
	const VERSION = '1.0';

	/** The plugin text domain */
	const TEXT_DOMAIN = 'wc-minimum-advertised-price';

	/** @var WC_Minimum_Advertised_Price_Admin The admin handling class */
	private $admin;

	/** @var WC_Minimum_Advertised_Price_Frontend The frontend handling class */
	private $frontend;

	/** @var string the plugin path */
	private $plugin_path;

	/** @var string the plugin url */
	private $plugin_url;

	/** @var bool whether to display the savings off the MAP or not */
	private $show_savings;


	/**
	 * Initializes the plugin
	 *
	 * @since 1.0
	 */
	public function __construct() {

		// include required files
		$this->includes();

		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {

			// add a 'Configure' link to the plugin action links
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'add_plugin_setup_link' ) );

			// run every time
			$this->install();
		}

		// load translation
		add_action( 'init', array( $this, 'load_translation' ) );
		add_action( 'init', array( $this, 'include_template_functions' ), 25 );
	}


	/**
	 * Include required files
	 *
	 * @since 1.0
	 */
	private function includes() {

		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {

			$this->admin_includes();

		} else {

			// load frontend class
			include( 'classes/class-wc-minimum-advertised-price-frontend.php' );
			$this->frontend = new WC_Minimum_Advertised_Price_Frontend();
		}

	}


	/**
	 * Include required files for admin
	 *
	 * @since 1.0
	 */
	private function admin_includes() {

		// load frontend class
		include( 'classes/class-wc-minimum-advertised-price-admin.php' );
		$this->admin = new WC_Minimum_Advertised_Price_Admin();
	}


	/**
	 * Handle localization, WPML compatible
	 *
	 * @since 1.0
	 */
	public function load_translation() {

		// localization in the init action for WPML support
		load_plugin_textdomain( self::TEXT_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/' );
	}


	/**
	 * Function used to init WooCommerce Minimum Advertised Price template functions,
	 * making them pluggable by plugins and themes.
	 *
	 * @since 1.0
	 */
	public function include_template_functions() {
		include_once( 'woocommerce-minimum-advertised-price-template.php' );
	}


	/** Admin methods ******************************************************/


	/**
	 * Return the plugin action links.  This will only be called if the plugin
	 * is active.
	 *
	 * @since 1.0
	 * @param array $actions associative array of action names to anchor tags
	 * @return array associative array of plugin action links
	 */
	public function add_plugin_setup_link( $actions ) {
		// add the link to the front of the actions list
		return ( array_merge( array( 'configure' => sprintf( '<a href="%s">%s</a>', admin_url( 'admin.php?page=woocommerce_settings&tab=catalog' ), __( 'Configure', self::TEXT_DOMAIN ) ) ),
			$actions )
		);
	}


	/** Helper methods ******************************************************/


	/**
	 * Returns true if MAP-price savings should be shown, false otherwise
	 *
	 * @since 1.0
	 * @return boolean true if MAP-price savings should be shown, false otherwise
	 */
	public function show_savings() {

		if ( ! isset( $this->show_savings ) ) {
			$this->show_savings = ( 'yes' === get_option( 'wc_minimum_advertised_price_show_savings' ) );
		}

		return $this->show_savings;
	}


	/**
	 * Gets the absolute plugin path without a trailing slash, e.g.
	 * /path/to/wp-content/plugins/plugin-directory
	 *
	 * @since 1.0
	 * @return string plugin path
	 */
	public function get_plugin_path() {

		if ( $this->plugin_path )
			return $this->plugin_path;

		return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	}


	/**
	 * Gets the plugin url without a trailing slash
	 *
	 * @since 1.0
	 * @return string the plugin url
	 */
	public function get_plugin_url() {

		if ( $this->plugin_url )
			return $this->plugin_url;

		return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
	}


	/** Lifecycle methods ******************************************************/


	/**
	 * Run every time.  Used since the activation hook is not executed when updating a plugin
	 *
	 * @since 1.0
	 */
	private function install() {

		// get current version to check for upgrade
		$installed_version = get_option( 'wc_minimum_advertised_price_version' );

		// install
		if ( ! $installed_version ) {

			// install default settings
			foreach ( WC_Minimum_Advertised_Price_Admin::get_global_settings() as $setting ) {

				if ( isset( $setting['default'] ) )
					add_option( $setting['id'], $setting['default'] );
			}
		}

		// upgrade if installed version lower than plugin version
		if ( -1 === version_compare( $installed_version, self::VERSION ) )
			$this->upgrade( $installed_version );
	}


	/**
	 * Perform any version-related changes. Changes to custom db tables should be handled by the migrate() method
	 *
	 * @since 1.0
	 * @param int $installed_version the currently installed version of the plugin
	 */
	private function upgrade( $installed_version ) {

		// update the installed version option
		update_option( 'wc_minimum_advertised_price_version', self::VERSION );
	}


} // end \WC_Minimum_Advertised_Price class
