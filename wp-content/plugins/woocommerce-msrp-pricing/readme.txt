=== Plugin Name ===
Contributors: leewillis77
Tags: e-commerce, woocommerce, e-commerce
Requires at least: 3.2
Tested up to: 3.4
Stable tag: 1.0
License: GPLv3

== Description ==

A WooCommerce extension that lets you flag Manufacturer Suggested Retail Prices against products, and display them on the front end.

== Installation ==

1. Upload the plugin to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to WooCommerce > Settings > Catalog and choose your settings for:
    * Show MSRP Pricing?
    * MSRP Labelling


