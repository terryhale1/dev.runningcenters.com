<?php
/*
Plugin Name: WooCommerce MSRP Pricing
Plugin URI: http://woothemes.com/woocommerce/
Description: A WooCommerce extension that lets you flag Manufacturer Suggested Retail Prices against products, and display them on the front end.
Author: Lee Willis
Version: 1.4
Author URI: http://plugins.leewillis.co.uk/
License: GPLv3
*/

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'b9133a56078a1ffa217e74136769022b', '18727' );

if ( is_woocommerce_active() ) {

    if ( is_admin() )
        require_once ( 'woocommerce-msrp-admin.php' );
    else
        require_once ( 'woocommerce-msrp-frontend.php' );

}

register_activation_hook( __FILE__, 'woocommerce_msrp_activate' );

/**
 * Add default option settings on plugin activation
 */
function woocommerce_msrp_activate() {
    add_option ( 'woocommerce_msrp_status', 'always', true );
    add_option ( 'woocommerce_msrp_description', 'MSRP', true );
}