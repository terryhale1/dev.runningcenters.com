<?php
/*
Plugin Name: Woocommerce Import Reward Points
Plugin URI:
Description:Import the Reward points
Version: 0.0.1
Author: VRAZER
Author URI: http://www.vrazer.com
*/

if (!function_exists('get_post_id_by_meta_key_and_value')) {
    /**
     * Get post id from meta key and value
     * @param string $key
     * @param mixed $value
     * @return int|bool
     */
    function get_post_id_by_meta_key_and_value($key, $value) {
        global $wpdb;
        echo $wpdb->escape($value);
        $meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='".$wpdb->escape($key)."' AND meta_value='".$wpdb->escape($value)."'");
        print_r($meta);
        if (is_array($meta) && !empty($meta) && isset($meta[0])) {
            $meta = $meta[0];
        }
        if (is_object($meta)) {
            return $meta->post_id;
        }
        else {
            return 0;
        }
    }
}


/*
 * Show the form to update the option
 */

function show_woocommerce_import_rewardpoints_form(){
   echo "<h2> Import Reward Points - Enter Csv file ID From Media Library </h2>";
   echo "<form name='update_option' method='post'>";
   echo "<input type='text' name='file_id' id='file_id' value='".$_POST['file_id']."'>";

   echo "<input type='submit' name='submit' id='submit'>";
   echo "</form>";

    if($_POST['submit']){

        if(!empty($_POST['file_id'])){
            if(add_reward_points(intval($_POST['file_id']))){
                ?>
                <div class="success">
                    <p><?php _e( 'Reward points successfully imported!'); ?></p>
                </div>
                <?php
            }

        } else {
            ?>
                <div class="error">
                    <p><?php _e( 'Import csv file id is not set, Please enter ID of the file!'); ?></p>
                </div>
            <?php
            }

        }
}


/*
 * Function for updating swatch type options for each product.
 *
 */

function add_reward_points($file_id){

    $import_file_url = wp_get_attachment_url($file_id);

    $csv = rewardpoints_readCSV($_SERVER['DOCUMENT_ROOT'].$import_file_url);

    $csv_final = array();
    global $wpdb;

    for($row=0; $row<count($csv);$row++){
        if($row==0){

        } else {
            if($csv[$row]['0']!='' && $csv[$row]['1']!='') {
                $email= $csv[$row]['0'];
                $reward_points =  $csv[$row]['1'];

                $userObj = get_user_by( 'email', $email );

                $user_id = $userObj->data->ID;
                //require_once('../../../wp-load.php');
                require_once(ABSPATH.'wp-admin/includes/upgrade.php');



                $points_table = $wpdb->prefix."wc_points_rewards_user_points";
                $points_log_table =$wpdb->prefix."wc_points_rewards_user_points_log";

                $user = $wpdb->get_var( "SELECT user_id FROM $points_table where user_id =$user_id" );

                if($user){
                    $wpdb->query( "
                                UPDATE $points_table
                                SET points = $reward_points, points_balance = $reward_points
                                WHERE user_id = $user_id
                                " );

                    $wpdb->query( "
                                UPDATE $points_log_table
                                SET points = $reward_points, type = 'admin-adjustment', admin_user_id = 1
                                WHERE user_id = $user_id
                                " );

                } else {

                    $date = date('Y-m-d hh:mm:ss');



                    if($wpdb->insert(
                        $points_table,
                        array(
                            'points' => $reward_points,
                            'points_balance' => $reward_points,
                            'user_id' => $user_id,
                            'date' => current_time('mysql')
                        ),
                        array(
                            '%d',
                            '%d',
                            '%d',
                            '%s'
                        )
                    )) {

                    } else {
                        echo mysql_error();
                    }
                    if($wpdb->insert(
                        $points_log_table,
                        array(
                            'points' => $reward_points,
                            'type' => 'admin-adjustment',
                            'admin_user_id' => 1,
                            'user_id' => $user_id,
                            'date' => current_time('mysql')
                        ),
                        array(
                            '%d',
                            '%s',
                            '%d',
                            '%d',
                            '%s'
                        )
                    )) {

                    }else {
                        echo mysql_error();
                    }
                }

                update_user_meta($user_id,'wc_points_balance',$reward_points);

            }
        }
    }

    return true;

}

/*
 * Read CSV file and return the array
 */
function rewardpoints_readCSV($csvFile){
    $file_handle = fopen($csvFile, 'r');
    while (!feof($file_handle) ) {
        $line_of_text[] = fgetcsv($file_handle, 1024);
    }
    fclose($file_handle);
    return $line_of_text;
}

add_action('admin_menu','woocommerce_import_rewardpoints');

function woocommerce_import_rewardpoints(){

    add_options_page('Import Reward Points', 'Import Reward Points', 'manage_options', 'import-rewardpoints.php', 'show_woocommerce_import_rewardpoints_form');
}