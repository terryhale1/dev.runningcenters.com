Plugin Name: WooCommerce Professor Cloud

Plugin URI: http://www.chromeorange.co.uk

Description: 
Modifies the standard large image display on 'single-product.php' to use the Professor Cloud hover/magnify :). 
Based on http://www.professorcloud.com/mainsite/cloud-zoom.htm. 
In the event of a problem please email andrew@chromeorange.co.uk with a screen shot of your cloud zoom settings 
and a link to your site. 
Tint and soft focus options are not available in version 1. 
Now includes Scaling via TimThumb

Version: 1.7
Author: Andrew Benbow
Author URI: http://www.chromeorange.co.uk

Copy the woocommerce-professor-cloud folder to your wp-content/plugins/ folder

UPDATES : 

Ver: 1.1 Minor error in size test fixed.
Ver: 1.2 TimThumb added to allow scaling of zoom image.
Ver: 1.3 Now works with thumbnail gallery, attribute images and product slider addon
Ver: 1.4 Never Released
Ver: 1.5 See change log for updates.
Ver: 1.6 See change log for updates.
Ver: 1.7 See change log for updates.
Ver: 2.0 See change log for updates.