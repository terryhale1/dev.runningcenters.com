<?php
/*
Plugin Name: Woocommerce Import Products and Variations
Plugin URI:
Description:Import Products and Variations
Version: 0.0.1
Author: VRAZER
Author URI: http://www.vrazer.com
*/

if (!function_exists('get_post_id_by_meta_key_and_like_value')) {
    /**
     * Get post id from meta key and value
     * @param string $key
     * @param mixed $value
     * @return int|bool
     */
    function get_post_id_by_meta_key_and_like_value($key, $value)
    {
        global $wpdb;
        $meta = $wpdb->get_results("SELECT * FROM `" . $wpdb->postmeta . "` WHERE meta_key='" . $wpdb->escape($key) . "' AND meta_value like '%" . $wpdb->escape($value) . "%'");
        return $meta;
    }
}

if (!function_exists('get_post_id_by_meta_key_and_value')) {
    /**
     * Get post id from meta key and value
     * @param string $key
     * @param mixed $value
     * @return int|bool
     */
    function get_post_id_by_meta_key_and_value($key, $value) {
        global $wpdb;
        $meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='".$wpdb->escape($key)."' AND meta_value='".$wpdb->escape($value)."'");
        if (is_array($meta) && !empty($meta) && isset($meta[0])) {
            $meta = $meta[0];
        }
        if (is_object($meta)) {
            return $meta->post_id;
        }
        else {
            return 0;
        }
    }
}


/*
 * Show the form to update the option
 */

function woocommerce_import_products_and_variations_form(){
    echo "<h2> Enter Import Csv file ID From Media Library </h2>";
    echo "<form name='update_products_option' method='post'>";
    echo "<input type='text' name='file_id' id='file_id'>";

    echo "<input type='submit' name='submit' id='submit' value='Import products with variations'>";
    echo "</form>";

    echo "<h2> After importing updating products and variations please visit the products in admin</h2>";

    echo "<a href='".admin_url('edit.php?post_type=product')."'>Visit Products page</a>";

    echo "<h2> Enter the same Id as above </h2>";
    echo "<form name='update_variation_price_option' method='post'>";
    echo "<input type='text' name='file_id' id='file_id'>";

    echo "<input type='submit' name='price_var' id='submit' value='Update Variation Prices'>";
    echo "</form>";

    if($_POST['submit']){

        if(!empty($_POST['file_id'])){
            update_option('woocommerce_import_products_and_variations_file',$_POST['file_id']);
            woocommerce_add_product_with_variations(intval($_POST['file_id']));
            ?>

            <div class="success">
                <p><?php _e( 'Products Successfully imported'); ?></p>
            </div>
        <?php

        } else {
            ?>
            <div class="error">
                <p><?php _e( 'Import csv file id is not set, Please enter ID of the file!'); ?></p>
            </div>
        <?php
        }

    }

    if($_POST['price_var']){

        if(!empty($_POST['file_id'])){
            update_option('woocommerce_import_products_and_variations_file',$_POST['file_id']);
            woocommerce_update_variation_prices(intval($_POST['file_id']));
            ?>

            <div class="success">
                <p><?php _e( 'Variation prices successfully updated!'); ?></p>
            </div>
            <?php

        } else {
            ?>
            <div class="error">
                <p><?php _e( 'Import csv file id is not set, Please enter ID of the file!'); ?></p>
            </div>
        <?php
        }

    }
}

function woocommerce_add_product_with_variations($file_id){

    /*
     * Product attribute setting for the variable product
     */

    $import_file_url = wp_get_attachment_url($file_id);

    $csv = products_with_variations_readCSV($_SERVER['DOCUMENT_ROOT'].$import_file_url);

    $csv_final = array();
    $parent_product =0;

    $min_variation_price = array();
    $max_variation_price = array();

    $row=0;
    for($row=0; $row<count($csv);$row++){
        if($row==0){

            //skip if first row

        } else {

        if($csv[$row][1]=='') {

            //Parent product addition in Woocommerce
            $my_post = array(
                'post_title'    => $csv[$row][12],
                'post_content'  => $csv[$row][12],
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => 'product'
            );

            $parent_product = wp_insert_post( $my_post );

            wp_set_post_terms( $parent_product, 'variable', 'product_type' ); //Set the product type as variable

            $term_id = term_exists($csv[$row][9], 'product_cat');

            if(count($term_id)>0){

                wp_set_post_terms( $parent_product, $term_id['term_id'], 'product_cat' );

            } else {
                $term_id = wp_insert_term($csv[$row][9],'product_cat');
                wp_set_post_terms( $parent_product, $term_id['term_id'], 'product_cat' );
            }

            $term_id = term_exists($csv[$row][8], 'product_brand');
            if(count($term_id)>0){
                wp_set_post_terms( $parent_product, array($term_id['term_id']), 'product_brand' );

            } else {
                $term_id = wp_insert_term($csv[$row][8],'product_brand');
                wp_set_post_terms( $parent_product, array($term_id['term_id']), 'product_brand' );
            }

            add_post_meta($parent_product,'_visibility','visible');
            add_post_meta($parent_product,'_stock_status','instock');
            add_post_meta($parent_product,'_swatch_type','default');
            add_post_meta($parent_product,'_swatch_type_options','');
            add_post_meta($parent_product,'total_sales','0');
            add_post_meta($parent_product,'_downloadable','no');
            add_post_meta($parent_product,'_product_image_gallery','');
            add_post_meta($parent_product,'_regular_price','');
            add_post_meta($parent_product,'_sale_price','');
            add_post_meta($parent_product,'_tax_status','');
            add_post_meta($parent_product,'_purchase_note','');
            add_post_meta($parent_product,'_featured','no');
            add_post_meta($parent_product,'_weight','');
            add_post_meta($parent_product,'_length','');
            add_post_meta($parent_product,'_width','');
            add_post_meta($parent_product,'_height','');
            add_post_meta($parent_product,'_sku',$csv[$row][0]);


            $product_attributes['pa_color']['name'] ='pa_color';
            $product_attributes['pa_color']['value'] = '';
            $product_attributes['pa_color']['position'] = 1;
            $product_attributes['pa_color']['is_visible'] = 1;
            $product_attributes['pa_color']['is_variation'] = 1;
            $product_attributes['pa_color']['is_taxonomy'] = 1;

            $product_attributes['pa_shoe-size']['name'] ='pa_shoe-size';
            $product_attributes['pa_shoe-size']['value'] = '';
            $product_attributes['pa_shoe-size']['position'] = 2;
            $product_attributes['pa_shoe-size']['is_visible'] = 1;
            $product_attributes['pa_shoe-size']['is_variation'] = 1;
            $product_attributes['pa_shoe-size']['is_taxonomy'] = 1;

            $product_attributes['pa_shoe-width']['name'] ='pa_shoe-width';
            $product_attributes['pa_shoe-width']['value'] = '';
            $product_attributes['pa_shoe-width']['position'] = 3;
            $product_attributes['pa_shoe-width']['is_visible'] = 1;
            $product_attributes['pa_shoe-width']['is_variation'] = 1;
            $product_attributes['pa_shoe-width']['is_taxonomy'] = 1;

            add_post_meta($parent_product,'_product_attributes',$product_attributes);
            add_post_meta($parent_product,'_sale_price_dates_from','');
            add_post_meta($parent_product,'_sale_price_dates_to','');
            add_post_meta($parent_product,'_price','');
            add_post_meta($parent_product,'_sold_individually','');
            add_post_meta($parent_product,'_stock','');
            add_post_meta($parent_product,'_backorders','no');
            add_post_meta($parent_product,'_manage_stock','no');
            add_post_meta($parent_product,'_coupon_title','');

            add_post_meta($parent_product,'_min_variation_regular_price','');
            add_post_meta($parent_product,'_max_variation_regular_price','');
            add_post_meta($parent_product,'_min_variation_sale_price','');
            add_post_meta($parent_product,'_max_variation_sale_price','');
            add_post_meta($parent_product,'_default_attributes',array());
            add_post_meta($parent_product,'_msrp_price','');
            add_post_meta($parent_product,'variation_image_gallery','');
            add_post_meta($parent_product,'_tax_class','');
            add_post_meta($parent_product,'_virtual','no');
            add_post_meta($parent_product,'_edit_last','1');

            $max_variation_price[$parent_product] = $csv[$row][5];
            $min_variation_price[$parent_product] = $csv[$row][6];

        } else {
            //child product addition

            $my_post = array(
                'post_title'    => $csv[$row][12],
                'post_content'  => 'product-'.$parent_product.'-variation-'.$row,
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     => 'product_variation',
                'post_parent'   => $parent_product

            );

            $child_product = wp_insert_post( $my_post );

            add_post_meta($child_product,'_msrp','');
            add_post_meta($child_product,'_sku',$csv[$row][0]);
            add_post_meta($child_product,'_weight','');
            add_post_meta($child_product,'_length','');
            add_post_meta($child_product,'_width','');
            add_post_meta($child_product,'_height','');
            add_post_meta($child_product,'_stock','');
            add_post_meta($child_product,'_thumbnail_id','');
            add_post_meta($child_product,'_virtual','');
            add_post_meta($child_product,'_downloadable','');
            add_post_meta($child_product,'_regular_price',$csv[$row][5]);
            add_post_meta($child_product,'_sale_price',$csv[$row][6]);
            add_post_meta($child_product,'_sale_price_dates_from','');
            add_post_meta($child_product,'_sale_price_dates_to','');
            add_post_meta($child_product,'_price',$csv[$row][6]);
            add_post_meta($child_product,'_tax_class','');
            add_post_meta($child_product,'_download_limit','');
            add_post_meta($child_product,'_download_expiry','');
            add_post_meta($child_product,'_file_paths','');

            $term_id = term_exists($csv[$row][10], 'pa_color');

            if(count($term_id)>0){
                $term = get_term_by( 'id', $term_id['term_id'], 'pa_color');
                add_post_meta($child_product,'attribute_pa_color',$term->slug);

            } else {
                $term_id = wp_insert_term($csv[$row][10],'pa_color');
                $term = get_term_by( 'id', $term_id['term_id'], 'pa_color');
                add_post_meta($child_product,'attribute_pa_color',$term->slug);
            }

            //Parent attribute term and texonomies
            $color_terms = wp_get_object_terms( $parent_product, 'pa_color' );
            if(!empty($color_terms)){
                foreach($color_terms as $ct){
                    if($ct->name == $csv[$row][10]){



                    } else {
                        $term_id = term_exists($csv[$row][10], 'pa_color');
                        if(count($term_id)>0){
                            wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_color' ,true);

                        } else {
                            $term_id = wp_insert_term($csv[$row][10],'pa_color');
                            wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_color',true );
                        }

                    }
                }
            } else {
                $term_id = term_exists($csv[$row][10], 'pa_color');
                if(count($term_id)>0){
                    wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_color',true );

                } else {
                    $term_id = wp_insert_term($csv[$row][10],'pa_color');
                    wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_color',true );
                }
            }


            $term_id = term_exists($csv[$row][3], 'pa_shoe-size');

            if(count($term_id)>0){
                $term = get_term_by( 'id', $term_id['term_id'], 'pa_shoe-size');
                add_post_meta($child_product,'attribute_pa_shoe-size',$term->slug);

            } else {
                $term_id = wp_insert_term($csv[$row][3],'pa_shoe-size');
                $term = get_term_by( 'id', $term_id['term_id'], 'pa_shoe-size');
                add_post_meta($child_product,'attribute_pa_shoe-size',$term->slug);
            }

            //Parent attribute term and texonomies
            $shoe_size_terms = wp_get_object_terms( $parent_product, 'pa_shoe-size' );
            if(!empty($shoe_size_terms)){
                foreach($shoe_size_terms as $szt){
                    if($szt->name == $csv[$row][3]){


                    } else {
                        $term_id = term_exists($csv[$row][3], 'pa_shoe-size');
                        if(count($term_id)>0){
                            wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_shoe-size' ,true);

                        } else {
                            $term_id = wp_insert_term($csv[$row][3],'pa_shoe-size');
                            wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_shoe-size' ,true);
                        }

                    }
                }
            } else {
                $term_id = term_exists($csv[$row][3], 'pa_shoe-size');
                if(count($term_id)>0){
                    wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_shoe-size',true );

                } else {
                    $term_id = wp_insert_term($csv[$row][3],'pa_shoe-size');
                    wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_shoe-size' ,true);
                }

            }

            ///Shoe Width variation

            $term_id = term_exists($csv[$row][2], 'pa_shoe-width');

            if(count($term_id)>0){
                $term = get_term_by( 'id', $term_id['term_id'], 'pa_shoe-width');
                add_post_meta($child_product,'attribute_pa_shoe-width',$term->slug);

            } else {
                $term_id = wp_insert_term($csv[$row][2],'pa_shoe-width');
                $term = get_term_by( 'id', $term_id['term_id'], 'pa_shoe-width');
                add_post_meta($child_product,'attribute_pa_shoe-width',$term->slug);
            }

            //Parent attribute term and texonomies
            $shoe_size_terms = wp_get_object_terms( $parent_product, 'pa_shoe-width' );
            if(!empty($shoe_size_terms)){
                foreach($shoe_size_terms as $szt){
                    if($szt->name == $csv[$row][2]){


                    } else {
                        $term_id = term_exists($csv[$row][2], 'pa_shoe-width');
                        if(count($term_id)>0){
                            wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_shoe-width' ,true);

                        } else {
                            $term_id = wp_insert_term($csv[$row][2],'pa_shoe-width');
                            wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_shoe-width' ,true);
                        }

                    }
                }
            } else {
                $term_id = term_exists($csv[$row][2], 'pa_shoe-width');
                if(count($term_id)>0){
                    wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_shoe-width',true );

                } else {
                    $term_id = wp_insert_term($csv[$row][2],'pa_shoe-width');
                    wp_set_post_terms( $parent_product, array($term_id['term_id']), 'pa_shoe-width' ,true);
                }

            }

        }

        }
    }

    /*
     * Update the minimum variation price meta for each parent product
     */

    foreach($min_variation_price as $product_id=>$price){
        update_post_meta($product_id,'_min_variation_price',$price);
    }

    /*
    * Update the maximum variation price meta for each parent product
    */

    foreach($max_variation_price as $product_id=>$price){
        update_post_meta($product_id,'_max_variation_price',$price);
    }

}


function woocommerce_update_variation_prices($file_id){

    /*
     * Product attribute setting for the variable product
     */

    $import_file_url = wp_get_attachment_url($file_id);

    $csv = products_with_variations_readCSV($_SERVER['DOCUMENT_ROOT'].$import_file_url);

    $csv_final = array();
    $parent_product =0;

    $min_variation_price = array();
    $max_variation_price = array();

    $row=0;
    for($row=0; $row<count($csv);$row++){
        if($row==0){

            //skip if first row

        } else {

            if($csv[$row][1]=='') {

                $parent_product = get_post_id_by_meta_key_and_value('_sku',$csv[$row][0]);

                $max_variation_price[$parent_product] = $csv[$row][5];
                $min_variation_price[$parent_product] = $csv[$row][6];

            } else {

            }

        }


    }



    /*
     * Update the minimum variation price meta for each parent product
     */

    foreach($min_variation_price as $product_id=>$price){
        $res = update_post_meta($product_id,'_min_variation_price',$price);

    }

    /*
    * Update the maximum variation price meta for each parent product
    */

    foreach($max_variation_price as $product_id=>$price){

        $res = update_post_meta($product_id,'_max_variation_price',$price);

    }

    echo "<pre>";
    echo "Maximum Variation price data: <br>";
    print_r($max_variation_price);
    echo "Minimum Variation price data: <br>";
    print_r($min_variation_price);


}
/*
 * Read CSV file and return the array
 */
function products_with_variations_readCSV($csvFile){
    $file_handle = fopen($csvFile, 'r');
    while (!feof($file_handle) ) {
        $line_of_text[] = fgetcsv($file_handle, 1024);
    }
    fclose($file_handle);
    return $line_of_text;
}

/*add_action('admin_menu', 'woocommerce_import_variation_images_menu');

function woocommerce_import_products_and_variations_menu()
{

    add_options_page('Import Woo Products with Variations', 'Import Woo Products with Variations', 'manage_options', 'import-products-and-variations.php', 'woocommerce_import_products_and_variations_form');
}*/

add_action( 'admin_menu', 'woocommerce_import_products_and_variations_menu' );

function woocommerce_import_products_and_variations_menu(){
    add_menu_page( 'Import Woo Products with Variations', 'Import Woo Products with Variations', 'manage_options', 'import-products-and-variations.php', 'woocommerce_import_products_and_variations_form', '', 120 );
}

function ipav_terms(){
//    $term = '5';
//    $taxonomy ='pa_shoe-size';
//    $term_id = term_exists( $term, $taxonomy);
//    //wp_set_object_terms( '42', $cat_ids, 'category' );
//    // Post Id , term ids = 35, , taxonomy name = pa_shoe-size
//
//    print_r(wp_get_post_terms(2856,'product_brand'));
//
//
//
//
//    echo "<pre>";
//    print_r(get_post_meta(2856,'_product_attributes'));
//    echo "The term id is:";
//    print_r($term_id);
}

