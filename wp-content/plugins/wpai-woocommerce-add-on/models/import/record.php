<?php

class PMWI_Import_Record extends PMWI_Model_Record {		

	/**
	 * Associative array of data which will be automatically available as variables when template is rendered
	 * @var array
	 */
	public $data = array();

	/**
	 * Initialize model instance
	 * @param array[optional] $data Array of record data to initialize object with
	 */
	public function __construct($data = array()) {
		parent::__construct($data);
		$this->setTable(PMXI_Plugin::getInstance()->getTablePrefix() . 'imports');
	}	
	
	/**
	 * Perform import operation
	 * @param string $xml XML string to import
	 * @param callback[optional] $logger Method where progress messages are submmitted
	 * @return PMWI_Import_Record
	 * @chainable
	 */
	public function process($import, $count, $xml, $logger = NULL, $chunk = false) {
		add_filter('user_has_cap', array($this, '_filter_has_cap_unfiltered_html')); kses_init(); // do not perform special filtering for imported content
		
		$this->data = array();

		$records = array();

		($chunk == 1 or (empty($import->large_import) or $import->large_import == 'No')) and $logger and call_user_func($logger, __('Composing product data...', 'pmxi_plugin'));

		// Composing product types
		if ($import->options['is_multiple_product_type'] != 'yes' and "" != $import->options['single_product_type']){
			$this->data['product_types'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_type'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_types'] = array_fill(0, $count, $import->options['multiple_product_type']);
		}

		// Composing product is Virtual									
		if ($import->options['is_product_virtual'] == 'xpath' and "" != $import->options['single_product_virtual']){
			$this->data['product_virtual'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_virtual'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_virtual'] = array_fill(0, $count, $import->options['is_product_virtual']);
		}

		// Composing product is Downloadable									
		if ($import->options['is_product_downloadable'] == 'xpath' and "" != $import->options['single_product_downloadable']){
			$this->data['product_downloadable'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_downloadable'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_downloadable'] = array_fill(0, $count, $import->options['is_product_downloadable']);
		}

		// Composing product is Variable Enabled									
		if ($import->options['is_product_enabled'] == 'xpath' and "" != $import->options['single_product_enabled']){
			$this->data['product_enabled'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_enabled'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_enabled'] = array_fill(0, $count, $import->options['is_product_enabled']);
		}

		// Composing product is Featured									
		if ($import->options['is_product_featured'] == 'xpath' and "" != $import->options['single_product_featured']){
			$this->data['product_featured'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_featured'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_featured'] = array_fill(0, $count, $import->options['is_product_featured']);
		}

		// Composing product is Visibility									
		if ($import->options['is_product_visibility'] == 'xpath' and "" != $import->options['single_product_visibility']){
			$this->data['product_visibility'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_visibility'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_visibility'] = array_fill(0, $count, $import->options['is_product_visibility']);
		}

		if ("" != $import->options['single_product_sku']){
			$this->data['product_sku'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_sku'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_sku'] = array_fill(0, $count, "");
		}		

		if ("" != $import->options['single_product_url']){
			$this->data['product_url'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_url'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_url'] = array_fill(0, $count, "");
		}

		if ("" != $import->options['single_product_button_text']){
			$this->data['product_button_text'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_button_text'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_button_text'] = array_fill(0, $count, "");
		}

		if ("" != $import->options['single_product_regular_price']){
			$this->data['product_regular_price'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_regular_price'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_regular_price'] = array_fill(0, $count, "");
		}

		if ($import->options['is_regular_price_shedule'] and "" != $import->options['single_sale_price_dates_from']){
			$this->data['product_sale_price_dates_from'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_sale_price_dates_from'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_sale_price_dates_from'] = array_fill(0, $count, "");
		}

		if ($import->options['is_regular_price_shedule'] and "" != $import->options['single_sale_price_dates_to']){
			$this->data['product_sale_price_dates_to'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_sale_price_dates_to'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_sale_price_dates_to'] = array_fill(0, $count, "");
		}

		if ("" != $import->options['single_product_sale_price']){
			$this->data['product_sale_price'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_sale_price'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_sale_price'] = array_fill(0, $count, "");
		}

		if ("" != $import->options['single_product_files']){
			$this->data['product_files'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_files'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_files'] = array_fill(0, $count, "");
		}

		if ("" != $import->options['single_product_download_limit']){
			$this->data['product_download_limit'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_download_limit'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_download_limit'] = array_fill(0, $count, "");
		}

		if ("" != $import->options['single_product_download_expiry']){
			$this->data['product_download_expiry'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_download_expiry'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_download_expiry'] = array_fill(0, $count, "");
		}
		
		// Composing product Tax Status									
		if ($import->options['is_multiple_product_tax_status'] != 'yes' and "" != $import->options['single_product_tax_status']){
			$this->data['product_tax_status'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_tax_status'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_tax_status'] = array_fill(0, $count, $import->options['multiple_product_tax_status']);
		}

		// Composing product Tax Class									
		if ($import->options['is_multiple_product_tax_class'] != 'yes' and "" != $import->options['single_product_tax_class']){
			$this->data['product_tax_class'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_tax_class'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_tax_class'] = array_fill(0, $count, $import->options['multiple_product_tax_class']);
		}

		// Composing product Manage stock?								
		if ($import->options['is_product_manage_stock'] == 'xpath' and "" != $import->options['single_product_manage_stock']){
			$this->data['product_manage_stock'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_manage_stock'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_manage_stock'] = array_fill(0, $count, $import->options['is_product_manage_stock']);
		}

		if ("" != $import->options['single_product_stock_qty']){
			$this->data['product_stock_qty'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_stock_qty'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_stock_qty'] = array_fill(0, $count, "");
		}					

		// Composing product Stock status							
		if ($import->options['product_stock_status'] == 'xpath' and "" != $import->options['single_product_stock_status']){
			$this->data['product_stock_status'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_stock_status'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_stock_status'] = array_fill(0, $count, $import->options['product_stock_status']);
		}

		// Composing product Allow Backorders?						
		if ($import->options['product_allow_backorders'] == 'xpath' and "" != $import->options['single_product_allow_backorders']){
			$this->data['product_allow_backorders'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_allow_backorders'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_allow_backorders'] = array_fill(0, $count, $import->options['product_allow_backorders']);
		}

		// Composing product Sold Individually?					
		if ($import->options['product_sold_individually'] == 'xpath' and "" != $import->options['single_product_sold_individually']){
			$this->data['product_sold_individually'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_sold_individually'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_sold_individually'] = array_fill(0, $count, $import->options['product_sold_individually']);
		}

		if ("" != $import->options['single_product_weight']){
			$this->data['product_weight'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_weight'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_weight'] = array_fill(0, $count, "");
		}
		if ("" != $import->options['single_product_length']){
			$this->data['product_length'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_length'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_length'] = array_fill(0, $count, "");
		}
		if ("" != $import->options['single_product_width']){
			$this->data['product_width'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_width'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_width'] = array_fill(0, $count, "");
		}
		if ("" != $import->options['single_product_height']){
			$this->data['product_height'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_height'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_height'] = array_fill(0, $count, "");
		}

		// Composing product Shipping Class				
		if ($import->options['is_multiple_product_shipping_class'] != 'yes' and "" != $import->options['single_product_shipping_class']){
			$this->data['product_shipping_class'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_shipping_class'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_shipping_class'] = array_fill(0, $count, $import->options['multiple_product_shipping_class']);
		}

		if ("" != $import->options['single_product_up_sells']){
			$this->data['product_up_sells'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_up_sells'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_up_sells'] = array_fill(0, $count, "");
		}
		if ("" != $import->options['single_product_cross_sells']){
			$this->data['product_cross_sells'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_cross_sells'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_cross_sells'] = array_fill(0, $count, "");
		}

		if ("" != $import->options['grouping_product']){
			$this->data['product_grouping_parent'] = XmlImportParser::factory($xml, $import->xpath, $import->options['grouping_product'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_grouping_parent'] = array_fill(0, $count, "");
		}

		if ("" != $import->options['single_product_purchase_note']){
			$this->data['product_purchase_note'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_purchase_note'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_purchase_note'] = array_fill(0, $count, "");
		}
		if ("" != $import->options['single_product_menu_order']){
			$this->data['product_menu_order'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_menu_order'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['product_menu_order'] = array_fill(0, $count, "");
		}
		
		// Composing product Enable reviews		
		if ($import->options['is_product_enable_reviews'] == 'xpath' and "" != $import->options['single_product_enable_reviews']){
			$this->data['product_enable_reviews'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_enable_reviews'], $file)->parse($records); $tmp_files[] = $file;						
		}
		else{
			$count and $this->data['product_enable_reviews'] = array_fill(0, $count, $import->options['is_product_enable_reviews']);
		}

		if ("" != $import->options['single_product_id']){
			$this->data['single_product_ID'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_id'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['single_product_ID'] = array_fill(0, $count, "");
		}
		if ("" != $import->options['single_product_parent_id']){
			$this->data['single_product_parent_ID'] = XmlImportParser::factory($xml, $import->xpath, $import->options['single_product_parent_id'], $file)->parse($records); $tmp_files[] = $file;
		}
		else{
			$count and $this->data['single_product_parent_ID'] = array_fill(0, $count, "");
		}

		if ($import->options['matching_parent'] == 'manual' and $import->options['parent_indicator'] == "custom field"){
			if ("" != $import->options['custom_parent_indicator_name']){
				$this->data['custom_parent_indicator_name'] = XmlImportParser::factory($xml, $import->xpath, $import->options['custom_parent_indicator_name'], $file)->parse($records); $tmp_files[] = $file;
			}
			else{
				$count and $this->data['custom_parent_indicator_name'] = array_fill(0, $count, "");
			}
			if ("" != $import->options['custom_parent_indicator_value']){
				$this->data['custom_parent_indicator_value'] = XmlImportParser::factory($xml, $import->xpath, $import->options['custom_parent_indicator_value'], $file)->parse($records); $tmp_files[] = $file;
			}
			else{
				$count and $this->data['custom_parent_indicator_value'] = array_fill(0, $count, "");
			}			
		}
		
		// Composing variations attributes					
		($chunk == 1 or (empty($import->large_import) or $import->large_import == 'No')) and $logger and call_user_func($logger, __('Composing variations attributes...', 'pmxi_plugin'));
		$attribute_keys = array(); 
		$attribute_values = array();	
		$attribute_in_variation = array(); 
		$attribute_is_visible = array();			
		$attribute_is_taxonomy = array();	
		$attribute_create_taxonomy_terms = array();		
							
		if (!empty($import->options['attribute_name'][0])){			
			foreach ($import->options['attribute_name'] as $j => $attribute_name) { if ($attribute_name == "") continue;								    											
				$attribute_keys[$j]   = XmlImportParser::factory($xml, $import->xpath, $attribute_name, $file)->parse($records); $tmp_files[] = $file;
				$attribute_values[$j] = XmlImportParser::factory($xml, $import->xpath, $import->options['attribute_value'][$j], $file)->parse($records); $tmp_files[] = $file;
				$attribute_in_variation[$j] = XmlImportParser::factory($xml, $import->xpath, $import->options['in_variations'][$j], $file)->parse($records); $tmp_files[] = $file;
				$attribute_is_visible[$j] = XmlImportParser::factory($xml, $import->xpath, $import->options['is_visible'][$j], $file)->parse($records); $tmp_files[] = $file;
				$attribute_is_taxonomy[$j] = XmlImportParser::factory($xml, $import->xpath, $import->options['is_taxonomy'][$j], $file)->parse($records); $tmp_files[] = $file;
				$attribute_create_taxonomy_terms[$j] = XmlImportParser::factory($xml, $import->xpath, $import->options['create_taxonomy_in_not_exists'][$j], $file)->parse($records); $tmp_files[] = $file;
			}			
		}					

		// serialized attributes for product variations
		$this->data['serialized_attributes'] = array();
		if (!empty($attribute_keys)){
			foreach ($attribute_keys as $j => $attribute_name) {											
				if (!in_array($attribute_name[0], array_keys($this->data['serialized_attributes']))){
					$this->data['serialized_attributes'][$attribute_name[0]] = array(
						'value' => $attribute_values[$j],
						'is_visible' => $attribute_is_visible[$j],
						'in_variation' => $attribute_in_variation[$j],
						'in_taxonomy' => $attribute_is_taxonomy[$j],
						'is_create_taxonomy_terms' => $attribute_create_taxonomy_terms[$j]
					);						
				}							
			}
		} 					

		remove_filter('user_has_cap', array($this, '_filter_has_cap_unfiltered_html')); kses_init(); // return any filtering rules back if they has been disabled for import procedure
		
		foreach ($tmp_files as $file) { // remove all temporary files created
			unlink($file);
		}

		return $this->data;
	}	

	public function import($pid, $i, $import, $articleData, $xml, $is_cron = false){

		$logger = create_function('$m', 'echo "<div class=\\"progress-msg\\">$m</div>\\n"; if ( "" != strip_tags(pmxi_strip_tags_content($m))) { PMXI_Plugin::$session[\'pmxi_import\'][\'log\'] .= "<p>".strip_tags(pmxi_strip_tags_content($m))."</p>"; flush(); }');		

		global $woocommerce;

		extract($this->data);

		// Add any default post meta
		add_post_meta( $pid, 'total_sales', '0', true );

		// Get types
		$product_type 		= empty( $product_types[$i] ) ? 'simple' : sanitize_title( stripslashes( $product_types[$i] ) );
		$is_downloadable 	= $product_downloadable[$i];
		$is_virtual 		= $product_virtual[$i];
		$is_featured 		= $product_featured[$i];

		$existing_meta_keys = array();
		foreach (get_post_meta($pid, '') as $cur_meta_key => $cur_meta_val) $existing_meta_keys[] = $cur_meta_key;
				
		// Product type + Downloadable/Virtual
		wp_set_object_terms( $pid, $product_type, 'product_type' );
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_downloadable')) update_post_meta( $pid, '_downloadable', ($is_downloadable == "yes") ? 'yes' : 'no' );
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_virtual')) update_post_meta( $pid, '_virtual', ($is_virtual == "yes") ? 'yes' : 'no' );						

		// Update post meta
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_regular_price')) update_post_meta( $pid, '_regular_price', stripslashes( $product_regular_price[$i] ) );
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price')) update_post_meta( $pid, '_sale_price', stripslashes( $product_sale_price[$i] ) );
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_tax_status')) update_post_meta( $pid, '_tax_status', stripslashes( $product_tax_status[$i] ) );
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_tax_class')) update_post_meta( $pid, '_tax_class', stripslashes( $product_tax_class[$i] ) );			
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_visibility')) update_post_meta( $pid, '_visibility', stripslashes( $product_visibility[$i] ) );			
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_purchase_note')) update_post_meta( $pid, '_purchase_note', stripslashes( $product_purchase_note[$i] ) );
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_featured')) update_post_meta( $pid, '_featured', ($is_featured == "yes") ? 'yes' : 'no' );

		// Dimensions
		if ( $is_virtual == 'no' ) {
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_weight')) update_post_meta( $pid, '_weight', stripslashes( $product_weight[$i] ) );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_length')) update_post_meta( $pid, '_length', stripslashes( $product_length[$i] ) );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_width')) update_post_meta( $pid, '_width', stripslashes( $product_width[$i] ) );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_height')) update_post_meta( $pid, '_height', stripslashes( $product_height[$i] ) );
		} else {
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_weight')) update_post_meta( $pid, '_weight', '' );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_length')) update_post_meta( $pid, '_length', '' );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_width')) update_post_meta( $pid, '_width', '' );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_height')) update_post_meta( $pid, '_height', '' );
		}

		// Save shipping class
		$product_shipping_class = $product_shipping_class[$i] > 0 && $product_type != 'external' ? absint( $product_shipping_class[$i] ) : '';
		wp_set_object_terms( $pid, $product_shipping_class, 'product_shipping_class');

		// Unique SKU
		$sku				= get_post_meta($pid, '_sku', true);
		$new_sku 			= esc_html( trim( stripslashes( $product_sku[$i] ) ) );
		
		if ( $new_sku == '' ) {
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sku')) update_post_meta( $pid, '_sku', '' );
		} elseif ( $new_sku !== $sku ) {
			if ( ! empty( $new_sku ) ) {
				if (
					$this->wpdb->get_var( $this->wpdb->prepare("
						SELECT ".$this->wpdb->posts.".ID
					    FROM ".$this->wpdb->posts."
					    LEFT JOIN ".$this->wpdb->postmeta." ON (".$this->wpdb->posts.".ID = ".$this->wpdb->postmeta.".post_id)
					    WHERE ".$this->wpdb->posts.".post_type = 'product'
					    AND ".$this->wpdb->posts.".post_status = 'publish'
					    AND ".$this->wpdb->postmeta.".meta_key = '_sku' AND ".$this->wpdb->postmeta.".meta_value = '%s'
					 ", $new_sku ) )
					) {
					$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: Product SKU must be unique.', 'pmxi_plugin')));
					
				} else {
					if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sku')) update_post_meta( $pid, '_sku', $new_sku );
				}
			} else {
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sku')) update_post_meta( $pid, '_sku', '' );
			}
		}

		// Save Attributes
		$attributes = array();
		
		if ( !empty($serialized_attributes) ) {							
			
			$attribute_position = 0;

			foreach ($serialized_attributes as $attr_name => $attr_data) {				
				
				$is_visible 	= intval( $attr_data['is_visible'][$i] );
				$is_variation 	= intval( $attr_data['in_variation'][$i] );
				$is_taxonomy 	= intval( $attr_data['in_taxonomy'][$i] );


				if ( $is_taxonomy ) {										

					if ( isset( $attr_data['value'][$i] ) ) {
				 		
				 		$values = array_map( 'stripslashes', array_map( 'strip_tags', explode( '|', $attr_data['value'][$i] ) ) );

					 	// Remove empty items in the array
					 	$values = array_filter( $values );

					 	if ( ! taxonomy_exists( $woocommerce->attribute_taxonomy_name( $attr_name ) ) and intval($attr_data['is_create_taxonomy_terms'][$i])) {

					 		// Grab the submitted data							
							$attribute_name    = ( isset( $attr_name ) )   ? woocommerce_sanitize_taxonomy_name( stripslashes( (string) $attr_name ) ) : '';
							$attribute_label   = ucwords($attribute_name);
							$attribute_type    = 'select';
							$attribute_orderby = 'menu_order';

							$reserved_terms = array(
								'attachment', 'attachment_id', 'author', 'author_name', 'calendar', 'cat', 'category', 'category__and',
								'category__in', 'category__not_in', 'category_name', 'comments_per_page', 'comments_popup', 'cpage', 'day',
								'debug', 'error', 'exact', 'feed', 'hour', 'link_category', 'm', 'minute', 'monthnum', 'more', 'name',
								'nav_menu', 'nopaging', 'offset', 'order', 'orderby', 'p', 'page', 'page_id', 'paged', 'pagename', 'pb', 'perm',
								'post', 'post__in', 'post__not_in', 'post_format', 'post_mime_type', 'post_status', 'post_tag', 'post_type',
								'posts', 'posts_per_archive_page', 'posts_per_page', 'preview', 'robots', 's', 'search', 'second', 'sentence',
								'showposts', 'static', 'subpost', 'subpost_id', 'tag', 'tag__and', 'tag__in', 'tag__not_in', 'tag_id',
								'tag_slug__and', 'tag_slug__in', 'taxonomy', 'tb', 'term', 'type', 'w', 'withcomments', 'withoutcomments', 'year',
							);

							if ( in_array( $attribute_name, $reserved_terms ) ) {
								$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: Slug “%s” is not allowed because it is a reserved term. Change it, please.', 'pmxi_plugin'), sanitize_title( $attribute_name )));
							}			
							else{
								$this->wpdb->insert(
									$this->wpdb->prefix . 'woocommerce_attribute_taxonomies',
									array(
										'attribute_label'   => $attribute_label,
										'attribute_name'    => $attribute_name,
										'attribute_type'    => $attribute_type,
										'attribute_orderby' => $attribute_orderby,
									)
								);								

								$logger and call_user_func($logger, sprintf(__('<b>CREATED</b>: Taxonomy attribute “%s” have been successfully created.', 'pmxi_plugin'), sanitize_title( $attribute_name )));	
								
								// Register the taxonomy now so that the import works!
								$domain = $woocommerce->attribute_taxonomy_name( $attr_name );
								register_taxonomy( $domain,
							        apply_filters( 'woocommerce_taxonomy_objects_' . $domain, array('product') ),
							        apply_filters( 'woocommerce_taxonomy_args_' . $domain, array(
							            'hierarchical' => true,
							            'show_ui' => false,
							            'query_var' => true,
							            'rewrite' => false,
							        ) )
							    );

								delete_transient( 'wc_attribute_taxonomies' );

								$attribute_taxonomies = $this->wpdb->get_results( "SELECT * FROM " . $this->wpdb->prefix . "woocommerce_attribute_taxonomies" );

								set_transient( 'wc_attribute_taxonomies', $attribute_taxonomies );

								apply_filters( 'woocommerce_attribute_taxonomies', $attribute_taxonomies );
							}

					 	}

					 	if ( ! empty($values) and taxonomy_exists( $woocommerce->attribute_taxonomy_name( $attr_name ) )){

					 		$attr_values = array();
					 		
					 		$terms = get_terms( $woocommerce->attribute_taxonomy_name( $attr_name ), array('hide_empty' => false));								
					 		
					 		if ( ! is_wp_error($terms) ){
					 		
						 		foreach ($values as $key => $value) {
						 			$term_founded = false;	
									if ( count($terms) > 0 ){	
									    foreach ( $terms as $term ) {

									    	if ( strtolower($term->name) == trim(strtolower($value)) ) {
									    		$attr_values[] = $term->slug;
									    		$term_founded = true;
									    	}
									    }
									}
								    if ( ! $term_founded and intval($attr_data['is_create_taxonomy_terms'][$i]) ){
								    	$term = wp_insert_term(
											$value, // the term 
										  	$woocommerce->attribute_taxonomy_name( $attr_name ) // the taxonomy										  	
										);	
										if ( ! is_wp_error($term) ){
											$term = get_term_by( 'id', $term['term_id'], $woocommerce->attribute_taxonomy_name( $attr_name ));
											$attr_values[] = $term->slug; 
										}						    		
											
								    }
						 		}
						 	}
						 	else {
						 		$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: %s.', 'pmxi_plugin'), $terms->get_error_message()));
						 	}

					 		$values = $attr_values;

					 	} 
					 	else {
					 		$values = array();
					 	}

				 	} 				 				 	

			 		// Update post terms
			 		if ( taxonomy_exists( $woocommerce->attribute_taxonomy_name( $attr_name ) ))
			 			wp_set_object_terms( $pid, $values, $woocommerce->attribute_taxonomy_name( $attr_name ) );
			 		
			 		if ( $values ) {
									 			
				 		// Add attribute to array, but don't set values
				 		$attributes[ $woocommerce->attribute_taxonomy_name( $attr_name ) ] = array(
					 		'name' 			=> $woocommerce->attribute_taxonomy_name( $attr_name ),
					 		'value' 		=> '',
					 		'position' 		=> $attribute_position,
					 		'is_visible' 	=> $is_visible,
					 		'is_variation' 	=> $is_variation,
					 		'is_taxonomy' 	=> 1,
					 		'is_create_taxonomy_terms' => (!empty($attr_data['is_create_taxonomy_terms'][$i])) ? 1 : 0
					 	);

				 	}

			 	} else {

			 		if ( taxonomy_exists( $woocommerce->attribute_taxonomy_name( $attr_name ) ))
			 			wp_set_object_terms( $pid, NULL, $woocommerce->attribute_taxonomy_name( $attr_name ) );

			 		// Text based, separate by pipe
			 		$values = implode( ' | ', array_map( 'sanitize_text_field', explode( '|', $attr_data['value'][$i] ) ) );

			 		// Custom attribute - Add attribute to array and set the values
				 	$attributes[ sanitize_title( $attr_name ) ] = array(
				 		'name' 			=> sanitize_text_field( $attr_name ),
				 		'value' 		=> $values,
				 		'position' 		=> $attribute_position,
				 		'is_visible' 	=> $is_visible,
				 		'is_variation' 	=> $is_variation,
				 		'is_taxonomy' 	=> 0
				 	);

			 	}

			 	$attribute_position++;
			}							
		}						
		
		if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_product_attributes')) update_post_meta( $pid, '_product_attributes', $attributes );

		// Sales and prices
		if ( ! in_array( $product_type, array( 'grouped' ) ) ) {

			$date_from = isset( $product_sale_price_dates_from[$i] ) ? $product_sale_price_dates_from[$i] : '';
			$date_to = isset( $product_sale_price_dates_to[$i] ) ? $product_sale_price_dates_to[$i] : '';

			// Dates
			if ( $date_from ){
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_from')) update_post_meta( $pid, '_sale_price_dates_from', strtotime( $date_from ) );
			}
			else{
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_from')) update_post_meta( $pid, '_sale_price_dates_from', '' );
			}

			if ( $date_to ){
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_to')) update_post_meta( $pid, '_sale_price_dates_to', strtotime( $date_to ) );
			}
			else{
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_to')) update_post_meta( $pid, '_sale_price_dates_to', '' );
			}

			if ( $date_to && ! $date_from ){
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_from')) update_post_meta( $pid, '_sale_price_dates_from', strtotime( 'NOW', current_time( 'timestamp' ) ) );
			}

			// Update price if on sale
			if ( $product_sale_price[$i] != '' && $date_to == '' && $date_from == '' ){
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_price')) update_post_meta( $pid, '_price', stripslashes( $product_sale_price[$i] ) );
			}
			else{
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_price')) update_post_meta( $pid, '_price', stripslashes( $product_regular_price[$i] ) );
			}

			if ( $product_sale_price[$i] != '' && $date_from && strtotime( $date_from ) < strtotime( 'NOW', current_time( 'timestamp' ) ) ){
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_price')) update_post_meta( $pid, '_price', stripslashes($product_sale_price[$i]) );				
			}

			if ( $date_to && strtotime( $date_to ) < strtotime( 'NOW', current_time( 'timestamp' ) ) ) {
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_price')) update_post_meta( $pid, '_price', stripslashes($product_regular_price[$i]) );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_from')) update_post_meta( $pid, '_sale_price_dates_from', '');
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_to')) update_post_meta( $pid, '_sale_price_dates_to', '');
			}
		}

		// Update parent if grouped so price sorting works and stays in sync with the cheapest child
		if ( $product_type == 'grouped' || ( "" != $product_grouping_parent[$i] and absint($product_grouping_parent[$i]) > 0)) {

			$clear_parent_ids = array();													

			if ( $product_type == 'grouped' )
				$clear_parent_ids[] = $pid;		

			if ( "" != $product_grouping_parent[$i] and absint($product_grouping_parent[$i]) > 0 )
				$clear_parent_ids[] = absint( $product_grouping_parent[$i] );					

			if ( $clear_parent_ids ) {
				foreach( $clear_parent_ids as $clear_id ) {

					$children_by_price = get_posts( array(
						'post_parent' 	=> $clear_id,
						'orderby' 		=> 'meta_value_num',
						'order'			=> 'asc',
						'meta_key'		=> '_price',
						'posts_per_page'=> 1,
						'post_type' 	=> 'product',
						'fields' 		=> 'ids'
					) );
					if ( $children_by_price ) {
						foreach ( $children_by_price as $child ) {
							$child_price = get_post_meta( $child, '_price', true );
							update_post_meta( $clear_id, '_price', $child_price );
						}
					}

					// Clear cache/transients
					$woocommerce->clear_product_transients( $clear_id );
				}
			}
		}

		// Sold Individuall
		if ( "yes" == $product_sold_individually[$i] ) {
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sold_individually')) update_post_meta( $pid, '_sold_individually', 'yes' );
		} else {
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sold_individually')) update_post_meta( $pid, '_sold_individually', '' );
		}
		
		// Stock Data
		if ( $product_manage_stock[$i] == 'yes' ) {

			if ( $product_type == 'grouped' ) {

				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock_status')) update_post_meta( $pid, '_stock_status', stripslashes( $product_stock_status[$i] ) );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock')) update_post_meta( $pid, '_stock', '' );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_manage_stock')) update_post_meta( $pid, '_manage_stock', 'no' );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_backorders')) update_post_meta( $pid, '_backorders', 'no' );

			} elseif ( $product_type == 'external' ) {

				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock_status')) update_post_meta( $pid, '_stock_status', 'instock' );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock')) update_post_meta( $pid, '_stock', '' );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_manage_stock')) update_post_meta( $pid, '_manage_stock', 'no' );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_backorders')) update_post_meta( $pid, '_backorders', 'no' );

			} elseif ( ! empty( $product_manage_stock[$i] ) ) {

				// Manage stock
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock')) update_post_meta( $pid, '_stock', (int) $product_stock_qty[$i] );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock_status')) update_post_meta( $pid, '_stock_status', stripslashes( $product_stock_status[$i] ) );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_backorders')) update_post_meta( $pid, '_backorders', stripslashes( $product_allow_backorders[$i] ) );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_manage_stock')) update_post_meta( $pid, '_manage_stock', 'yes' );

				// Check stock level
				if ( $product_type !== 'variable' && $product_allow_backorders[$i] == 'no' && (int) $product_stock_qty[$i] < 1 ){
					if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock_status')) update_post_meta( $pid, '_stock_status', 'outofstock' );
				}

			} else {

				// Don't manage stock
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock')) update_post_meta( $pid, '_stock', '' );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock_status')) update_post_meta( $pid, '_stock_status', stripslashes( $product_stock_status[$i] ) );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_backorders')) update_post_meta( $pid, '_backorders', stripslashes( $product_allow_backorders[$i] ) );
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_manage_stock')) update_post_meta( $pid, '_manage_stock', 'no' );

			}

		} else {

			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_stock_status')) update_post_meta( $pid, '_stock_status', stripslashes( $product_stock_status[$i] ) );

		}

		// Upsells
		if ( !empty( $product_up_sells[$i] ) ) {
			$upsells = array();
			$ids = explode(',', $product_up_sells[$i]);
			foreach ( $ids as $id ){								
				$args = array(
					'post_type' => 'product',
					'meta_query' => array(
						array(
							'key' => '_sku',
							'value' => $id,						
						)
					)
				);			
				$query = new WP_Query( $args );
				
				if ( $query->have_posts() ) $upsells[] = $query->post->ID;

				wp_reset_postdata();
			}								

			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_upsell_ids')) update_post_meta( $pid, '_upsell_ids', $upsells );
		} else {
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_upsell_ids')) delete_post_meta( $pid, '_upsell_ids' );
		}

		// Cross sells
		if ( !empty( $product_cross_sells[$i] ) ) {
			$crosssells = array();
			$ids = explode(',', $product_cross_sells[$i]);
			foreach ( $ids as $id ){
				$args = array(
					'post_type' => 'product',
					'meta_query' => array(
						array(
							'key' => '_sku',
							'value' => $id,						
						)
					)
				);			
				$query = new WP_Query( $args );
				
				if ( $query->have_posts() ) $crosssells[] = $query->post->ID;

				wp_reset_postdata();
			}								

			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_crosssell_ids')) update_post_meta( $pid, '_crosssell_ids', $crosssells );
		} else {
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_crosssell_ids')) delete_post_meta( $pid, '_crosssell_ids' );
		}

		// Downloadable options
		if ( $is_downloadable == 'yes' ) {

			$_download_limit = absint( $product_download_limit[$i] );
			if ( ! $_download_limit )
				$_download_limit = ''; // 0 or blank = unlimited

			$_download_expiry = absint( $product_download_expiry[$i] );
			if ( ! $_download_expiry )
				$_download_expiry = ''; // 0 or blank = unlimited

			// file paths will be stored in an array keyed off md5(file path)
			if ( !empty( $product_file_paths[$i] ) ) {
				$_file_paths = array();
				
				$file_paths = explode( $import->options['product_files_delim'] , $product_file_paths[$i] );

				foreach ( $file_paths as $file_path ) {
					$file_path = trim( $file_path );					
					$_file_paths[ md5( $file_path ) ] = $file_path;
				}				

				// grant permission to any newly added files on any existing orders for this product
				do_action( 'woocommerce_process_product_file_download_paths', $pid, 0, $_file_paths );

				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_file_paths')) update_post_meta( $pid, '_file_paths', $_file_paths );
			}
			if ( isset( $product_download_limit[$i] ) )
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_download_limit')) update_post_meta( $pid, '_download_limit', esc_attr( $_download_limit ) );
			if ( isset( $product_download_expiry[$i] ) )
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_download_expiry')) update_post_meta( $pid, '_download_expiry', esc_attr( $_download_expiry ) );
		}

		// Product url
		if ( $product_type == 'external' ) {
			if ( isset( $product_url[$i] ) && $product_url[$i] ){
				$this->auto_cloak_links($import, $product_url[$i]);				
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_product_url')) update_post_meta( $pid, '_product_url', esc_attr( $product_url[$i] ) );
			}
			if ( isset( $product_button_text[$i] ) && $product_button_text[$i] ){
				if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_button_text')) update_post_meta( $pid, '_button_text', esc_attr( $product_button_text[$i] ) );
			}
		}						

		// Do action for product type
		do_action( 'woocommerce_process_product_meta_' . $product_type, $pid );

		// Clear cache/transients
		$woocommerce->clear_product_transients( $pid );								

		// VARIATIONS
		if (("" != $single_product_parent_ID[$i] or "manual" == $import->options['matching_parent']) and $product_type == 'variable' and ! $import->options['link_all_variations']){

			$first_is_parent = "yes";

			if ( "auto" == $import->options['matching_parent']){
				$parentRecord = new PMXI_Post_Record();

				$parentRecord->clear();
				// find corresponding article among previously imported
				$parentRecord->getBy(array(
					'product_key' => $single_product_parent_ID[$i],
					'import_id' => $import->id,
				));
				if ( ! $parentRecord->isEmpty() ) 
					$product_parent_post = get_post($product_parent_post_id = $parentRecord->post_id);	

				$first_is_parent = $import->options['first_is_parent'];
			}
			else {													
				// handle duplicates according to import settings
				if ($duplicates = $this->findDuplicates($articleData, $custom_parent_indicator_name[$i], $custom_parent_indicator_value[$i], $import->options['parent_indicator'])) {															
					$duplicate_id = array_shift($duplicates);
					if ($duplicate_id) {														
						$product_parent_post = get_post($product_parent_post_id = $duplicate_id);
					}						
				}

			}								

			if ($product_parent_post_id and ($product_parent_post_id != $pid or ($product_parent_post_id == $pid and $first_is_parent == "no"))) {

				$create_new_variation = ($product_parent_post_id == $pid and $first_is_parent == "no") ? true : false;
				
				if ($create_new_variation){

					$postRecord = new PMXI_Post_Record();
					
					$postRecord->clear();
					// find corresponding article among previously imported
					$postRecord->getBy(array(
						'unique_key' => 'Variation ' . $new_sku,
						'import_id' => $import->id,
					));
					if ( ! $postRecord->isEmpty() ) 
						$pid = $postRecord->post_id;
					else 
						$pid = false;
						
				}

				$variable_enabled = ($product_enabled[$i] == "yes") ? 'yes' : 'no'; 

				$attributes = (array) maybe_unserialize( get_post_meta( $product_parent_post_id, '_product_attributes', true ) );

				// Enabled or disabled
				$post_status = ( $variable_enabled == 'yes' ) ? 'publish' : 'private';

				// Generate a useful post title
				$variation_post_title = sprintf( __( 'Variation #%s of %s', 'woocommerce' ), absint( $pid ), esc_html( get_the_title( $product_parent_post_id ) ) );

				// Update or Add post							

					$variation = array(
						'post_title' 	=> $variation_post_title,
						'post_content' 	=> '',
						'post_status' 	=> $post_status,									
						'post_parent' 	=> $product_parent_post_id,
						'post_type' 	=> 'product_variation'									
					);

				if ( ! $pid ) {

					$pid = wp_insert_post( $variation );

					if ($create_new_variation){
						
						$this->duplicate_post_copy_post_meta_info($pid, $product_parent_post);

						// associate variation with import
						$postRecord->isEmpty() and $postRecord->set(array(
							'post_id' => $pid,
							'import_id' => $import->id,
							'unique_key' => 'Variation ' . $new_sku,
							'product_key' => ''
						))->insert();
					}

					do_action( 'woocommerce_create_product_variation', $pid );

				} else {

					if ($create_new_variation) $this->duplicate_post_copy_post_meta_info($pid, $product_parent_post);											

					$this->wpdb->update( $this->wpdb->posts, $variation, array( 'ID' => $pid ) );

					do_action( 'woocommerce_update_product_variation', $pid );

				}

				if ( $product_tax_class[ $i ] !== 'parent' )
					update_post_meta( $pid, '_tax_class', sanitize_text_field( $product_tax_class[ $i ] ) );
				else
					delete_post_meta( $pid, '_tax_class' );

				if ( $is_downloadable == 'yes' ) {
					update_post_meta( $pid, '_download_limit', sanitize_text_field( $product_download_limit[ $i ] ) );
					update_post_meta( $pid, '_download_expiry', sanitize_text_field( $product_download_expiry[ $i ] ) );

					$_file_paths = array();
					
					if ( !empty($product_file_paths[$i]) ) {
						$file_paths = explode( $import->options['product_files_delim'] , $product_file_paths[$i] );

						foreach ( $file_paths as $file_path ) {
							$file_path = sanitize_text_field( $file_path );							
							$_file_paths[ md5( $file_path ) ] = $file_path;
						}
					}

					// grant permission to any newly added files on any existing orders for this product
					do_action( 'woocommerce_process_product_file_download_paths', $product_parent_post_id, $pid, $_file_paths );

					update_post_meta( $pid, '_file_paths', $_file_paths );
				} else {
					update_post_meta( $pid, '_download_limit', '' );
					update_post_meta( $pid, '_download_expiry', '' );
					update_post_meta( $pid, '_file_paths', '' );
				}

				// Remove old taxonomies attributes so data is kept up to date
				if ( $pid ) {
					$this->wpdb->query( $this->wpdb->prepare( "DELETE FROM {$this->wpdb->postmeta} WHERE meta_key LIKE 'attribute_%%' AND post_id = %d;", $pid ) );
					wp_cache_delete( $pid, 'post_meta');
				}

				// Update taxonomies
				foreach ($serialized_attributes as $attr_name => $attr_data) {
					
					if ( intval($attr_data['in_taxonomy'][$i]) and ( strpos($attr_name, "pa_") === false or strpos($attr_name, "pa_") != 0 ) ) $attr_name = "pa_" . $attr_name;
													
					$is_variation 	= intval( $attr_data['in_variation'][$i]);								

					if ($is_variation){
						// Don't use woocommerce_clean as it destroys sanitized characters
						$values = sanitize_title($attr_data['value'][$i]);									

						update_post_meta( $pid, 'attribute_' . sanitize_title( $attr_name ), $values );
					}

				}				

				do_action( 'woocommerce_save_product_variation', $pid );

				// Update parent if variable so price sorting works and stays in sync with the cheapest child
				$post_parent = $product_parent_post_id;

				$children = get_posts( array(
					'post_parent' 	=> $post_parent,
					'posts_per_page'=> -1,
					'post_type' 	=> 'product_variation',
					'fields' 		=> 'ids',
					'post_status'	=> 'publish'
				) );

				$lowest_price = $lowest_regular_price = $lowest_sale_price = $highest_price = $highest_regular_price = $highest_sale_price = '';

				if ( $children ) {
					foreach ( $children as $child ) {

						$child_price 			= get_post_meta( $child, '_price', true );
						$child_regular_price 	= get_post_meta( $child, '_regular_price', true );
						$child_sale_price 		= get_post_meta( $child, '_sale_price', true );

						// Regular prices
						if ( ! is_numeric( $lowest_regular_price ) || $child_regular_price < $lowest_regular_price )
							$lowest_regular_price = $child_regular_price;

						if ( ! is_numeric( $highest_regular_price ) || $child_regular_price > $highest_regular_price )
							$highest_regular_price = $child_regular_price;

						// Sale prices
						if ( $child_price == $child_sale_price ) {
							if ( $child_sale_price !== '' && ( ! is_numeric( $lowest_sale_price ) || $child_sale_price < $lowest_sale_price ) )
								$lowest_sale_price = $child_sale_price;

							if ( $child_sale_price !== '' && ( ! is_numeric( $highest_sale_price ) || $child_sale_price > $highest_sale_price ) )
								$highest_sale_price = $child_sale_price;
						}
					}

			    	$lowest_price 	= $lowest_sale_price === '' || $lowest_regular_price < $lowest_sale_price ? $lowest_regular_price : $lowest_sale_price;
					$highest_price 	= $highest_sale_price === '' || $highest_regular_price > $highest_sale_price ? $highest_regular_price : $highest_sale_price;
				}

				update_post_meta( $post_parent, '_price', $lowest_price );
				update_post_meta( $post_parent, '_min_variation_price', $lowest_price );
				update_post_meta( $post_parent, '_max_variation_price', $highest_price );
				update_post_meta( $post_parent, '_min_variation_regular_price', $lowest_regular_price );
				update_post_meta( $post_parent, '_max_variation_regular_price', $highest_regular_price );
				update_post_meta( $post_parent, '_min_variation_sale_price', $lowest_sale_price );
				update_post_meta( $post_parent, '_max_variation_sale_price', $highest_sale_price );

				// Update default attribute options setting
				$default_attributes = array();
				$parent_attributes = array();
				$attribute_position = 0;

				foreach ( $attributes as $attribute ) {															

					$default_attributes[ $attribute['name'] ] = array();
					$values = array();
					foreach ( $children as $child ) {
						if ( $attribute['is_variation'] ) {
							
							$value = esc_attr(trim( get_post_meta($child, 'attribute_'.$attribute['name'], true)));

							if (!empty($value) and !in_array($value, $values))  $values[] = $value;

							if (!empty($value) and empty($default_attributes[ $attribute['name'] ]))
								$default_attributes[ $attribute['name'] ] = $value;
							
						}
					}

					if ( $attribute['is_taxonomy'] ){

						if ( ! empty($values) ) {				 							 									

						 	// Remove empty items in the array
						 	$values = array_filter( $values );

						 	if ( ! empty($values) ){

						 		$attr_values = array();
						 		
						 		$terms = get_terms($attribute['name'], array('hide_empty' => false));								

						 		if ( ! is_wp_error($terms) ){

							 		foreach ($values as $key => $value) {
							 			$term_founded = false;	
										if ( count($terms) > 0 ){	
										    foreach ( $terms as $term ) {

										    	if ( strtolower($term->name) == trim(strtolower($value)) ) {
										    		$attr_values[] = $term->slug;
										    		$term_founded = true;
										    	}
										    }
										}
									    if ( ! $term_founded and taxonomy_exists( $attribute['name'] ) and $attribute['is_create_taxonomy_terms'] ){
									    	$term = wp_insert_term(
												$value, // the term 
											  	$attribute['name'] // the taxonomy										  	
											);	
											if ( ! is_wp_error($term) ){
												$term = get_term_by( 'id', $term['term_id'], $attribute['name']);
												$attr_values[] = $term->slug; 
											}
									    }
							 		}
						 		} else {
						 			$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: %s.', 'pmxi_plugin'), $terms->get_error_message()));
						 		}

						 		$values = $attr_values;
						 	}

					 	} else {
					 		$values = array();
					 	}

				 		// Update post terms
				 		if ( taxonomy_exists( $attribute['name'] ) )
				 			wp_set_object_terms( $post_parent, $values, $attribute['name'] );

				 		if ( $values ) {
					 		// Add attribute to array, but don't set values
					 		$parent_attributes[ sanitize_title( $attribute['name'] ) ] = array(
						 		'name' 			=> sanitize_title( $attribute['name'] ),
						 		'value' 		=> '',
						 		'position' 		=> $attribute_position,
						 		'is_visible' 	=> $attribute['is_visible'],
						 		'is_variation' 	=> $attribute['is_variation'],
						 		'is_taxonomy' 	=> 1,
						 		'is_create_taxonomy_terms' => $attribute['is_create_taxonomy_terms'],
						 	);
					 	}

					}
					else
					{
						$parent_attributes[ sanitize_title( $attribute['name'] ) ] = array(
					 		'name' 			=> sanitize_text_field( $attribute['name'] ),
					 		'value' 		=> implode('|', $values),
					 		'position' 		=> $attribute_position,
					 		'is_visible' 	=> $attribute['is_visible'],
					 		'is_variation' 	=> $attribute['is_variation'],
					 		'is_taxonomy' 	=> 0
					 	);
					}

				 	$attribute_position++;		
				}				

				update_post_meta( $post_parent, '_default_attributes', $default_attributes );												
		
				update_post_meta( $post_parent, '_product_attributes', $parent_attributes );

			}
			else{								

				$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: Parent froduct for %s was not found.', 'pmxi_plugin'), $articleData['post_title']));							
			}

		} elseif ( in_array( $product_type, array( 'variable', 'grouped' ) ) ){

			// Link All Variations
			if ( "variable" == $product_type and $import->options['link_all_variations'] ){

				$added_variations = $this->pmwi_link_all_variations($pid);

				$logger and call_user_func($logger, sprintf(__('<b>CREATED</b>: %s variations for parent froduct %s.', 'pmxi_plugin'), $added_variations, $articleData['post_title']));	

			}

			// Variable and grouped products have no prices
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_regular_price')) update_post_meta( $pid, '_regular_price', '' );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price')) update_post_meta( $pid, '_sale_price', '' );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_from')) update_post_meta( $pid, '_sale_price_dates_from', '' );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_sale_price_dates_to')) update_post_meta( $pid, '_sale_price_dates_to', '' );
			if ($this->keep_custom_fields($existing_meta_keys, $import->options, '_price')) update_post_meta( $pid, '_price', '' );			

		}

		// Find children elements by XPath and create variations
		if ( "variable" == $product_type and "xml" == $import->options['matching_parent'] and "" != $import->options['variations_xpath'] and "" != $import->options['variable_sku'] and ! $import->options['link_all_variations']) {
			
			$variation_xpath = $import->xpath . '/'.  ltrim(trim(str_replace("[*]","",$import->options['variations_xpath']),'{}'), '/');
			
			$records = array();

			$variation_sku = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_sku'], $file)->parse($records); $tmp_files[] = $file;
			$count_variations = count($variation_sku);

			if ( $count_variations > 0 ):
				// Stock Qty
				if (!empty($import->options['variable_stock'])){
					if ($import->options['variable_stock_use_parent']){
						$parent_variation_stock = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_stock'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_variation_stock[0])){
							count($variation_sku) and $variation_stock = array_fill(0, count($variation_sku), $parent_variation_stock[0]);
						}
						else{
							count($variation_sku) and $variation_stock = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_stock = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_stock'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_stock = array_fill(0, count($variation_sku), '');
				}

				// Image			
				$variation_image = array();				
				if ($import->options['variable_image']) {
					
					if ($import->options['variable_image_use_parent']){
						$parent_image = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_image'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_image[0])){
							count($variation_sku) and $variation_image = array_fill(0, count($variation_sku), $parent_image[0]);
						}
						else{
							count($variation_sku) and $variation_image = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_image = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_image'], $file)->parse($records); $tmp_files[] = $file;	
					}					
					
				} else {
					count($variation_sku) and $variation_image = array_fill(0, count($variation_sku), '');
				}

				// Regular Price
				if (!empty($import->options['variable_regular_price'])){
					if ($import->options['variable_regular_price_use_parent']){
						$parent_regular_price = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_regular_price'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_regular_price[0])){
							count($variation_sku) and $variation_regular_price = array_fill(0, count($variation_sku), $parent_regular_price[0]);
						}
						else{
							count($variation_sku) and $variation_regular_price = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_regular_price = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_regular_price'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_regular_price = array_fill(0, count($variation_sku), '');
				}

				// Sale Price
				if (!empty($import->options['variable_sale_price'])){
					if ($import->options['variable_sale_price_use_parent']){
						$parent_sale_price = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_sale_price'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_sale_price[0])){
							count($variation_sku) and $variation_sale_price = array_fill(0, count($variation_sku), $parent_sale_price[0]);
						}
						else{
							count($variation_sku) and $variation_sale_price = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_sale_price = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_sale_price'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_sale_price = array_fill(0, count($variation_sku), '');
				}	

				if ( $import->options['is_variable_sale_price_shedule']){
					// Sale price dates from
					if (!empty($import->options['variable_sale_price_dates_from'])){

						if ($import->options['variable_sale_dates_use_parent']){
							$parent_sale_date_start = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_sale_price_dates_from'], $file)->parse($records); $tmp_files[] = $file;
							if (!empty($parent_sale_date_start[0])){
								count($variation_sku) and $variation_sale_price_dates_from = array_fill(0, count($variation_sku), $parent_sale_date_start[0]);
							}
							else{
								count($variation_sku) and $variation_sale_price_dates_from = array_fill(0, count($variation_sku), '');
							}
						}
						else {
							$variation_sale_price_dates_from = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_sale_price_dates_from'], $file)->parse($records); $tmp_files[] = $file;
						}
					}
					else{
						count($variation_sku) and $variation_sale_price_dates_from = array_fill(0, count($variation_sku), '');
					}

					// Sale price dates to
					if (!empty($import->options['variable_sale_price_dates_to'])){
						
						if ($import->options['variable_sale_dates_use_parent']){
							$parent_sale_date_end = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_sale_price_dates_to'], $file)->parse($records); $tmp_files[] = $file;
							if (!empty($parent_sale_date_end[0])){
								count($variation_sku) and $variation_sale_price_dates_to = array_fill(0, count($variation_sku), $parent_sale_date_end[0]);
							}
							else{
								count($variation_sku) and $variation_sale_price_dates_to = array_fill(0, count($variation_sku), '');
							}
						}
						else {
							$variation_sale_price_dates_to = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_sale_price_dates_to'], $file)->parse($records); $tmp_files[] = $file;
						}						
					}
					else{
						count($variation_sku) and $variation_sale_price_dates_to = array_fill(0, count($variation_sku), '');
					}
				}			

				// Composing product is Virtual									
				if ($import->options['is_variable_product_virtual'] == 'xpath' and "" != $import->options['single_variable_product_virtual']){
					if ($import->options['single_variable_product_virtual_use_parent']){
						$parent_variable_product_virtual = XmlImportParser::factory($xml, $import->xpath, $import->options['single_variable_product_virtual'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_variable_product_virtual[0])){
							count($variation_sku) and $variation_product_virtual = array_fill(0, count($variation_sku), $parent_variable_product_virtual[0]);
						}
						else{
							count($variation_sku) and $variation_product_virtual = array_fill(0, count($variation_sku), $import->options['is_variable_product_virtual']);
						}
					}
					else {
						$variation_product_virtual = XmlImportParser::factory($xml, $variation_xpath, $import->options['single_variable_product_virtual'], $file)->parse($records); $tmp_files[] = $file;						
					}
				}
				else{
					count($variation_sku) and $variation_product_virtual = array_fill(0, count($variation_sku), $import->options['is_variable_product_virtual']);
				}

				// Composing product is Downloadable									
				if ($import->options['is_variable_product_downloadable'] == 'xpath' and "" != $import->options['single_variable_product_downloadable']){
					if ($import->options['single_variable_product_downloadable_use_parent']){
						$parent_variable_product_downloadable = XmlImportParser::factory($xml, $import->xpath, $import->options['single_variable_product_downloadable'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_variable_product_downloadable[0])){
							count($variation_sku) and $variation_product_downloadable = array_fill(0, count($variation_sku), $parent_variable_product_downloadable[0]);
						}
						else{
							count($variation_sku) and $variation_product_downloadable = array_fill(0, count($variation_sku), $import->options['is_variable_product_downloadable']);
						}
					}
					else {
						$variation_product_downloadable = XmlImportParser::factory($xml, $variation_xpath, $import->options['single_variable_product_downloadable'], $file)->parse($records); $tmp_files[] = $file;						
					}
				}
				else{
					count($variation_sku) and $variation_product_downloadable = array_fill(0, count($variation_sku), $import->options['is_variable_product_downloadable']);
				}

				// Weigth										
				if (!empty($import->options['variable_weight'])){
					if ($import->options['variable_weight_use_parent']){
						$parent_weight = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_weight'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_weight[0])){
							count($variation_sku) and $variation_weight = array_fill(0, count($variation_sku), $parent_weight[0]);
						}
						else{
							count($variation_sku) and $variation_weight = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_weight = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_weight'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_weight = array_fill(0, count($variation_sku), '');
				}

				// Length										
				if (!empty($import->options['variable_length'])){
					if ($import->options['variable_dimensions_use_parent']){
						$parent_length = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_length'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_length[0])){
							count($variation_sku) and $variation_length = array_fill(0, count($variation_sku), $parent_length[0]);
						}
						else{
							count($variation_sku) and $variation_length = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_length = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_length'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_length = array_fill(0, count($variation_sku), '');
				}

				// Width
				if (!empty($import->options['variable_width'])){
					if ($import->options['variable_dimensions_use_parent']){
						$parent_width = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_width'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_width[0])){
							count($variation_sku) and $variation_width = array_fill(0, count($variation_sku), $parent_width[0]);
						}
						else{
							count($variation_sku) and $variation_width = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_width = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_width'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_width = array_fill(0, count($variation_sku), '');
				}

				// Heigth										
				if (!empty($import->options['variable_height'])){
					if ($import->options['variable_dimensions_use_parent']){
						$parent_heigth = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_height'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_heigth[0])){
							count($variation_sku) and $variation_height = array_fill(0, count($variation_sku), $parent_heigth[0]);
						}
						else{
							count($variation_sku) and $variation_height = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_height = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_height'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_height = array_fill(0, count($variation_sku), '');
				}
				
				// Composing product Shipping Class				
				if ($import->options['is_multiple_variable_product_shipping_class'] != 'yes' and "" != $import->options['single_variable_product_shipping_class']){
					if ($import->options['single_variable_product_shipping_class_use_parent']){
						$parent_shipping_class = XmlImportParser::factory($xml, $import->xpath, $import->options['single_variable_product_shipping_class'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_shipping_class[0])){
							count($variation_sku) and $variation_product_shipping_class = array_fill(0, count($variation_sku), $parent_shipping_class[0]);
						}
						else{
							count($variation_sku) and $variation_product_shipping_class = array_fill(0, count($variation_sku), $import->options['multiple_variable_product_shipping_class']);
						}
					}
					else {
						$variation_product_shipping_class = XmlImportParser::factory($xml, $variation_xpath, $import->options['single_variable_product_shipping_class'], $file)->parse($records); $tmp_files[] = $file;						
					}
				}
				else{
					count($variation_sku) and $variation_product_shipping_class = array_fill(0, count($variation_sku), $import->options['multiple_variable_product_shipping_class']);
				}

				// Composing product Tax Class				
				if ($import->options['is_multiple_variable_product_tax_class'] != 'yes' and "" != $import->options['single_variable_product_tax_class']){
					if ($import->options['single_variable_product_tax_class_use_parent']){
						$parent_tax_class = XmlImportParser::factory($xml, $import->xpath, $import->options['single_variable_product_tax_class'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_tax_class[0])){
							count($variation_sku) and $variation_product_tax_class = array_fill(0, count($variation_sku), $parent_tax_class[0]);
						}
						else{
							count($variation_sku) and $variation_product_tax_class = array_fill(0, count($variation_sku), $import->options['multiple_variable_product_tax_class']);
						}
					}
					else {
						$variation_product_tax_class = XmlImportParser::factory($xml, $variation_xpath, $import->options['single_variable_product_tax_class'], $file)->parse($records); $tmp_files[] = $file;						
					}
				}
				else{
					count($variation_sku) and $variation_product_tax_class = array_fill(0, count($variation_sku), $import->options['multiple_variable_product_tax_class']);
				}

				// Download limit										
				if (!empty($import->options['variable_download_limit'])){
					if ($import->options['variable_download_limit_use_parent']){
						$parent_download_limit = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_download_limit'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_download_limit[0])){
							count($variation_sku) and $variation_download_limit = array_fill(0, count($variation_sku), $parent_download_limit[0]);
						}
						else{
							count($variation_sku) and $variation_download_limit = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_download_limit = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_download_limit'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_download_limit = array_fill(0, count($variation_sku), '');
				}

				// Download expiry										
				if (!empty($import->options['variable_download_expiry'])){
					if ($import->options['variable_download_expiry_use_parent']){
						$parent_download_expiry = XmlImportParser::factory($xml, $import->xpath, $import->options['variable_download_expiry'], $file)->parse($records); $tmp_files[] = $file;
						if (!empty($parent_download_expiry[0])){
							count($variation_sku) and $variation_download_expiry = array_fill(0, count($variation_sku), $parent_download_expiry[0]);
						}
						else{
							count($variation_sku) and $variation_download_expiry = array_fill(0, count($variation_sku), '');
						}
					}
					else {
						$variation_download_expiry = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_download_expiry'], $file)->parse($records); $tmp_files[] = $file;
					}
				}
				else{
					count($variation_sku) and $variation_download_expiry = array_fill(0, count($variation_sku), '');
				}

				// File paths								
				if (!empty($import->options['variable_file_paths'])){
					$variation_file_paths = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_file_paths'], $file)->parse($records); $tmp_files[] = $file;
				}
				else{
					count($variation_sku) and $variation_file_paths = array_fill(0, count($variation_sku), '');
				}

				// Variation enabled								
				if ($import->options['is_variable_product_enabled'] == 'xpath' and "" != $import->options['single_variable_product_enabled']){
					$variation_product_enabled = XmlImportParser::factory($xml, $variation_xpath, $import->options['single_variable_product_enabled'], $file)->parse($records); $tmp_files[] = $file;						
				}
				else{
					count($variation_sku) and $variation_product_enabled = array_fill(0, count($variation_sku), $import->options['is_variable_product_enabled']);
				}

				$variation_attribute_keys = array(); 
				$variation_attribute_values = array();	
				$variation_attribute_in_variation = array(); 
				$variation_attribute_is_visible = array();
				$variation_attribute_in_taxonomy = array();			
				$variable_create_terms_in_not_exists = array();
									
				if (!empty($import->options['variable_attribute_name'][0])){
					foreach ($import->options['variable_attribute_name'] as $j => $attribute_name) { if ($attribute_name == "") continue;						
						$variation_attribute_keys[$j]   = XmlImportParser::factory($xml, $variation_xpath, $attribute_name, $file)->parse($records); $tmp_files[] = $file;
						$variation_attribute_values[$j] = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_attribute_value'][$j], $file)->parse($records); $tmp_files[] = $file;
						$variation_attribute_in_variation[$j] = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_in_variations'][$j], $file)->parse($records); $tmp_files[] = $file;
						$variation_attribute_is_visible[$j] = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_is_visible'][$j], $file)->parse($records); $tmp_files[] = $file;						
						$variation_attribute_in_taxonomy[$j] = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_is_taxonomy'][$j], $file)->parse($records); $tmp_files[] = $file;						
						$variable_create_terms_in_not_exists[$j] = XmlImportParser::factory($xml, $variation_xpath, $import->options['variable_create_taxonomy_in_not_exists'][$j], $file)->parse($records); $tmp_files[] = $file;
					}
				}					

				// serialized attributes for product variations
				$variation_serialized_attributes = array();
				if (!empty($variation_attribute_keys)){
					foreach ($variation_attribute_keys as $j => $attribute_name) {											
						if (!in_array($attribute_name[0], array_keys($variation_serialized_attributes))){
							$variation_serialized_attributes[$attribute_name[0]] = array(
								'value' => $variation_attribute_values[$j],
								'is_visible' => $variation_attribute_is_visible[$j],
								'in_variation' => $variation_attribute_in_variation[$j],
								'in_taxonomy' => $variation_attribute_in_taxonomy[$j],
								'is_create_taxonomy_terms' => $variable_create_terms_in_not_exists[$j]
							);						
						}							
					}
				} 

				// Create Variations
				foreach ($variation_sku as $j => $void) {	if ("" == $variation_sku[$j]) continue;

					$variable_enabled = ($variation_product_enabled[$j] == "yes") ? 'yes' : 'no'; 					

					// Enabled or disabled
					$post_status = ( $variable_enabled == 'yes' ) ? 'publish' : 'private';

					$variation_to_update_id = false;

					$postRecord = new PMXI_Post_Record();
					
					$postRecord->clear();
					// find corresponding article among previously imported
					$postRecord->getBy(array(
						'unique_key' => 'Variation ' . $variation_sku[$j],
						'import_id' => $import->id,
					));
					if ( ! $postRecord->isEmpty() ) 
						$variation_to_update = get_post($variation_to_update_id = $postRecord->post_id);

					// Generate a useful post title
					$variation_post_title = sprintf( __( 'Variation #%s of %s', 'woocommerce' ), absint( $variation_to_update_id ), esc_html( get_the_title( $pid ) ) );

					// Update or Add post							

						$variation = array(
							'post_title' 	=> $variation_post_title,
							'post_content' 	=> '',
							'post_status' 	=> $post_status,									
							'post_parent' 	=> $pid,
							'post_type' 	=> 'product_variation'									
						);

					if ( ! $variation_to_update_id ) {

						$variation_to_update_id = wp_insert_post( $variation );

						do_action( 'woocommerce_create_product_variation', $variation_to_update_id );

						// associate variation with import
						$postRecord->isEmpty() and $postRecord->set(array(
							'post_id' => $variation_to_update_id,
							'import_id' => $import->id,
							'unique_key' => 'Variation ' . $variation_sku[$j],
							'product_key' => ''
						))->insert();

						$logger and call_user_func($logger, sprintf(__('`%s`: variation created successfully', 'pmxi_plugin'), sprintf( __( 'Variation #%s of %s', 'woocommerce' ), absint( $variation_to_update_id ), esc_html( get_the_title( $pid ) ) )));

					} else {

						$this->wpdb->update( $this->wpdb->posts, $variation, array( 'ID' => $variation_to_update_id ) );

						do_action( 'woocommerce_update_product_variation', $variation_to_update_id );

						$logger and call_user_func($logger, sprintf(__('`%s`: variation updated successfully', 'pmxi_plugin'), $variation_post_title));
					}

					PMXI_Plugin::$session['pmxi_import']['current_post_ids'][]  = $variation_to_update_id;
					if ($is_cron){
						$tmp_array = (!empty($import->current_post_ids)) ? json_decode($import->current_post_ids, true) : array();
						$tmp_array[] = $variation_to_update_id;
						$this->set(array(
							'current_post_ids' => json_encode($tmp_array)
						))->save();
					}

					// Add any default post meta
					add_post_meta( $variation_to_update_id, 'total_sales', '0', true );
					
					// Product type + Downloadable/Virtual
					wp_set_object_terms( $variation_to_update_id, $product_type, 'product_type' );
					update_post_meta( $variation_to_update_id, '_downloadable', ($variation_product_downloadable[$j] == "yes") ? 'yes' : 'no' );
					update_post_meta( $variation_to_update_id, '_virtual', ($variation_product_virtual[$j] == "yes") ? 'yes' : 'no' );						

					// Update post meta
					update_post_meta( $variation_to_update_id, '_regular_price', stripslashes( $variation_regular_price[$j] ) );
					update_post_meta( $variation_to_update_id, '_sale_price', stripslashes( $variation_sale_price[$j] ) );

					// Dimensions
					if ( $variation_product_virtual[$j] == 'no' ) {
						update_post_meta( $variation_to_update_id, '_weight', stripslashes( $variation_weight[$i] ) );
						update_post_meta( $variation_to_update_id, '_length', stripslashes( $variation_length[$i] ) );
						update_post_meta( $variation_to_update_id, '_width', stripslashes( $variation_width[$i] ) );
						update_post_meta( $variation_to_update_id, '_height', stripslashes( $variation_height[$i] ) );
					} else {
						update_post_meta( $variation_to_update_id, '_weight', '' );
						update_post_meta( $variation_to_update_id, '_length', '' );
						update_post_meta( $variation_to_update_id, '_width', '' );
						update_post_meta( $variation_to_update_id, '_height', '' );
					}

					// Save shipping class					
					wp_set_object_terms( $variation_to_update_id, $variation_product_shipping_class[$j], 'product_shipping_class');

					// Unique SKU
					$sku				= get_post_meta($variation_to_update_id, '_sku', true);
					$new_sku 			= esc_html( trim( stripslashes( $variation_sku[$j] ) ) );
					
					if ( $new_sku == '' ) {
						update_post_meta( $variation_to_update_id, '_sku', '' );
					} elseif ( $new_sku !== $sku ) {
						if ( ! empty( $new_sku ) ) {
							if (
								$this->wpdb->get_var( $this->wpdb->prepare("
									SELECT ".$this->wpdb->posts.".ID
								    FROM ".$this->wpdb->posts."
								    LEFT JOIN ".$this->wpdb->postmeta." ON (".$this->wpdb->posts.".ID = ".$this->wpdb->postmeta.".post_id)
								    WHERE ".$this->wpdb->posts.".post_type = 'product'
								    AND ".$this->wpdb->posts.".post_status = 'publish'
								    AND ".$this->wpdb->postmeta.".meta_key = '_sku' AND ".$this->wpdb->postmeta.".meta_value = '%s'
								 ", $new_sku ) )
								) {
								$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: Product SKU must be unique.', 'pmxi_plugin')));
								
							} else {
								update_post_meta( $variation_to_update_id, '_sku', $new_sku );
							}
						} else {
							update_post_meta( $variation_to_update_id, '_sku', '' );
						}
					}

					$date_from = isset( $variation_sale_price_dates_from[$j] ) ? $variation_sale_price_dates_from[$j] : '';
					$date_to = isset( $variation_sale_price_dates_to[$i] ) ? $variation_sale_price_dates_to[$i] : '';

					// Dates
					if ( $date_from )
						update_post_meta( $variation_to_update_id, '_sale_price_dates_from', strtotime( $date_from ) );
					else
						update_post_meta( $variation_to_update_id, '_sale_price_dates_from', '' );

					if ( $date_to )
						update_post_meta( $variation_to_update_id, '_sale_price_dates_to', strtotime( $date_to ) );
					else
						update_post_meta( $variation_to_update_id, '_sale_price_dates_to', '' );

					if ( $date_to && ! $date_from )
						update_post_meta( $variation_to_update_id, '_sale_price_dates_from', strtotime( 'NOW', current_time( 'timestamp' ) ) );

					// Update price if on sale
					if ( $variation_sale_price[$j] != '' && $date_to == '' && $date_from == '' )
						update_post_meta( $variation_to_update_id, '_price', stripslashes( $variation_sale_price[$j] ) );
					else
						update_post_meta( $variation_to_update_id, '_price', stripslashes( $variation_regular_price[$j] ) );

					if ( $variation_sale_price[$j] != '' && $date_from && strtotime( $date_from ) < strtotime( 'NOW', current_time( 'timestamp' ) ) )
						update_post_meta( $variation_to_update_id, '_price', stripslashes($variation_sale_price[$j]) );

					if ( $date_to && strtotime( $date_to ) < strtotime( 'NOW', current_time( 'timestamp' ) ) ) {
						update_post_meta( $variation_to_update_id, '_price', stripslashes($variation_regular_price[$j]) );
						update_post_meta( $variation_to_update_id, '_sale_price_dates_from', '');
						update_post_meta( $variation_to_update_id, '_sale_price_dates_to', '');
					}

					// Manage stock
					update_post_meta( $variation_to_update_id, '_stock', (!empty($variation_stock[$j])) ? $variation_stock[$j] : '' );

					if ( $variation_product_tax_class[ $j ] !== 'parent' )
						update_post_meta( $variation_to_update_id, '_tax_class', sanitize_text_field( $variation_product_tax_class[ $j ] ) );
					else
						delete_post_meta( $variation_to_update_id, '_tax_class' );

					if ( $variation_product_downloadable[$j] == 'yes' ) {
						update_post_meta( $variation_to_update_id, '_download_limit', sanitize_text_field( $variation_download_limit[ $j ] ) );
						update_post_meta( $variation_to_update_id, '_download_expiry', sanitize_text_field( $variation_download_expiry[ $j ] ) );

						$_file_paths = array();
						
						if ( !empty($variation_file_paths[$j]) ) {
							$file_paths = explode( $import->options['variable_product_files_delim'] , $variation_file_paths[$j] );

							foreach ( $file_paths as $file_path ) {
								$file_path = sanitize_text_field( $file_path );								
								$_file_paths[ md5( $file_path ) ] = $file_path;
							}
						}

						// grant permission to any newly added files on any existing orders for this product
						do_action( 'woocommerce_process_product_file_download_paths', $pid, $variation_to_update_id, $_file_paths );

						update_post_meta( $variation_to_update_id, '_file_paths', $_file_paths );
					} else {
						update_post_meta( $variation_to_update_id, '_download_limit', '' );
						update_post_meta( $variation_to_update_id, '_download_expiry', '' );
						update_post_meta( $variation_to_update_id, '_file_paths', '' );
					}

					// Remove old taxonomies attributes so data is kept up to date
					if ( $variation_to_update_id ) {
						$this->wpdb->query( $this->wpdb->prepare( "DELETE FROM {$this->wpdb->postmeta} WHERE meta_key LIKE 'attribute_%%' AND post_id = %d;", $variation_to_update_id ) );
						wp_cache_delete( $variation_to_update_id, 'post_meta');
					}

					// Update taxonomies
					foreach ($variation_serialized_attributes as $attr_name => $attr_data) {																										

						$is_variation 	= ( intval($attr_data['in_variation'][$j]) ) ? 1 : 0;								

						if ($is_variation){
							// Don't use woocommerce_clean as it destroys sanitized characters
							$values = trim(sanitize_title($attr_data['value'][$j]));									
							
							update_post_meta( $variation_to_update_id, 'attribute_' . (( intval($attr_data['in_taxonomy'][$j])) ? $woocommerce->attribute_taxonomy_name( $attr_name ) : sanitize_title($attr_name)) , $values );
						}
						
					}

					do_action( 'woocommerce_save_product_variation', $variation_to_update_id );			

					if ( ! is_array($variation_image[$j]) ) $variation_image[$j] = array($variation_image[$j]);

					$uploads = wp_upload_dir();

					foreach ($variation_image[$j] as $featured_image)
					{							
						$imgs = str_getcsv($featured_image, ',');

						if (!empty($imgs)) {	

							foreach ($imgs as $img_url) { if (empty($img_url)) continue;									
								$create_image = false;
								
								$img_ext = pmxi_get_remote_image_ext($img_url);
								
								$image_filename = wp_unique_filename($uploads['path'], uniqid() . (("" != $img_ext) ? '.'.$img_ext : ''));
								$image_filepath = $uploads['path'] . '/' . url_title($image_filename);

								$img_url = str_replace(" ", "%20", trim($img_url));

								if ( ! get_file_curl($img_url, $image_filepath) and ! @file_put_contents($image_filepath, @file_get_contents($img_url))) {										
									$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: File %s cannot be saved locally as %s', 'pmxi_plugin'), $img_url, $image_filepath));
									PMXI_Plugin::$session['pmxi_import']['warnings'] = PMXI_Plugin::$session->data['pmxi_import']['warnings']++;
									unlink($image_filepath); // delete file since failed upload may result in empty file created										
								} elseif( ! ($image_info = @getimagesize($image_filepath)) or ! in_array($image_info[2], array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG))) {
									$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: File %s is not a valid image and cannot be set as featured one', 'pmxi_plugin'), $img_url));
									PMXI_Plugin::$session['pmxi_import']['warnings'] = PMXI_Plugin::$session->data['pmxi_import']['warnings']++;
								} else {
									$create_image = true;											
								}																

								if ($create_image){
									$attachment = array(
										'post_mime_type' => image_type_to_mime_type($image_info[2]),
										'guid' => $uploads['url'] . '/' . $image_filename,
										'post_title' => $image_filename,
										'post_content' => '',
									);
									if (($image_meta = wp_read_image_metadata($image_filepath))) {
										if (trim($image_meta['title']) && ! is_numeric(sanitize_title($image_meta['title'])))
											$attachment['post_title'] = $image_meta['title'];
										if (trim($image_meta['caption']))
											$attachment['post_content'] = $image_meta['caption'];
									}
									$attid = wp_insert_attachment($attachment, $image_filepath, $variation_to_update_id);
									if (is_wp_error($attid)) {
										$logger and call_user_func($logger, __('<b>WARNING</b>', 'pmxi_plugin') . ': ' . $variation_to_update_id->get_error_message());
										PMXI_Plugin::$session['pmxi_import']['warnings'] = PMXI_Plugin::$session->data['pmxi_import']['warnings']++;
									} else {
										// you must first include the image.php file
										// for the function wp_generate_attachment_metadata() to work
										require_once(ABSPATH . 'wp-admin/includes/image.php');
										wp_update_attachment_metadata($attid, wp_generate_attachment_metadata($attid, $image_filepath));										
										set_post_thumbnail($variation_to_update_id, $attid); 										
									}
								}																	
							}									
						}
					}


					$woocommerce->clear_product_transients( $variation_to_update_id );	
				}

				foreach ($tmp_files as $file) { // remove all temporary files created
					unlink($file);
				}

				// Update parent if variable so price sorting works and stays in sync with the cheapest child				

				$children = get_posts( array(
					'post_parent' 	=> $pid,
					'posts_per_page'=> -1,
					'post_type' 	=> 'product_variation',
					'fields' 		=> 'ids',
					'post_status'	=> 'publish'
				) );

				$lowest_price = $lowest_regular_price = $lowest_sale_price = $highest_price = $highest_regular_price = $highest_sale_price = '';

				if ( $children ) {
					foreach ( $children as $child ) {

						$child_price 			= get_post_meta( $child, '_price', true );
						$child_regular_price 	= get_post_meta( $child, '_regular_price', true );
						$child_sale_price 		= get_post_meta( $child, '_sale_price', true );

						// Regular prices
						if ( ! is_numeric( $lowest_regular_price ) || $child_regular_price < $lowest_regular_price )
							$lowest_regular_price = $child_regular_price;

						if ( ! is_numeric( $highest_regular_price ) || $child_regular_price > $highest_regular_price )
							$highest_regular_price = $child_regular_price;

						// Sale prices
						if ( $child_price == $child_sale_price ) {
							if ( $child_sale_price !== '' && ( ! is_numeric( $lowest_sale_price ) || $child_sale_price < $lowest_sale_price ) )
								$lowest_sale_price = $child_sale_price;

							if ( $child_sale_price !== '' && ( ! is_numeric( $highest_sale_price ) || $child_sale_price > $highest_sale_price ) )
								$highest_sale_price = $child_sale_price;
						}
					}

			    	$lowest_price 	= $lowest_sale_price === '' || $lowest_regular_price < $lowest_sale_price ? $lowest_regular_price : $lowest_sale_price;
					$highest_price 	= $highest_sale_price === '' || $highest_regular_price > $highest_sale_price ? $highest_regular_price : $highest_sale_price;
				}

				update_post_meta( $pid, '_price', $lowest_price );
				update_post_meta( $pid, '_min_variation_price', $lowest_price );
				update_post_meta( $pid, '_max_variation_price', $highest_price );
				update_post_meta( $pid, '_min_variation_regular_price', $lowest_regular_price );
				update_post_meta( $pid, '_max_variation_regular_price', $highest_regular_price );
				update_post_meta( $pid, '_min_variation_sale_price', $lowest_sale_price );
				update_post_meta( $pid, '_max_variation_sale_price', $highest_sale_price );

				// Update default attribute options setting
				$default_attributes = array();

				$parent_attributes = array();

				$attribute_position = 0;

				foreach ($variation_serialized_attributes as $attr_name => $attr_data) {
					
					$values = array();

					foreach ($variation_sku as $j => $void) {

						//if ( $attr_data['in_taxonomy'][$j] == "yes") $attr_name = $woocommerce->attribute_taxonomy_name( $attr_name );

						$is_variation 	= ( intval($attr_data['in_variation'][$j]) ) ? 1 : 0;								

						if ($is_variation){

							$value = esc_attr(trim( $attr_data['value'][$j] ));

							if (!empty($value) and !in_array($value, $values))  $values[] = $value;

							if (!empty($value) and empty($default_attributes[ (( intval($attr_data['in_taxonomy'][$j])) ? $woocommerce->attribute_taxonomy_name( $attr_name ) : sanitize_title($attr_name)) ]))
								$default_attributes[ (( intval($attr_data['in_taxonomy'][$j]) ) ? $woocommerce->attribute_taxonomy_name( $attr_name ) : sanitize_title($attr_name)) ] = sanitize_title($value);
						}
					}

					if ( intval($attr_data['in_taxonomy'][0]) ){						

						if ( ! taxonomy_exists( $woocommerce->attribute_taxonomy_name( $attr_name ) ) and intval($attr_data['is_create_taxonomy_terms'][0])){

					 		// Grab the submitted data							
							$attribute_name    = ( isset( $attr_name ) )   ? woocommerce_sanitize_taxonomy_name( stripslashes( (string) $attr_name ) ) : '';
							$attribute_label   = ucwords($attribute_name);
							$attribute_type    = 'select';
							$attribute_orderby = 'menu_order';

							$reserved_terms = array(
								'attachment', 'attachment_id', 'author', 'author_name', 'calendar', 'cat', 'category', 'category__and',
								'category__in', 'category__not_in', 'category_name', 'comments_per_page', 'comments_popup', 'cpage', 'day',
								'debug', 'error', 'exact', 'feed', 'hour', 'link_category', 'm', 'minute', 'monthnum', 'more', 'name',
								'nav_menu', 'nopaging', 'offset', 'order', 'orderby', 'p', 'page', 'page_id', 'paged', 'pagename', 'pb', 'perm',
								'post', 'post__in', 'post__not_in', 'post_format', 'post_mime_type', 'post_status', 'post_tag', 'post_type',
								'posts', 'posts_per_archive_page', 'posts_per_page', 'preview', 'robots', 's', 'search', 'second', 'sentence',
								'showposts', 'static', 'subpost', 'subpost_id', 'tag', 'tag__and', 'tag__in', 'tag__not_in', 'tag_id',
								'tag_slug__and', 'tag_slug__in', 'taxonomy', 'tb', 'term', 'type', 'w', 'withcomments', 'withoutcomments', 'year',
							);

							if ( in_array( $attribute_name, $reserved_terms ) ) {
								$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: Slug “%s” is not allowed because it is a reserved term. Change it, please.', 'pmxi_plugin'), sanitize_title( $attribute_name )));
							}			
							else{
								$this->wpdb->insert(
									$this->wpdb->prefix . 'woocommerce_attribute_taxonomies',
									array(
										'attribute_label'   => $attribute_label,
										'attribute_name'    => $attribute_name,
										'attribute_type'    => $attribute_type,
										'attribute_orderby' => $attribute_orderby,
									)
								);								

								$logger and call_user_func($logger, sprintf(__('<b>CREATED</b>: Taxonomy attribute “%s” have been successfully created.', 'pmxi_plugin'), sanitize_title( $attribute_name )));	
								
								// Register the taxonomy now so that the import works!
								$domain = $woocommerce->attribute_taxonomy_name( $attr_name );
								register_taxonomy( $domain,
							        apply_filters( 'woocommerce_taxonomy_objects_' . $domain, array('product') ),
							        apply_filters( 'woocommerce_taxonomy_args_' . $domain, array(
							            'hierarchical' => true,
							            'show_ui' => false,
							            'query_var' => true,
							            'rewrite' => false,
							        ) )
							    );

								delete_transient( 'wc_attribute_taxonomies' );

								$attribute_taxonomies = $this->wpdb->get_results( "SELECT * FROM " . $this->wpdb->prefix . "woocommerce_attribute_taxonomies" );

								set_transient( 'wc_attribute_taxonomies', $attribute_taxonomies );

								apply_filters( 'woocommerce_attribute_taxonomies', $attribute_taxonomies );
							}

					 	}						

						if ( ! empty($values) and taxonomy_exists( $woocommerce->attribute_taxonomy_name( $attr_name ) ) ) {				 							 		

						 	// Remove empty items in the array
						 	$values = array_filter( $values );

						 	if ( ! empty($values) ){

						 		$attr_values = array();
						 		
						 		$terms = get_terms( $woocommerce->attribute_taxonomy_name( $attr_name ), array('hide_empty' => false));								

						 		if ( ! is_wp_error($terms) ){

							 		foreach ($values as $key => $value) {
							 			$term_founded = false;	
										if ( count($terms) > 0 ){	
										    foreach ( $terms as $term ) {

										    	if ( strtolower($term->name) == trim(strtolower($value)) ) {
										    		$attr_values[] = $term->slug;
										    		$term_founded = true;
										    	}
										    }
										}
									    if ( ! $term_founded and intval($attr_data['is_create_taxonomy_terms'][0]) ){
									    	$term = wp_insert_term(
												$value, // the term 
											  	$woocommerce->attribute_taxonomy_name( $attr_name ) // the taxonomy										  	
											);		
											if ( ! is_wp_error($term) ){
												$term = get_term_by( 'id', $term['term_id'], $woocommerce->attribute_taxonomy_name( $attr_name ));
												$attr_values[] = $term->slug; 
											}
									    }
							 		}
							 	}
							 	else{
							 		$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: %s.', 'pmxi_plugin'), $terms->get_error_message()));
							 	}

						 		$values = $attr_values;
						 	}

					 	} else {
					 		$values = array();
					 	}

				 		// Update post terms
				 		if ( taxonomy_exists( $woocommerce->attribute_taxonomy_name( $attr_name ) ))
				 			wp_set_object_terms( $pid, $values, $woocommerce->attribute_taxonomy_name( $attr_name ));

				 		if ( $values ) {
					 		// Add attribute to array, but don't set values
					 		$parent_attributes[ $woocommerce->attribute_taxonomy_name( $attr_name ) ] = array(
						 		'name' 			=> $woocommerce->attribute_taxonomy_name( $attr_name ),
						 		'value' 		=> '',
						 		'position' 		=> $attribute_position,
						 		'is_visible' 	=> (!empty($attr_data['is_visible'][0])) ? 1 : 0,
						 		'is_variation' 	=> (!empty($attr_data['in_variation'][0])) ? 1 : 0,
						 		'is_taxonomy' 	=> 1,
						 		'is_create_taxonomy_terms' => (!empty( $attr_data['is_create_taxonomy_terms'][0] )) ? 1 : 0
						 	);
					 	}

					}
					else{

						if ( taxonomy_exists( $woocommerce->attribute_taxonomy_name( $attr_name ) ))
							wp_set_object_terms( $pid, NULL, $woocommerce->attribute_taxonomy_name( $attr_name ));

						$parent_attributes[ sanitize_title( $attr_name ) ] = array(
					 		'name' 			=> sanitize_text_field( $attr_name ),
					 		'value' 		=> implode('|', $values),
					 		'position' 		=> $attribute_position,
					 		'is_visible' 	=> (!empty($attr_data['is_visible'][0])) ? 1 : 0,
						 	'is_variation' 	=> (!empty($attr_data['in_variation'][0])) ? 1 : 0,
					 		'is_taxonomy' 	=> 0
					 	);
					}

				 	$attribute_position++;	
					
				}							

				update_post_meta( $pid, '_default_attributes', $default_attributes );												
		
				update_post_meta( $pid, '_product_attributes', $parent_attributes );
				
			endif;	
		}	

		// Clear cache/transients
		$woocommerce->clear_product_transients( $pid );	

	}
	

	function pmwi_link_all_variations($product_id) {

		global $woocommerce;

		@set_time_limit(0);

		$post_id = intval( $product_id );

		if ( ! $post_id ) return 0;

		$variations = array();

		$_product = get_product( $post_id, array( 'product_type' => 'variable' ) );

		$v = $_product->get_attributes();		

		// Put variation attributes into an array
		foreach ( $_product->get_attributes() as $attribute ) {

			if ( ! $attribute['is_variation'] ) continue;

			$attribute_field_name = 'attribute_' . sanitize_title( $attribute['name'] );

			if ( $attribute['is_taxonomy'] ) {
				$post_terms = wp_get_post_terms( $post_id, $attribute['name'] );
				$options = array();
				foreach ( $post_terms as $term ) {
					$options[] = $term->slug;
				}
			} else {
				$options = explode( '|', $attribute['value'] );
			}

			$options = array_map( 'sanitize_title', array_map( 'trim', $options ) );

			$variations[ $attribute_field_name ] = $options;
		}

		// Quit out if none were found
		if ( sizeof( $variations ) == 0 ) return 0;

		// Get existing variations so we don't create duplicates
	    $available_variations = array();

	    foreach( $_product->get_children() as $child_id ) {
	    	$child = $_product->get_child( $child_id );

	        if ( ! empty( $child->variation_id ) ) {
	            $available_variations[] = $child->get_variation_attributes();
	        }
	    }	  

		// Created posts will all have the following data
		$variation_post_data = array(
			'post_title' => 'Product #' . $post_id . ' Variation',
			'post_content' => '',
			'post_status' => 'publish',
			'post_author' => get_current_user_id(),
			'post_parent' => $post_id,
			'post_type' => 'product_variation'
		);
		
		$variation_ids = array();
		$added = 0;
		$possible_variations = $this->array_cartesian( $variations );		

		foreach ( $possible_variations as $variation ) {

			// Check if variation already exists
			if ( in_array( $variation, $available_variations ) )
				continue;

			$variation_id = wp_insert_post( $variation_post_data );			
			
			update_post_meta( $variation_id, '_regular_price', get_post_meta( $post_id, '_regular_price', true ) );
			update_post_meta( $variation_id, '_sale_price', get_post_meta( $post_id, '_sale_price', true ) );
			update_post_meta( $variation_id, '_sale_price_dates_from', get_post_meta( $post_id, '_sale_price_dates_from', true ) );
			update_post_meta( $variation_id, '_sale_price_dates_to', get_post_meta( $post_id, '_sale_price_dates_to', true ) );
			update_post_meta( $variation_id, '_price', get_post_meta( $post_id, '_price', true ) );			

			$variation_ids[] = $variation_id;

			foreach ( $variation as $key => $value ) {
				update_post_meta( $variation_id, $key, $value );
			}

			$added++;

			do_action( 'product_variation_linked', $variation_id );
			
		}		

		$woocommerce->clear_product_transients( $post_id );

		return $added;
	}


	function array_cartesian( $input ) {

		    $result = array();

		    while ( list( $key, $values ) = each( $input ) ) {
		        // If a sub-array is empty, it doesn't affect the cartesian product
		        if ( empty( $values ) ) {
		            continue;
		        }

		        // Special case: seeding the product array with the values from the first sub-array
		        if ( empty( $result ) ) {
		            foreach ( $values as $value ) {
		                $result[] = array( $key => $value );
		            }
		        }
		        else {
		            // Second and subsequent input sub-arrays work like this:
		            //   1. In each existing array inside $product, add an item with
		            //      key == $key and value == first item in input sub-array
		            //   2. Then, for each remaining item in current input sub-array,
		            //      add a copy of each existing array inside $product with
		            //      key == $key and value == first item in current input sub-array

		            // Store all items to be added to $product here; adding them on the spot
		            // inside the foreach will result in an infinite loop
		            $append = array();
		            foreach( $result as &$product ) {
		                // Do step 1 above. array_shift is not the most efficient, but it
		                // allows us to iterate over the rest of the items with a simple
		                // foreach, making the code short and familiar.
		                $product[ $key ] = array_shift( $values );

		                // $product is by reference (that's why the key we added above
		                // will appear in the end result), so make a copy of it here
		                $copy = $product;

		                // Do step 2 above.
		                foreach( $values as $item ) {
		                    $copy[ $key ] = $item;
		                    $append[] = $copy;
		                }

		                // Undo the side effecst of array_shift
		                array_unshift( $values, $product[ $key ] );
		            }

		            // Out of the foreach, we can add to $results now
		            $result = array_merge( $result, $append );
		        }
		    }

		    return $result;
		}

	public function _filter_has_cap_unfiltered_html($caps)
	{
		$caps['unfiltered_html'] = true;
		return $caps;
	}
	
	/**
	 * Find duplicates according to settings
	 */
	public function findDuplicates($articleData, $custom_duplicate_name = '', $custom_duplicate_value = '', $duplicate_indicator = 'title')
	{		
		if ('custom field' == $duplicate_indicator){
			$duplicate_ids = array();
			$args = array(
				'post_type' => $articleData['post_type'],
				'meta_query' => array(
					array(
						'key' => $custom_duplicate_name,
						'value' => $custom_duplicate_value,						
					)
				)
			);			
			$query = new WP_Query( $args );
			
			if ( $query->have_posts() ) $duplicate_ids[] = $query->post->ID;

			wp_reset_postdata();

			return $duplicate_ids;
		}
		else{
			$field = 'post_' . $duplicate_indicator; // post_title or post_content
			return $this->wpdb->get_col($this->wpdb->prepare("
				SELECT ID FROM " . $this->wpdb->posts . "
				WHERE
					post_type = %s
					AND ID != %s
					AND REPLACE(REPLACE(REPLACE($field, ' ', ''), '\\t', ''), '\\n', '') = %s
				",
				$articleData['post_type'],
				isset($articleData['ID']) ? $articleData['ID'] : 0,
				preg_replace('%[ \\t\\n]%', '', $articleData[$field])
			));
		}
	}
	
	function duplicate_post_copy_post_meta_info($new_id, $post) {
		$post_meta_keys = get_post_custom_keys($post->ID);
		$meta_blacklist = array();
		$meta_keys = array_diff($post_meta_keys, $meta_blacklist);

		foreach ($meta_keys as $meta_key) {
			$meta_values = get_post_custom_values($meta_key, $post->ID);
			foreach ($meta_values as $meta_value) {
				$meta_value = maybe_unserialize($meta_value);
				add_post_meta($new_id, $meta_key, $meta_value);
			}
		}
	}	

	function auto_cloak_links($import, &$url){
		$url = trim($url);
		// cloak urls with `WP Wizard Cloak` if corresponding option is set
		if ( ! empty($import->options['is_cloak']) and class_exists('PMLC_Plugin')) {														
			if (preg_match('%^\w+://%i', $url)) { // mask only links having protocol
				// try to find matching cloaked link among already registered ones
				$list = new PMLC_Link_List(); $linkTable = $list->getTable();
				$rule = new PMLC_Rule_Record(); $ruleTable = $rule->getTable();
				$dest = new PMLC_Destination_Record(); $destTable = $dest->getTable();
				$list->join($ruleTable, "$ruleTable.link_id = $linkTable.id")
					->join($destTable, "$destTable.rule_id = $ruleTable.id")
					->setColumns("$linkTable.*")
					->getBy(array(
						"$linkTable.destination_type =" => 'ONE_SET',
						"$linkTable.is_trashed =" => 0,
						"$linkTable.preset =" => '',
						"$linkTable.expire_on =" => '0000-00-00',
						"$ruleTable.type =" => 'ONE_SET',
						"$destTable.weight =" => 100,
						"$destTable.url LIKE" => $url,
					), NULL, 1, 1)->convertRecords();
				if ($list->count()) { // matching link found
					$link = $list[0];
				} else { // register new cloaked link
					global $wpdb;
					$slug = max(
						intval($wpdb->get_var("SELECT MAX(CONVERT(name, SIGNED)) FROM $linkTable")),
						intval($wpdb->get_var("SELECT MAX(CONVERT(slug, SIGNED)) FROM $linkTable")),
						0
					);
					$i = 0; do {
						is_int(++$slug) and $slug > 0 or $slug = 1;
						$is_slug_found = ! intval($wpdb->get_var("SELECT COUNT(*) FROM $linkTable WHERE name = '$slug' OR slug = '$slug'"));
					} while( ! $is_slug_found and $i++ < 100000);
					if ($is_slug_found) {
						$link = new PMLC_Link_Record(array(
							'name' => strval($slug),
							'slug' => strval($slug),
							'header_tracking_code' => '',
							'footer_tracking_code' => '',
							'redirect_type' => '301',
							'destination_type' => 'ONE_SET',
							'preset' => '',
							'forward_url_params' => 1,
							'no_global_tracking_code' => 0,
							'expire_on' => '0000-00-00',
							'created_on' => date('Y-m-d H:i:s'),
							'is_trashed' => 0,
						));
						$link->insert();
						$rule = new PMLC_Rule_Record(array(
							'link_id' => $link->id,
							'type' => 'ONE_SET',
							'rule' => '',
						));
						$rule->insert();
						$dest = new PMLC_Destination_Record(array(
							'rule_id' => $rule->id,
							'url' => $url,
							'weight' => 100,
						));
						$dest->insert();
					} else {
						$logger and call_user_func($logger, sprintf(__('<b>WARNING</b>: Unable to create cloaked link for %s', 'pmxi_plugin'), $url));
						PMXI_Plugin::$session['pmxi_import']['warnings'] = PMXI_Plugin::$session->data['pmxi_import']['warnings']++;
						$link = NULL;
					}
				}
				if ($link) { // cloaked link is found or created for url
					$url = preg_replace('%' . preg_quote($url, '%') . '(?=([\s\'"]|$))%i', $link->getUrl(), $url);								
				}									
			}
		}
	}

	function keep_custom_fields($existing_meta_keys, $options, $meta_key){

		$keep_custom_fields_specific = ( ! $options['keep_custom_fields'] and ! empty($options['keep_custom_fields_specific'])) ? array_map('trim', explode(',', $options['keep_custom_fields_specific'])) : array();

		return (($options['keep_custom_fields'] and in_array($meta_key, $existing_meta_keys)) or (in_array($meta_key, $existing_meta_keys) and ! $options['keep_custom_fields'] and in_array($meta_key, $keep_custom_fields_specific))) ? false : true;

	}
}
