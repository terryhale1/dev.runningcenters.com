*** WooCommerce Wishlist Changelog ***
2013.08.03 - version 1.2.3
	* Update: Added missing localization strings

2013.07.16 - version 1.2.2
	* Update:  Clean up older javascript. 
	* Update: Javascript to check for length of bundles or variations before attempting to move the wishlist wrapper. 

2013.07.11 - version 1.2.1
	* Update: Compatibility with product bundles when variable products are in the bundle. 
	* Update: Use wc_print_messages for latest version of WooCommerce

2013.06.23 - version 1.2.0
	* Update: Moved menu position below core woocommerce items. 

2013.06.17 - version 1.1.9
	* Update:  Moved email modal template to footer to prevent z-index issues.  

2013.06.13 - version 1.1.8
	* Fix: Check if date is present on list item before attempting to sort. 

2013.05.30 - version 1.1.7
	* Fix:  Incorrect redirect when navigate directly to the Edit List page without selecting a list first. 

2013.05.29 - version 1.1.6
	* Update: More fixes for the email button and third party themes that are capturing the click event and stopping it. 

2013.05.23 - version 1.1.5
    * Update:  Change selector on the email modal to be compatible with some third party themes that break it. 

2013.05.22 - version 1.1.4
    * Update: Use closest form to the add to wishlist rather than form.cart.  
        This is because some theme authors remove the cart class from the main product form. 

2013.05.07 - version 1.1.3
    * Update:  Loaded text domain wc_wishlist and created empty lang folder. 

2013.05.01 - version 1.1.2
    * Fixes sharing problems for unauthenticated users. 

2013.04.10 - version 1.1.1
 * Modified add-to-wishlist popup to allow it to work when used inside another modal window. 
 * Fixed send email button code

2013.04.06 - version 1.1.0
 * Check for post before processing templates

2013.04.02 - version 1.0.1
 *Remove call by reference in item collection class. 

2013.03.22 - version 1.0
 *Initial Release