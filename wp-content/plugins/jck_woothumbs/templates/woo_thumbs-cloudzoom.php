<?php 
/*
If you need to change this template, duplicate the file
and add it to /wp-content/themes/your-theme/jck/woo_thumbs.php
*/
global $attachments; // DO NOT EDIT

// ===== Edit below this line after copying to your theme folder (if required) ===== //

$small_thumbnail_size  	= apply_filters('single_product_small_thumbnail_size', 'shop_thumbnail');
$medium_thumbnail_size 	= apply_filters('single_product_large_thumbnail_size', 'shop_single');

$loop = 0;
$columns = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );

foreach ( $attachments as $attachment_id ) {

	if ( get_post_meta( $attachment_id, '_woocommerce_exclude_image', true ) == 1 )
		continue;

	$loop++;

	$url        = wp_get_attachment_image_src( $attachment_id, 'full' );
	$post_title = esc_attr( get_the_title( $attachment_id ) );
	$image      = wp_get_attachment_image( $attachment_id, $small_thumbnail_size );

	$cloudmediumImage	= wp_get_attachment_image_src( $attachment_id, $medium_thumbnail_size );
	$cloudimagepath 	= wp_get_attachment_image_src( $attachment_id, 'large' );

	echo '<a href="' . $url[0] . '" title="' . $post_title . '" cloud="useZoom:\'zoom1\',smallImage:\'' . $cloudmediumImage[0] . '\'" class="cloud-zoom-gallery zoom noLightbox';
	if ( $loop == 1 || ( $loop - 1 ) % $columns == 0 ) echo ' first';
	if ( $loop % $columns == 0 ) echo ' last';
	echo '" rel="'.$rel.'">' . $image . '</a>' ."\r\n" ."\r\n";
}

// CALLBACK: jQuery('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();