<?php
/**
 * Quick view template
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $post, $woocommerce;
?>
<div class="woocommerce quick-view product">

	<div class="quick-view-image images">

		<?php if ( has_post_thumbnail() ) : ?>

			<?php echo get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) ) ?>

		<?php else : ?>

			<img src="<?php echo woocommerce_placeholder_img_src(); ?>" alt="Placeholder" />

		<?php endif; ?>
        <a class="button" href="<?php echo get_permalink( $product->id ); ?>"><?php _e( 'View Full Details', 'wc_quick_view' ); ?></a>

	</div>


	<div class="quick-view-content">

		<?php woocommerce_template_single_title(); ?>
		<?php woocommerce_template_single_price(); ?>
		<?php woocommerce_template_single_excerpt(); ?>
		<?php woocommerce_template_single_add_to_cart(); ?>

	</div>

</div>