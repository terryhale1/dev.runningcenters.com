=== WooCommerce - Store Toolkit ===

Contributors: visser
Donate link: http://www.visser.com.au/#donations
Tags: woocommerce, mod, delete store, clean store, nuke, store toolkit
Requires at least: 2.9.2
Tested up to: 3.7
Stable tag: 1.3.7

== Description ==

Store Toolkit includes a growing set of commonly-used WooCommerce administration tools aimed at web developers and store maintainers.

**Features**

1. Nuke support for clearing WooCommerce store records

* Products
* Product Images
* Produt Tags
* Product Categories
* Orders
* Coupons

2. Nuke support for clearing WordPress records

* Posts
* Post Categories
* Post Tags
* Links
* Comments

If you find yourself in the situation where you need to start over with a fresh installation of WooCommerce then a 'nuke' will do the job.

For more information visit: http://www.visser.com.au/woocommerce/

== Installation ==

1. Upload the folder 'woocommerce-store-toolkit' to the '/wp-content/plugins/' directory
2. Activate 'WooCommerce - Store Toolkit' through the 'Plugins' menu in WordPress

That's it!

== Usage ==

1. Open WooCommerce > Store Toolkit
2. Select which WooCommerce details you would like to remove and click Nuke

Done!

== Frequently Asked Questions ==

**Where can I request new features?**

You can vote on and request new features and extensions on our Support Forum, see http://www.visser.com.au

**Where can I report bugs or contribute to the project?**

Bugs can be reported here on WordPress.org or on our Support Forum, see http://www.visser.com.au

== Support ==

If you have any problems, questions or suggestions please join the members discussion on my WooCommerce dedicated forum.

http://www.visser.com.au/woocommerce/forums/

== Changelog ==

= 1.3.7 =
* Fixed: Missing icon
* Changed: Layout styling of descriptions on Nuke screen
* Added: Screenshots to Plugin page

= 1.3.6 =
* Changed: Plugin description
* Fixed: Updated URL for WooCommerce Plugin News widget

= 1.3.5 =
* Changed: Removed woo_admin_is_valid_icon
* Changed: Using default WooCommerce icons

= 1.3.4 =
* Added: Select All options to Nuke

= 1.3.3 =
* Fixed: Coupons removal

= 1.3.2 =
* Added: Per-Category Product nuking
* Added: Tabs support
* Changed: Removed Tools menu reference

= 1.3.1 =
* Added: Store Toolkit menu item under WooCommerce

= 1.3 =
* Added: Attributes support

= 1.2 =
* Changed: Cleaned up markup
* Added: Remove WooCommerce term details when removing Categories

= 1.1 =
* Fixed: Dashboard widget URL out of date

= 1.0 =
* Added: Delete Products, Product Categories, Tags and Orders
* Added: First working release of the Plugin

== Screenshots ==

1. The Store Toolkit overview screen.
2. The Nuke WooCommerce screen with 'nuke' options.
3. Additional 'nuke' options for WordPress details.

== Disclaimer ==

It is not responsible for any harm or wrong doing this Plugin may cause. Users are fully responsible for their own use. This Plugin is to be used WITHOUT warranty.